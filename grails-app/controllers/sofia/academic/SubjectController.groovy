package sofia.academic

import grails.rest.RestfulController

class SubjectController extends RestfulController<Subject> {
    static responseFormats = ['json']

    SubjectModelService subjectModelService

    SubjectController() {
        super(Subject)
    }

    @Override
    protected List<Subject> listAllResources(Map params) {
        subjectModelService.getBySchoolYearAndGrade(params.schoolYearId as Long, params.gradeId as Long)
    }
}