package sofia.academic

import grails.rest.RestfulController

class EvaluationController extends RestfulController<Evaluation> {
    static responseFormats = ['json']

    EvaluationModelService evaluationModelService

    EvaluationController() {
        super(Evaluation)
    }

    @Override
    protected List<Evaluation> listAllResources(Map params) {
        evaluationModelService.findAllByPeriodAndCourse(params.periodId as Long, params.courseId as Long)
    }

    @Override
    Object update() {
        evaluationModelService.update(this)
    }

    @Override
    Object save() {
        evaluationModelService.save(this)
    }
}
