package sofia.academic

import grails.rest.RestfulController

class StudentEnrollmentController extends RestfulController<Enrollment> {
    static responseFormats = ['json']

    EnrollmentModelService enrollmentModelService

    StudentEnrollmentController() {
        super(Enrollment, false)
    }

    @Override
    def show() {
        respond queryForResource(params.studentId)
    }

    @Override
    protected Enrollment queryForResource(Serializable id) {
        enrollmentModelService.getCurrentEnrollmentByStudent(id as Long)
    }

    @Override
    protected List listAllResources(Map params) {
        enrollmentModelService.getEnrollmentsByStudent(params.studentId as Long)
    }

    @Override
    def update() {
        enrollmentModelService.updateWithParam(this, 'studentId')
    }

    @Override
    def save() {
        enrollmentModelService.save(this)
    }
}