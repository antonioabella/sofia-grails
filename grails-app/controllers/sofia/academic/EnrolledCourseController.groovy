package sofia.academic

import grails.rest.RestfulController

class EnrolledCourseController extends RestfulController<EnrolledCourse> {
    static responseFormats = ['json']

    EnrolledCourseDataService enrolledCourseDataService

    EnrolledCourseController() {
        super(EnrolledCourse)
    }

    @Override
    protected List<EnrolledCourse> listAllResources(Map params) {
        enrolledCourseDataService.findAllByCourse(params.courseId as Long)
    }
}