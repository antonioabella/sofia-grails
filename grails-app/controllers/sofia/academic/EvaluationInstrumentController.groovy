package sofia.academic

import grails.rest.RestfulController

class EvaluationInstrumentController extends RestfulController<EvaluationInstrument> {
    static responseFormats = ['json']

    EvaluationInstrumentController() {
        super(EvaluationInstrument)
    }

    @Override
    protected List<EvaluationInstrument> listAllResources(Map params) {
        resource.list()
    }
}
