package sofia.academic

import grails.rest.RestfulController

class CourseController extends RestfulController<Course> {
    static responseFormats = ['json']

    CourseDataService courseDataService

    CourseController() {
        super(Course)
    }

    @Override
    protected List<Course> listAllResources(Map params) {
        courseDataService.findAllBySchoolYearAndTeacher(params.schoolYearId as Long, params.teacherId as Long)
    }
}
