package sofia.academic

import grails.transaction.Transactional

@Transactional(readOnly = true)
class SectionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static responseFormats = ['json']

    SectionDataService sectionDataService

    def index(Long schoolYearId) {
        request.withFormat {
            json {
                respond sectionDataService.list(schoolYearId)
            }
        }
    }
}
