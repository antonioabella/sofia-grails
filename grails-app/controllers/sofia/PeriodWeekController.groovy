package sofia

import grails.rest.RestfulController

class PeriodWeekController extends RestfulController<PeriodWeek> {
    static responseFormats = ['json']

    PeriodWeekDataService periodWeekDataService

    PeriodWeekController() {
        super(PeriodWeek)
    }

    @Override
    protected List<PeriodWeek> listAllResources(Map params) {
        periodWeekDataService.findAllByPeriod(params.periodId as Long)
    }
}
