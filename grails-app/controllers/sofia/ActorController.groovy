package sofia

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ActorController {
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ActorSearchService actorSearchService
    ActorDataService actorDataService
    ActorBuilderService actorBuilderService

    static responseFormats = ['json']

    def index(ActorSearchCommand actorSearchCommand) {
        request.withFormat {
            json {
                respond actorSearchService.search(actorSearchCommand)
            }
        }
    }

    def show(Long id) {
        request.withFormat {
            json {
                respond actorDataService.get(id)
            }
        }
    }

    private boolean validate(def actor) {
        if (actor.hasErrors()) {
            respond actor
            return false
        }
        actor.person?.validate()
        if (!actor.person || actor.person.hasErrors()) {
            respond actor.person
            return false
        }
        true
    }

    @Transactional
    def save(ActorCommand actorCommand) {
        request.withFormat {
            json {
                if (!validate(actorCommand)) return
                Actor actor = actorBuilderService.save(actorCommand)
                respond actor, status: actor ? CREATED.value() : UNPROCESSABLE_ENTITY.value()
            }
        }
    }

    @Transactional
    def update(Actor actor) {
        request.withFormat {
            json {
               if (!validate(actor)) return
                actor = actorDataService.save(actor)
                respond actor, status: actor ? OK.value() : UNPROCESSABLE_ENTITY.value()
            }
        }
    }
}

class ActorSearchCommand {
    String entityName
    String criteria
}

class ActorCommand {
    String entityName
    Person person
    static constraints = {
        entityName nullable: false
        person nullable: false, validator: { val, obj ->
            if (obj.entityName == 'student') return true
            if (!val.phoneNumber) return 'person.not.phoneNumber.message'
            if (!val.emailAddress) return 'person.not.emailAddress.message'
            true
        }
    }

    String toString() {
        person.toString()
    }
}
