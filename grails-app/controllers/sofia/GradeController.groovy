package sofia

import grails.transaction.Transactional

@Transactional(readOnly = true)
class GradeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static responseFormats = ['json']

    GradeDataService gradeDataService

    def index() {
        request.withFormat {
            json {
                respond gradeDataService.list()
            }
        }
    }
}
