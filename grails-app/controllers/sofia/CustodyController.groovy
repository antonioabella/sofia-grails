package sofia

import grails.rest.RestfulController

import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED

class CustodyController extends RestfulController<Custody> {
    static responseFormats = ['json']

    CustodyController() {
        super(Custody)
    }

    @Override
    def index() {
        render status: METHOD_NOT_ALLOWED
    }
}