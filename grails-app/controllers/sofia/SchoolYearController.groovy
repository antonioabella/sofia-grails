package sofia

import grails.transaction.Transactional

@Transactional(readOnly = true)
class SchoolYearController {

    static responseFormats = ['json']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    SchoolYearDataService schoolYearDataService

    def index() {
        request.withFormat {
            json {
                respond schoolYearDataService.list()
            }
        }
    }
}
