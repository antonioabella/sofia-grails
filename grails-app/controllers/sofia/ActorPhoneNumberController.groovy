package sofia

import grails.rest.RestfulController

class ActorPhoneNumberController extends RestfulController<PhoneNumber> {
    static responseFormats = ['json']

    ActorModelService actorModelService

    ActorPhoneNumberController() {
        super(PhoneNumber)
    }

    protected void deleteContact(Person person, PhoneNumber phoneNumber) {
        actorModelService.removePhone(person, phoneNumber)
    }

    protected void updateContact(Person person, PhoneNumber phoneNumber) {
        actorModelService.updatePhone(person, phoneNumber)
    }

    @Override
    protected List listAllResources(Map params) {
        actorModelService.getPhoneNumbers(params.actorId as Long)
    }

    @Override
    def save() {
        actorModelService.addPhoneNumber(this)
    }

    @Override
    def update() {
        actorModelService.updateContact(this)
    }

    @Override
    def delete() {
        actorModelService.deleteContact(this)
    }
}