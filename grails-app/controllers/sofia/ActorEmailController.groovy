package sofia

import grails.rest.RestfulController

class ActorEmailController extends RestfulController<Email> {
    static responseFormats = ['json']

    ActorModelService actorModelService

    ActorEmailController() {
        super(Email)
    }

    @Override
    protected List listAllResources(Map params) {
        actorModelService.getEmails(params.actorId as Long)
    }

    @Override
    def delete() {
        actorModelService.deleteContact(this)
    }

    protected void deleteContact(Person person, Email email) {
       actorModelService.removeEmail(person, email)
    }
}