package sofia

import grails.transaction.Transactional

@Transactional(readOnly = true)
class MonthController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static responseFormats = ['json']

    MonthDataService monthDataService

    def index() {
        request.withFormat {
            json {
                respond monthDataService.list()
            }
        }
    }
}
