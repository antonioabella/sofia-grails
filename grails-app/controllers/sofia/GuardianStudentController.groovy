package sofia

import grails.rest.RestfulController

class GuardianStudentController extends RestfulController<Guardian> {
    static responseFormats = ['json']

    GuardianDataService guardianDataService

    GuardianStudentController() {
        super(Guardian, true)
    }

    @Override
    protected List listAllResources(Map params) {
        guardianDataService.getStudents(params.guardianId as Long)
    }
}