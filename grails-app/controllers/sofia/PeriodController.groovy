package sofia

import grails.rest.RestfulController

class PeriodController extends RestfulController<Period> {
    static responseFormats = ['json']

    PeriodDataService periodDataService

    PeriodController() {
        super(Period)
    }

    @Override
    protected List<Period> listAllResources(Map params) {
        periodDataService.findAllBySchoolYear(params.schoolYearId as Long)
    }
}
