package sofia

import grails.rest.RestfulController

class StudentCustodianController extends RestfulController<Student> {
    static responseFormats = ['json']
    StudentDataService studentDataService
    CustodyModelService custodyModelService

    StudentCustodianController() {
        super(Student)
    }

    @Override
    protected List listAllResources(Map params) {
        studentDataService.getCustodians(params.studentId as Long)
    }

    @Override
    def update() {
        custodyModelService.setMainCustodian(this)
    }
}