package sofia

import grails.transaction.Transactional
import sofia.academic.EnrollmentDataService

@Transactional(readOnly = true)
class GuardianEnrollmentController {

    GuardianEnrollmentModelService guardianEnrollmentModelService

    static responseFormats = ['json']

    def index(GuardianSchoolYearCommand cmd) {
        request.withFormat {
            json {
                respond guardianEnrollmentModelService
                        .findAllByGuardianIdAndSchoolYearId(cmd.guardianId,
                        cmd.schoolYearId)
            }
        }
    }
}

class GuardianSchoolYearCommand {
    Long guardianId
    Long schoolYearId
}
