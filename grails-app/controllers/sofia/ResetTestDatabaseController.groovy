package sofia

import grails.plugin.fixtures.FixtureLoader
import grails.util.Environment
import org.hibernate.Query
import org.hibernate.Session

class ResetTestDatabaseController {
    FixtureLoader fixtureLoader

    def index() {
        Environment.executeForCurrentEnvironment {
            test {
                Institute.withNewSession { Session session ->
                    integrityOff(session)
                    truncateTables(session)
                    integrityOn(session)
                    session.flush()
                }
                fixtureLoader.load 'academic/evaluations*'
            }
        }
    }

    private void truncateTables(Session session) {
        Query query = session.createSQLQuery('select table_name from information_schema.tables where table_type = \'TABLE\';')
        query.list().each {
            Query truncate = session.createSQLQuery("truncate table ${it};")
            truncate.executeUpdate()
        }
    }

    private void integrityOn(Session session) {
        Query integrity = session.createSQLQuery('SET REFERENTIAL_INTEGRITY TRUE;')
        integrity.executeUpdate()
    }

    private void integrityOff(Session session) {
        Query integrity = session.createSQLQuery('SET REFERENTIAL_INTEGRITY FALSE;')
        integrity.executeUpdate()
    }
}