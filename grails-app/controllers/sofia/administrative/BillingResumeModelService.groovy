package sofia.administrative

class BillingResumeModelService {
    InvoiceDataService invoiceDataService
    PaidConceptDataService paidConceptDataService
    PaymentItemDataService paymentItemDataService
    PaymentDataService paymentDataService

    Map getResume(Long billingGroupId, Long schoolYearId, Date from, Date until) {
        List<Long> invoicesIds = invoiceDataService.getPaidInvoicesIds(billingGroupId, schoolYearId, from, until)
        List<Long> paymentsIds = paymentDataService.getPaymentsIds(invoicesIds)
        [invoiceResume    : invoiceDataService.getResume(billingGroupId, schoolYearId, from, until),
         paidConceptResume: paidConceptDataService.getResume(billingGroupId, schoolYearId, from, until),
         paymentResume    : paymentItemDataService.getResume(paymentsIds)
        ]
    }
}