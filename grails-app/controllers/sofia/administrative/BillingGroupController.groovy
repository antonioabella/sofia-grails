package sofia.administrative

import grails.transaction.Transactional

@Transactional(readOnly = true)
class BillingGroupController {
    BillingGroupDataService billingGroupDataService

    static responseFormats = ['json']

    def index() {
        request.withFormat {
            json {
               respond billingGroupDataService.list()
            }
        }
    }

}
