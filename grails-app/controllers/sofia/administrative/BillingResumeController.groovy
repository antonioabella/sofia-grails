package sofia.administrative

import grails.transaction.Transactional
import groovy.time.TimeCategory

@Transactional(readOnly = true)
class BillingResumeController {
    BillingResumeModelService billingResumeModelService

    static responseFormats = ['json']

    def index(BillingResumeCommand brc) {
        request.withFormat {
            json {
                respond billingResumeModelService.getResume(brc.billingGroupId, brc.schoolYearId, brc.from, brc.until)
            }
        }
    }
}

class BillingResumeCommand {
    Long billingGroupId
    Long schoolYearId
    Date from
    Date until

    void setUntil(Date value) {
        value.clearTime()
        use (TimeCategory) {
            value = value + 1.day
        }
        until = value
    }

    void setFrom(Date value) {
        value.clearTime()
        from = value
    }
}
