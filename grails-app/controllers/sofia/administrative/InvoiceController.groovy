package sofia.administrative

import grails.transaction.Transactional

class InvoiceController {

    @Transactional
    def save(InvoiceCommand invoiceCommand) {
    }
}

class ConceptToPayCommand {
    Long enrollmentId
    Long conceptId
    Long monthId
    Long amount
    Long discount
}

class InvoiceCommand {
    List<ConceptToPayCommand> conceptsToPay
}