package sofia.administrative

import grails.transaction.Transactional

@Transactional(readOnly = true)
class ConceptCostController {
    ConceptCostDataService conceptCostDataService

    static responseFormats = ['json']

    def index(Long schoolYearId) {
        request.withFormat {
            json {
               respond conceptCostDataService.findAllBySchoolYearId(schoolYearId)
            }
        }
    }

}
