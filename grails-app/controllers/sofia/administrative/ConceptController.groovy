package sofia.administrative

import grails.transaction.Transactional

@Transactional(readOnly = true)
class ConceptController {
    ConceptDataService conceptDataService

    static responseFormats = ['json']

    def index() {
        request.withFormat {
            json {
               respond conceptDataService.list()
            }
        }
    }

}
