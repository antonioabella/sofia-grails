package sofia

import grails.transaction.Transactional

@Transactional(readOnly = true)
class GuardianController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static responseFormats = ['json']

    GuardianDataService guardianDataService

    def show(Long id) {
        request.withFormat {
            json {
                respond guardianDataService.getInstance(id)
            }
        }
    }
}
