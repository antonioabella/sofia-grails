package sofia

import static org.springframework.http.HttpStatus.NO_CONTENT


class GenericController {

    String resourceClassName
    String defaultAction

    def searchNotFound(String criteria) {
        flash.message = message(code: "elemento.not.found.message", args: [resourceClassName, criteria])
        redirect(action: defaultAction)
    }

    def savingError(instance) {
        render(view: "create", model: [("${resourceClassName}Instance".toString()): instance])
    }

    def savingOk(instance) {
        flash.message = message(code: "default.created.message", args: [message(code: "${resourceClassName}.label"), instance.id])
        redirect(action: "show", id: instance.id)
    }

    def notFound(Long id) {
        flash.message = message(code: "default.not.found.message", args: [message(code: "${resourceClassName}.label", default: "${resourceClassName}"), id])
        redirect(action: defaultAction)
    }

    def updatingOk(instance) {
        flash.message = message(code: "default.updated.message", args: [message(code: "${resourceClassName}.label", default: "${resourceClassName}"), instance.id])
        redirect(action: "show", id: instance.id)
    }

    def updatingError(instance) {
        render(view: "edit", model: [("${resourceClassName}Instance".toString()): instance])
    }

    def updatingVersionError(instance) {
        instance.errors.rejectValue("version", "default.optimistic.locking.failure",
                [message(code: "${resourceClassName}.label", default: "${resourceClassName}")] as Object[],
                "Another user has updated this ${resourceClassName} while you were editing")
        render(view: "edit", model: [("${resourceClassName}Instance".toString()): instance])
    }

    def deletingOk(instance) {
        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: "${resourceClassName}.label".toString(), default: resourceClassName), instance.id])
                redirect action: defaultAction, method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    def deletingError(instance) {
        flash.message = message(code: "default.not.deleted.message", args: [message(code: "${resourceClassName}.label", default: "${resourceClassName}"), instance.id])
        redirect(action: "show", id: instance.id)
    }

    def inactiveError(instance) {
        flash.message = message(code: "default.inactive.message", args: [message(code: "${resourceClassName}.label", default: "${resourceClassName}"), instance.id])
        redirect(action: "show", id: instance.id)
    }
}
