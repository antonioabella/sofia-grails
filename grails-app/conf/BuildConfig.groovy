grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.project.war.file = "ROOT.war"

grails.project.fork = [
    // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
    //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],

    // configure settings for the test-app JVM, uses the daemon by default
    test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    // configure settings for the run-app JVM
    run: false, // [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the run-war JVM
    war: false, // war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the Console UI JVM
    console: false, //console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()
        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://repo.spring.io/milestone/"
    }

    dependencies {
        runtime 'mysql:mysql-connector-java:5.1.31'
        test "org.grails:grails-datastore-test-support:1.0-grails-2.4"
        test 'cglib:cglib-nodep:3.1'
        test 'com.github.groovy-wslite:groovy-wslite:1.1.0'
    }

    plugins {
        // plugins for the build system only
        build ":tomcat:7.0.54"

        compile ":improx:0.3"
        compile ":spring-security-core:2.0-SNAPSHOT"
        compile ":mail:1.0.8-SNAPSHOT"
        compile ":functional-spock:0.7"
        compile ":remote-control:1.5"
        compile ":grails-melody:1.55.0"

        // plugins needed at runtime but not for compilation
        runtime ":build-test-data:2.2.2"
        runtime ':console:1.5.4'
        runtime ":cors:1.1.6"
        runtime ":database-migration:1.4.0"
        runtime ":fixtures:1.3.1-SNAPSHOT"
        runtime ":hibernate4:4.3.5.4" // or ":hibernate:3.6.10.16"

        test ":code-coverage:2.0.3-3"
        test ":codenarc:0.22"
        test ':dumbster:0.2'
        test ":gmetrics:0.3.1", { exclude "GMetrics" }
    }
}

codenarc.properties = {
    // Each property definition is of the form:  RULE.PROPERTY-NAME = PROPERTY-VALUE
    GrailsPublicControllerMethod.enabled = false
    GrailsDomainHasEquals.enabled = false
    EmptyIfStatement.priority = 1
}

coverage {
    exclusions = [
            "**/sofia/security/LoginController**",
            "**/sofia/enums/**",
            "**/*Config*"
    ]
}
