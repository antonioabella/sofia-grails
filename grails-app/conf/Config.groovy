grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}

grails.app.context = "/"
grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration
log4j.main = {
    appenders {
        String logDir = System.getProperty("catalina.base") ?: "target"
        rollingFile name: "stacktrace",
                maxFileSize: 10 * 1024 * 1024,
                file: "$logDir/logs/sofia.log",
                layout: pattern(conversionPattern: "'%d [%t] %-5p %c{2} %x - %m%n'"),
                maxBackupIndex: 10
        // console name:"stdout", layout:pattern(conversionPattern: "%c{2} %m%n")
    }

    error 'org.codehaus.groovy.grails.web.servlet',        // controllers
          'org.codehaus.groovy.grails.web.pages',          // GSP
          'org.codehaus.groovy.grails.web.sitemesh',       // layouts
          'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
          'org.codehaus.groovy.grails.web.mapping',        // URL mapping
          'org.codehaus.groovy.grails.commons',            // core / classloading
          'org.codehaus.groovy.grails.plugins',            // plugins
          'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
          'org.springframework',
          'org.hibernate',
          'net.sf.ehcache.hibernate'

    info "grails.app"

    if(System.getProperty('show.sql')) {
        debug "org.hibernate.SQL"
        trace "org.hibernate.type.descriptor.sql.BasicBinder"
    }

}
/***
 * Antonio Abella 2011-08-23
 * Configuración para el funcionamiento del envío de correos.
 */
environments {
    test {
        dumbster.enabled = true
        grails.mail.host = "localhost"
    }

    development {
        dumpster.enabled = true
        grails.mail.host = "localhost"
    }

    production {
        grails {
            mail {
                host = "email-smtp.us-east-1.amazonaws.com"
                port = 465
                username = "AKIAIF6AXMIUAJ3UDQYQ"
                password = "AoHQZJgBlJxY1V1xK1AY/NHJVMLM3unh/umX800u0BAw"
                props = [
                        "mail.smtp.auth"                  : "true",
                        "mail.smtp.socketFactory.port"    : "465",
                        "mail.smtp.socketFactory.class"   : "javax.net.ssl.SSLSocketFactory",
                        "mail.smtp.socketFactory.fallback": "false"
                ]
            }
        }
    }
}

grails.mail.default.from = "Sistema Sofia<contacto@sistemasofia.com>"

gmetrics.metricSetfilename = "file:test.gmetrics"

coverage {
    enabledByDefault = false
}

grails.plugin.springsecurity.active = false

//environments {
//    production {
//        grails.plugin.springsecurity.active = true
//    }
//}

grails.dbconsole.enabled = true
//grails.gorm.failOnError = true
grails.databinding.dateFormats = ["yyyy-MM-dd'T'hh:mm:ss'Z'", "yyyy-MM-dd'T'hh:mm:ss.S'Z'", 'MMddyyyy', 'yyyy-MM-dd HH:mm:ss.S']
grails.plugin.console.enabled = true
