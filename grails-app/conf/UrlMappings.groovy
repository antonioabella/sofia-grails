class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        '/'(view: '/index')
        '500'(view: '/error')
        '/actors'(resources: 'actor') {
            '/phones'(resources: 'actorPhoneNumber')
        }
        '/actors'(resources: 'actor') {
            '/emails'(resources: 'actorEmail')
        }
        '/billingGroups'(resources: 'billingGroup')
        '/billingResume'(resources: 'billingResume')
        '/concepts'(resources: 'concept')
        '/conceptCosts'(resources: 'conceptCost')
        '/subjects'(resources: 'subject')
        '/custodies'(resources: 'custody')
        '/enrolledCourses'(resources: 'enrolledCourse')
        '/evaluationInstruments'(resources: 'evaluationInstrument')
        '/evaluations'(resources: 'evaluation')
        '/grades'(resources: 'grade')
        '/guardians'(resources: 'guardian') {
            '/enrollments'(resources: 'guardianEnrollment')
            '/students'(resources: 'guardianStudent')
        }
        '/invoices'(resources: 'invoice')
        '/months'(resources: 'month')
        '/periods'(resources: 'period')
        '/periodWeeks'(resources: 'periodWeek')
        '/courses'(resources: 'course') {
            '/students'(resources: 'enrolledClass')
        }
        '/schoolYears'(resources: 'schoolYear')
        '/sections'(resources: 'section')
        '/students'(resources: 'student') {
            '/custodians'(resources: 'studentCustodian')
            '/enrollments'(resources: 'studentEnrollment')
        }
    }
}