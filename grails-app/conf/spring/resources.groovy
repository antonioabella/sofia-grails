import sofia.MarshallerRegistrar

beans = {
    localeResolver(org.springframework.web.servlet.i18n.SessionLocaleResolver) {
        defaultLocale = new Locale("es", "VE")
        java.util.Locale.setDefault(defaultLocale)
    }
    sofiaMarshallerRegistrar(MarshallerRegistrar)
}