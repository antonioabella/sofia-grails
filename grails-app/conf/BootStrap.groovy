import grails.converters.JSON
import grails.plugin.fixtures.FixtureLoader
import sofia.EducationLevel
import sofia.Grade
import sofia.Period
import sofia.academic.Enrollment
import sofia.academic.Evaluation
import sofia.academic.Section
import sofia.enums.Gender
import sofia.enums.Kinship
import sofia.enums.PhoneType

class BootStrap {
    FixtureLoader fixtureLoader

    def init = { servletContext ->
        if (System.getProperty('load.fixtures')) fixtureLoader.load 'academic/evaluations*'
        JSON.registerObjectMarshaller(EducationLevel) { EducationLevel level ->
            [
                    id  : level.id,
                    name: level.name,
            ]
        }
        JSON.registerObjectMarshaller(Grade) { Grade grade ->
            [
                    id      : grade.id,
                    name    : grade.name,
                    level   : grade.level,
                    position: grade.position
            ]
        }
        JSON.registerObjectMarshaller(Section) { Section section ->
            [
                    id   : section.id,
                    name : section.name,
                    grade: section.grade,
            ]
        }
        JSON.registerObjectMarshaller(Evaluation) { Evaluation it ->
            [
                    id        : it.id,
                    content   : it.content,
                    course    : it.course,
                    courseName: it.course.subject.name,
                    instrument: it.instrument,
                    period    : it.period,
                    periodName: it.period.name,
                    remarks   : it.remarks,
                    results   : it.results,
                    week      : it.week,
                    weekName  : it.week.toString(),
                    weight    : it.weight,
            ]
        }
        JSON.registerObjectMarshaller(Period) { Period it ->
            [
                    id  : it.id,
                    name: it.name
            ]
        }
        JSON.registerObjectMarshaller(Enrollment) { Enrollment it ->
            [
                    id                  : it.id,
                    enrollmentDate      : it.enrollmentDate,
                    enrollmentMonth     : it.enrollmentMonth,
                    grade               : it.grade,
                    section             : it.section,
                    sofia               : it.sofia,
                    withdrawalDate      : it.withdrawalDate,
                    withdrawalMonth     : it.withdrawalMonth,
                    withdrawalReason    : it.withdrawalReason,
                    originInstitute     : it.originInstitute,
                    destinationInstitute: it.destinationInstitute
            ]
        }
        JSON.registerObjectMarshaller(Gender) { Gender it -> it.toString() }
        JSON.registerObjectMarshaller(PhoneType) { PhoneType it -> it.toString() }
        JSON.registerObjectMarshaller(Kinship) { Kinship it -> it.toString() }
    }
    def destroy = {
    }
}