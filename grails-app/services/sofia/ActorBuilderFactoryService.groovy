package sofia

class ActorBuilderFactoryService {
    StudentBuilderService studentBuilderService
    GuardianBuilderService guardianBuilderService
//    TeacherBuilderService teacherBuilderService
    ActorBuilder create(String entityName) {
        log.info entityName
        if (entityName == 'student') return studentBuilderService
        if (entityName == 'guardian') return guardianBuilderService
    }
}