package sofia

class PeriodDataService extends GenericData<Period> {
    @Override
    Class<Period> getSource() {
        Period
    }

    List<Period> findAllBySchoolYear(Long schoolYearId) {
        source.withCriteria {
            schoolYear {
                eq 'id', schoolYearId
            }
        }
    }
}
