package sofia

class MainCustodyDataService extends CustodyDataService implements ActorFinder {

    @Override
    boolean onlyMainGuardians() {
        true
    }
}