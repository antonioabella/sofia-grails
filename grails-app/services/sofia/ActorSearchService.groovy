package sofia

class ActorSearchService {
    ActorFinderFactoryService actorFinderFactoryService

    List search(ActorSearchCommand actorSearchCommand) {
        ActorFinder actorFinder = actorFinderFactoryService.create(actorSearchCommand.entityName)
        return actorFinder? actorFinder.search(actorSearchCommand.criteria) : []
    }
}