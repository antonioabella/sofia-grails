package sofia

class PhoneNumberDataService extends GenericData<PhoneNumber> {

    @Override
    Class<PhoneNumber> getSource() {
        return PhoneNumber
    }
}
