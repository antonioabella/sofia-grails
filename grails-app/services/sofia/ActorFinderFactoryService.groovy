package sofia

class ActorFinderFactoryService {
    StudentDataService studentDataService
    CustodyDataService custodyDataService
    MainCustodyDataService mainCustodyDataService
    ActorFinder create(String entityName) {
        if (entityName in ['student']) return studentDataService
        if (entityName in ['billing']) return mainCustodyDataService
        if (entityName == 'guardian') return custodyDataService
    }
}