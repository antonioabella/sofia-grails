package sofia

import org.hibernate.criterion.CriteriaSpecification
import org.hibernate.sql.JoinType

class StudentDataService extends GenericData implements ActorFinder {
    @Override
    Class<Student> getSource() {
        return Student
    }

    @Override
    List search(String criteria) {
        conditionalSearch(criteria, true)
    }

    @Override
    List searchInactive(String criteria) {
        conditionalSearch(criteria, false)
    }

    private List conditionalSearch(String criteria, boolean active) {
        source.withCriteria {
            createAlias 'person', 'p'
            projections {
                property 'id', 'id'
                property 'p.id', 'personId'
                property 'active', 'active'
                property 'p.identityCard', 'identityCard'
                property 'p.firstName', 'firstName'
                property 'p.lastName', 'lastName'
            }
            eq "active", active
            or {
                ilike "p.firstName", "%${criteria}%"
                ilike "p.lastName", "%${criteria}%"
                eq "p.identityCard", criteria
            }
            maxResults PEOPLE_LIST_SIZE_LIMIT
            order 'p.lastName'
            order 'p.firstName'
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }

    List getCustodians(Long studentId) {
        source.withCriteria {
            createAlias 'custodians', 'c'
            createAlias 'c.guardian', 'g'
            createAlias 'g.person', 'p'
            projections {
                property 'c.id', 'id'
                property 'g.id', 'actorId'
                property 'id', 'studentId'
                property 'c.main', 'main'
                property 'c.kinship', 'kinship'
                property 'p.firstName', 'firstName'
                property 'p.lastName', 'lastName'
                property 'p.picture', 'picture'
                property 'p.id', 'personId'
            }
            eq 'id', studentId
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }

    List getEnrollments(Long studentId) {
        source.withCriteria {
            createAlias 'enrollments', 'e'
            createAlias 'e.schoolYear', 'y', JoinType.LEFT_OUTER_JOIN
            createAlias 'e.grade', 'g', JoinType.LEFT_OUTER_JOIN
            createAlias 'e.section', 's', JoinType.LEFT_OUTER_JOIN
            projections {
                property 'e.id', 'id'
                property 'g.name', 'grade'
                property 's.name', 'section'
                property 'y.start', 'start'
                property 'y.end', 'end'
                property 'e.withdrawalMonth', 'withdrawalMonth'
                property 'e.withdrawalReason', 'withdrawalReason'
                property 'e.withdrawalDate', 'withdrawalDate'
                property 'e.destinationInstitute', 'destinationInstitute'
            }
            eq 'id', studentId
            order 'y.id', 'desc'
            order 'e.enrollmentDate', 'desc'
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }

    Long getCurrentEnrollmentId(Long studentId) {
        source.createCriteria().get {
            createAlias 'enrollments', 'e'
            createAlias 'e.schoolYear', 'y'
            projections {
                property 'e.id', 'id'
            }
            eq 'id', studentId
            eq 'y.enrollment', true
            isNull 'e.withdrawalMonth'
        }
    }
}
