package sofia

import org.hibernate.criterion.CriteriaSpecification
import sofia.enums.Kinship

class CustodyDataService extends GenericData implements ActorFinder {

    boolean onlyMainGuardians() {
        false
    }

    @Override
    Class getSource() {
        return Custody
    }

    @Override
    List search(String criteria) {
        conditionalSearch(criteria, true)
    }

    @Override
    List searchInactive(String criteria) {
        conditionalSearch(criteria, false)
    }

    Custody setMainCustodian(Long studentId, Long guardianId) {
        Set<Custody> guardians = Student.get(studentId).custodians
        if (!toggleMain(guardians, { Custody it -> it.main })) return null
        return toggleMain(guardians, { Custody it -> it.id == guardianId })
    }

    Custody toggleMain(Set<Custody> guardians, Closure criteria) {
        Custody guardian = guardians.find criteria
        guardian.main ^= true
        return guardian.save()
    }

    Custody save(Student student, Guardian guardian, Kinship kinship) {
        save(create(student: student, guardian: guardian, kinship: kinship))
    }

    private List conditionalSearch(String criteria, boolean active) {
        source.withCriteria {
            createAlias 'guardian', 'g'
            createAlias 'g.person', 'p'
            projections {
                property 'g.id', 'id'
                property 'g.active', 'active'
                property 'p.id', 'personId'
                property 'p.identityCard', 'identityCard'
                property 'p.firstName', 'firstName'
                property 'p.lastName', 'lastName'

            }
            if (onlyMainGuardians()) {
                eq 'main', true
            }
            eq 'g.active', active
            or {
                ilike 'p.firstName', "%$criteria%"
                ilike 'p.lastName', "%$criteria%"
                eq 'p.identityCard', criteria
            }
            maxResults PEOPLE_LIST_SIZE_LIMIT
            order 'p.lastName'
            order 'p.firstName'
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }.unique()
    }
}