package sofia

import grails.rest.RestfulController

class CustodyModelService extends GenericRestModel {
    CustodyDataService custodyDataService

    Custody setMainCustodian(RestfulController listener) {
        Map p = listener.params
        listener.respond source.setMainCustodian(p.studentId as Long, p.id as Long)
    }

    @Override
    protected GenericData getSource() {
        return custodyDataService
    }
}
