package sofia

class PersonDataService extends GenericData<Person> {
    @Override
    Class<Person> getSource() {
        Person.class
    }
}