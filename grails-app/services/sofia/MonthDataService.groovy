package sofia

class MonthDataService extends GenericData<Month> {
    @Override
    Class<Month> getSource() {
        return Month.class
    }
}
