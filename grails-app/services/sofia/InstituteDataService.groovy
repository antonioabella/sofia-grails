package sofia

import groovy.transform.CompileStatic

@CompileStatic
class InstituteDataService extends GenericData<Institute> {

    @Override
    Class<Institute> getSource() {
        return Institute.class
    }
}