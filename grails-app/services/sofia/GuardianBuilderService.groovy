package sofia

class GuardianBuilderService implements ActorBuilder {
    GuardianDataService guardianDataService
    PersonDataService personDataService

    @Override
    Actor save(ActorCommand actorCommand) {
        Person person = personDataService.save(actorCommand.person)
        Guardian guardian = guardianDataService.create()
        if (person) {
            guardian.person = person
            guardian = guardianDataService.save(guardian)
        }
        guardian
    }
}