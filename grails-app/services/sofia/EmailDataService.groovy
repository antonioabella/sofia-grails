package sofia

class EmailDataService extends GenericData<Email> {

    @Override
    Class<Email> getSource() {
        return Email
    }
}
