package sofia

class StudentBuilderService implements ActorBuilder {
    StudentDataService studentDataService
    PersonDataService personDataService

    @Override
    Actor save(ActorCommand actorCommand) {
        Person person = personDataService.save(actorCommand.person)
        Student student = studentDataService.create()
        if (person) {
            student.person = person
            student = studentDataService.save(student)
        }
        student
    }
}