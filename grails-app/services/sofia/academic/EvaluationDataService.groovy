package sofia.academic

import sofia.GenericData

class EvaluationDataService extends GenericData<Evaluation>{
    @Override
    Class<Evaluation> getSource() {
        Evaluation
    }

    List<EvaluationController> findAllByPeriodAndCourse(Long periodId, Long courseId) {
        source.withCriteria {
            period {
                eq 'id', periodId
            }
            course {
                eq 'id', courseId
            }
        }
    }
}