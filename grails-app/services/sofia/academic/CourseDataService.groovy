package sofia.academic

import grails.transaction.Transactional
import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData

class CourseDataService extends GenericData<Course> {

    @Override
    Class<Course> getSource() {
        Course
    }

    List<Course> findAllBySchoolYearAndTeacher(Long schoolYearId, Long teacherId) {
       source.withCriteria {
            createAlias 'subject', 'c'
            createAlias 'section', 's'
            createAlias 's.grade', 'g'
            createAlias 'c.schoolYear', 'y'
            projections {
                property 'id', 'id'
                property 'c.name', 'subject'
                property 's.name', 'section'
                property 'g.name', 'grade'
            }
            eq 'y.id', schoolYearId
            teacher {
                eq 'id', teacherId
            }
            order 'c.name'
            order 'g.name'
            order 's.name'
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}