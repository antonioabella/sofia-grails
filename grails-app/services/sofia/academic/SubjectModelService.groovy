package sofia.academic

import sofia.GenericData
import sofia.GenericRestModel

class SubjectModelService extends GenericRestModel<Subject>{
    SubjectDataService subjectDataService

    @Override
    protected GenericData getSource() {
        subjectDataService
    }

    List<Subject> getBySchoolYearAndGrade(Long schoolYearId, Long gradeId) {
        source.findAllBySchoolYearAndGrade(schoolYearId, gradeId)
    }
}
