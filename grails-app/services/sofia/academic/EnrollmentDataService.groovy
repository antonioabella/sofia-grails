package sofia.academic

import org.hibernate.FetchMode
import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData

class EnrollmentDataService extends GenericData<Enrollment> {
    public Class<Enrollment> getSource() {
        return Enrollment
    }

    List findAllByGuardianIdAndSchoolYearId(Long guardianId, Long schoolYearId) {
        source.withCriteria {
            createAlias 'student', 's'
            createAlias 's.person', 'p'
            createAlias 's.custodians', 'c'
            createAlias 'c.guardian', 'g'
            projections {
                property 'id', 'id'
                property 'p.firstName', 'firstName'
                property 'p.lastName', 'lastName'
                property 'enrollmentDate', 'enrollmentDate'
                property 'enrollmentMonth', 'enrollmentMonth'
                property 'withdrawalDate', 'withdrawalDate'
                property 'withdrawalMonth', 'withdrawalMonth'
            }
            fetchMode 'enrollmentMonth', FetchMode.JOIN
            fetchMode 'withdrawalMonth', FetchMode.JOIN
            schoolYear {
                eq 'id', schoolYearId
            }
            eq 'c.main', true
            eq 'g.id', guardianId
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}
