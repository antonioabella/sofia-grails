package sofia.academic

import sofia.GenericData
import sofia.GenericRestModel

class EvaluationModelService extends GenericRestModel<Evaluation> {
    EvaluationDataService evaluationDataService
    @Override
    protected GenericData getSource() {
        evaluationDataService
    }

    List<Evaluation> findAllByPeriodAndCourse(Long periodId, Long courseId) {
        source.findAllByPeriodAndCourse(periodId, courseId)
    }

    @Override
    protected void discardChildChanges(Evaluation entity) {
        entity.period.discard()
        entity.course.discard()
        entity.week?.discard()
    }
}
