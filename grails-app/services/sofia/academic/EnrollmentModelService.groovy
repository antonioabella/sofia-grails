package sofia.academic

import sofia.*

class EnrollmentModelService extends GenericRestModel<Enrollment> {
    EnrollmentDataService enrollmentDataService
    StudentDataService studentDataService
    InstituteDataService instituteDataService
    SchoolYearDataService schoolYearDataService

    Enrollment getCurrentEnrollmentByStudent(Long studentId) {
        Long enrollmentId = studentDataService.getCurrentEnrollmentId(studentId)
        source.getInstance(enrollmentId)
    }

    List getEnrollmentsByStudent(Long studentId) {
        studentDataService.getEnrollments(studentId)
    }

    private Enrollment setInstitute(Enrollment enrollment) {
        if (!enrollment.institute) enrollment.setInstitute(instituteDataService.getDefault())
        enrollment
    }

    private Enrollment setSchoolYear(Enrollment enrollment) {
        if (!enrollment.schoolYear) enrollment.setSchoolYear(schoolYearDataService.getEnrollmentYear())
        enrollment
    }

    private Enrollment setEnrollmentDate(Enrollment enrollment) {
        if (!enrollment.enrollmentDate) enrollment.enrollmentDate = new Date()
        enrollment
    }

    @Override
    protected void addDefaultValues(Enrollment enrollment) {
        setEnrollmentDate(setInstitute(setSchoolYear(enrollment)))
    }

    @Override
    protected GenericData getSource() {
        return enrollmentDataService
    }

    @Override
    protected void discardChildChanges(Enrollment enrollment) {
        enrollment.grade?.discard()
        enrollment.section?.discard()
        enrollment.enrollmentMonth?.discard()
        enrollment.withdrawalMonth?.discard()
    }
}