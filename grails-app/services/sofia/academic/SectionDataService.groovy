package sofia.academic

import sofia.GenericData

class SectionDataService extends GenericData {

    Class getSource() {
        return Section.class
    }

    List<Section> list(Long schoolYearId) {
        source.withCriteria {
            schoolYear {
                eq "id", schoolYearId
            }
            order 'name'
        }
    }

    List<Section> findAllByGradesAndSchoolYear(List<Long> gradesId, Long schoolYearId) {
        source.withCriteria {
            grade {
                'in' "id", gradesId
            }
            schoolYear {
                eq "id", schoolYearId
            }
        }
    }

    List<Section> findAllBySections(List<Long> sectionsId) {
        List<Section> sections = []
        if (sectionsId.size() > 0) {
            sections = source.withCriteria {
                "in" "id", sectionsId
            }
        }
        sections
    }
}
