package sofia.academic

import sofia.GenericData

class SubjectDataService extends GenericData<Subject> {
    @Override
    Class<Subject> getSource() {
        Subject
    }

    List<Subject> findAllBySchoolYearAndGrade(Long schoolYearId, Long gradeId) {
        source.withCriteria {
            schoolYear {
                eq 'id', schoolYearId
            }
            grade {
                eq 'id', gradeId
            }
        }
    }
}