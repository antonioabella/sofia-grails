package sofia.academic

import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData

class EnrolledCourseDataService extends GenericData<EnrolledCourse> {
    @Override
    Class<EnrolledCourse> getSource() {
        EnrolledCourse
    }

    List<Subject> findAllByCourse(Long courseId) {
        source.withCriteria {
            createAlias 'enrollment', 'e'
            createAlias 'e.enrollmentMonth', 'm'
            createAlias 'e.student', 's'
            createAlias 's.person', 'p'
            projections {
                property 'p.firstName', 'firstName'
                property 'p.lastName', 'lastName'
                property 'e.id', 'enrollmentId'
            }
            course {
                eq 'id', courseId
            }
            order 'm.position'
            order 'p.lastName'
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}
