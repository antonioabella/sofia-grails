package sofia

class ActorBuilderService {
    ActorBuilderFactoryService actorBuilderFactoryService

    Actor save(ActorCommand actorCommand) {
        ActorBuilder actorBuilder = actorBuilderFactoryService.create(actorCommand.entityName)
        return actorBuilder? actorBuilder.save(actorCommand) : null
    }
}