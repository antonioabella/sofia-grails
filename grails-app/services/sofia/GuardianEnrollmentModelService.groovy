package sofia

import sofia.academic.EnrollmentDataService
import sofia.administrative.CommitmentDataService
import sofia.administrative.DiscountDataService
import sofia.administrative.PaidConceptDataService

class GuardianEnrollmentModelService {
    EnrollmentDataService enrollmentDataService
    CommitmentDataService commitmentDataService
    PaidConceptDataService paidConceptDataService
    DiscountDataService discountDataService

    Map findAllByGuardianIdAndSchoolYearId(Long guardianId, Long schoolYearId) {
        List enrollments = enrollmentDataService.findAllByGuardianIdAndSchoolYearId(guardianId, schoolYearId)
        List enrollmentsIds = enrollments*.id
        List commitments = commitmentDataService.findAllByEnrollmentsIds(enrollmentsIds)
        List paidConcepts = paidConceptDataService.findAllByEnrollmentsIds(enrollmentsIds)
        List discounts = discountDataService.findAllByEnrollmentsIds(enrollmentsIds)

        [enrollments: enrollments, commitments: commitments, paidConcepts: paidConcepts, discounts: discounts]
    }
}
