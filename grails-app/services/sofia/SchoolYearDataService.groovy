package sofia

class SchoolYearDataService extends GenericData<SchoolYear> {

    @Override
    Class<SchoolYear> getSource() {
        SchoolYear
    }

    @Override
    SchoolYear getDefault() {
        source.findByActive(true, [cache: true])
    }

    SchoolYear getEnrollmentYear() {
        source.findByEnrollment(true)
    }
}
