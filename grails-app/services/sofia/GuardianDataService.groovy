package sofia

import org.hibernate.criterion.CriteriaSpecification

class GuardianDataService extends GenericData<Guardian> {
    @Override
    Class<Guardian> getSource() {
        return Guardian.class
    }

    List getStudents(Long guardianId) {
        source.withCriteria {
            createAlias 'custodianships', 'c'
            createAlias 'c.student', 's'
            createAlias 's.person', 'p'
            projections {
                property 'c.id', 'id'
                property 's.id', 'actorId'
                property 'id', 'guardianId'
                property 'c.kinship', 'kinship'
                property 'p.firstName', 'firstName'
                property 'p.lastName', 'lastName'
                property 'p.picture', 'picture'
                property 'p.id', 'personId'
            }
            eq 'id', guardianId
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}