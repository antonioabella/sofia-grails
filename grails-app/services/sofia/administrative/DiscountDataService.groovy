package sofia.administrative

import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData

class DiscountDataService extends GenericData<Discount> {
    @Override
    Class getSource() {
        return Discount.class
    }

    List findAllByEnrollmentsIds(List enrollmentsIds) {
        if(enrollmentsIds.size() == 0) return []
        source.withCriteria {
            createAlias 'enrollment', 'e'
            createAlias 'concept', 'c'
            projections {
                property 'id', 'id'
                property 'e.id', 'enrollmentId'
                property 'c.id', 'conceptId'
                property 'amount', 'amount'
                property 'isPercentage', 'isPercentage'
                property 'active', 'active'
            }
            'in' 'e.id', enrollmentsIds
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}
