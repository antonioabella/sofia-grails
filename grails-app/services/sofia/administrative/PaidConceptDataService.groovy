package sofia.administrative

import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData
import sofia.enums.InvoiceStatus

class PaidConceptDataService extends GenericData <PaidConcept> {
    @Override
    Class<PaidConcept> getSource() {
        return PaidConcept
    }

    List findAllByEnrollmentsIds(List enrollmentsIds) {
        if(enrollmentsIds.size() == 0) return []
        source.withCriteria {
            createAlias 'enrollment', 'e'
            createAlias 'invoice', 'i'
            createAlias 'concept', 'c'
            createAlias 'month', 'm'
            projections {
                property 'id', 'id'
                property 'e.id', 'enrollmentId'
                property 'c.id', 'conceptId'
                property 'm.id', 'monthId'
                property 'amount', 'amount'
            }
            'in' 'e.id', enrollmentsIds
            eq 'i.status', InvoiceStatus.PAID
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }

    List getResume(Long billingGroupId, Long schoolYearId, Date from, Date until) {
        PaidConcept.withCriteria {
            createAlias 'invoice', 'i'
            createAlias 'concept', 'c'
            createAlias 'c.billingGroup', 'bg'
            projections {
                groupProperty 'c.name', 'concept'
                sum 'amount', 'amount'
                sum 'discount', 'discount'
            }
            eq 'bg.id', billingGroupId
            if(schoolYearId > 0) {
                enrollment {
                    schoolYear {
                        eq 'id', schoolYearId
                    }
                }
            }
            between 'i.invoiceDate', from, until
            eq 'i.status', InvoiceStatus.PAID
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}
