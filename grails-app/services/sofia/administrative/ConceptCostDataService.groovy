package sofia.administrative

import sofia.GenericData
import org.hibernate.criterion.CriteriaSpecification

class ConceptCostDataService extends GenericData<ConceptCost> {

    @Override
    Class<ConceptCost> getSource() {
        return ConceptCost.class
    }

    List findAllBySchoolYearId(Long schoolYearId) {
        source.withCriteria {
            createAlias 'month', 'm'
            createAlias 'concept', 'c'
            projections {
                property 'm.id', 'monthId'
                property 'm.position', 'monthPosition'
                property 'c.id', 'conceptId'
                property 'conceptCostType', 'conceptCostType'
                property 'amount', 'amount'
            }
            schoolYear {
                eq 'id', schoolYearId
            }
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}
