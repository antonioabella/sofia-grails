package sofia.administrative

import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData

class PaymentItemDataService extends GenericData<PaymentItem> {

    @Override
    Class<PaymentItem> getSource() {
        return PaymentItem.class
    }

    List getResume(List<Long> paymentsIds) {
        if (paymentsIds.size() == 0) return []
        source.withCriteria {
            createAlias 'paymentMethod', 'pm'
            projections {
                groupProperty 'pm.name', 'paymentMethod'
                sum 'amount', 'amount'
            }
            payment {
                'in' 'id', paymentsIds
            }
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}