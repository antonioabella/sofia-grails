package sofia.administrative

import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData

class PaymentDataService extends GenericData<Payment> {
    @Override
    Class<Payment> getSource() {
        Payment.class
    }

    List<Long> getPaymentsIds(List<Long> invoicesIds) {
        if (invoicesIds.size() == 0) return []
        source.withCriteria {
            projections {
                property 'id'
            }
            invoices {
                'in' 'id', invoicesIds
            }
        }.unique()
    }
}
