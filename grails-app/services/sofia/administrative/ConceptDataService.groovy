package sofia.administrative

import sofia.GenericData

class ConceptDataService extends GenericData<Concept> {

    @Override
    Class<Concept> getSource() {
        return Concept.class
    }
}
