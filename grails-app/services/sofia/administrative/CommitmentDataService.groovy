package sofia.administrative

import org.hibernate.FetchMode
import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData

class CommitmentDataService extends GenericData<Commitment> {
    @Override
    Class<Commitment> getSource() {
        return Commitment
    }

    List findAllByEnrollmentsIds(List enrollmentsIds) {
        if(enrollmentsIds.size() == 0) return []
        source.withCriteria {
            createAlias 'enrollment', 'e'
            createAlias 'concept', 'c'
            projections {
                property 'id', 'id'
                property 'e.id', 'enrollmentId'
                property 'c.id', 'conceptId'
                property 'startDate', 'startDate'
                property 'enrollmentMonth', 'enrollmentMonth'
                property 'endDate', 'endDate'
                property 'withdrawalMonth', 'withdrawalMonth'
            }
            fetchMode 'enrollmentMonth', FetchMode.JOIN
            fetchMode 'withdrawalMonth', FetchMode.JOIN
            'in' 'e.id', enrollmentsIds
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}
