package sofia.administrative

import org.hibernate.criterion.CriteriaSpecification
import sofia.GenericData
import sofia.enums.InvoiceStatus

class InvoiceDataService extends GenericData<Invoice> {

    List getResume(Long billingGroupId, Long schoolYearId, Date from, Date until) {
        source.withCriteria {
            projections {
                count 'id', 'count'
                min 'invoiceNumber', 'fromNumber'
                max 'invoiceNumber', 'toNumber'
                sum 'total', 'amount'
                sum 'discount', 'discount'
                groupProperty 'status', 'status'
            }
            billingGroup {
                eq 'id', billingGroupId
            }
            if (schoolYearId > 0) {
                enrollment {
                    schoolYear {
                        eq 'id', schoolYearId
                    }
                }
            }
            between 'invoiceDate', from, until
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }

    @Override
    Class<Invoice> getSource() {
        Invoice.class
    }

    List<Long> getPaidInvoicesIds(Long billingGroupId, Long schoolYearId, Date from, Date until) {
        source.withCriteria {
            projections {
                property 'id'
            }
            billingGroup {
                eq 'id', billingGroupId
            }
            if (schoolYearId > 0) {
                enrollment {
                    schoolYear {
                        eq 'id', schoolYearId
                    }
                }
            }
            between 'invoiceDate', from, until
            eq 'status', InvoiceStatus.PAID
        }
    }
}
