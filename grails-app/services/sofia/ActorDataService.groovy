package sofia

import org.hibernate.criterion.CriteriaSpecification
import org.hibernate.sql.JoinType

class ActorDataService extends GenericData<Actor> {
    Class<Actor> getSource() {
        return Actor
    }

    Map get(Long id) {
        return source.createCriteria().get {
            createAlias 'person', 'p'
            createAlias 'p.phoneNumbers', 'pn', JoinType.LEFT_OUTER_JOIN
            createAlias 'p.emails', 'e', JoinType.LEFT_OUTER_JOIN
            projections {
                property 'class', 'class'
                property 'id', 'id'
                property 'p.id', 'personId'
                property 'active', 'active'
                property 'p.outsider', 'outsider'
                property 'p.identityCard', 'identityCard'
                property 'p.gender', 'gender'
                property 'p.firstName', 'firstName'
                property 'p.lastName', 'lastName'
                property 'p.address', 'address'
                property 'p.birthDate', 'birthDate'
                property 'p.birthCountry', 'birthCountry'
                property 'p.picture', 'picture'
                property 'p.notes', 'notes'
                property 'p.facebook', 'facebook'
                property 'p.twitter', 'twitter'
                property 'pn.number', 'phoneNumber'
                property 'e.address', 'emailAddress'
            }
            eq "id", id
            and {
                or {
                    eq 'pn.main', true
                    isNull 'pn.id'
                }
            }
            and {
                or {
                    eq 'e.main', true
                    isNull 'e.id'
                }
            }
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }

    Long getMainPhoneNumberId(Long actorId) {
        return source.createCriteria().get {
            createAlias 'person', 'p'
            createAlias 'p.phoneNumbers', 'pn', JoinType.LEFT_OUTER_JOIN
            projections {
                property 'pn.id', 'phoneId'
            }
            eq 'id', actorId
            eq 'pn.main', true
        } as Long
    }

    List getPhoneNumbers(Long actorId) {
        return source.withCriteria {
            createAlias 'person', 'p'
            createAlias 'p.phoneNumbers', 'pn', JoinType.LEFT_OUTER_JOIN
            projections {
                property 'pn.id', 'id'
                property 'pn.number', 'number'
                property 'pn.phoneType', 'phoneType'
                property 'pn.main', 'main'
                property 'id', 'actorId'
            }
            eq 'id', actorId
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }

    Long getMainEmailId(Long actorId) {
        return source.createCriteria().get {
            createAlias 'person', 'p'
            createAlias 'p.emails', 'e', JoinType.LEFT_OUTER_JOIN
            projections {
                property 'e.id', 'emailId'
            }
            eq 'id', actorId
            eq 'e.main', true
        } as Long
    }

    List getEmails(Long actorId) {
        return source.withCriteria {
            createAlias 'person', 'p'
            createAlias 'p.emails', 'e', JoinType.LEFT_OUTER_JOIN
            projections {
                property 'e.id', 'id'
                property 'e.address', 'address'
                property 'e.main', 'main'
                property 'id', 'actorId'
            }
            eq 'id', actorId
            resultTransformer CriteriaSpecification.ALIAS_TO_ENTITY_MAP
        }
    }
}