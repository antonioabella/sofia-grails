package sofia

import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

class ActorModelService extends GenericRestModel {
    ActorDataService actorDataService
    PhoneNumberDataService phoneNumberDataService
    EmailDataService emailDataService

    @Override
    protected GenericData getSource() {
        return actorDataService
    }

    PhoneNumber getMainPhoneNumber(Long actorId) {
        Long id = source.getMainPhoneNumberId(actorId)
        phoneNumberDataService.getInstance(id)
    }

    List<PhoneNumber> getPhoneNumbers(Long actorId) {
        source.getPhoneNumbers(actorId)
    }

    Email getMainEmail(Long actorId) {
        Long id = source.getMainEmailId(actorId)
        emailDataService.getInstance(id)
    }

    List<Email> getEmails(Long actorId) {
        source.getEmails(actorId)
    }

    void removePhone(Person person, PhoneNumber phoneNumber) {
        person.removeFromPhoneNumbers(phoneNumber)
    }

    void removeEmail(Person person, Email email) {
        person.removeFromEmails(email)
    }

    void updatePhone(Person person, PhoneNumber phoneNumber) {
        checkMain(person.phoneNumbers, phoneNumber, phoneNumberDataService.&save)
        phoneNumberDataService.save(phoneNumber)
    }

    private void checkMain(def contacts, def instance, Closure saver) {
        def theMain = contacts.find { it.main }
        if (instance.main && theMain?.id != instance.id) {
            theMain.main = false
            saver theMain
        }
    }

    void updateEmail(Person person, Email email) {
        checkMain(person.emails, email, emailDataService.&save)
        emailDataService.save(email)
    }

    void deleteContact(def listener) {
        def (boolean notFound, instance, Actor actor) = checkExists(listener)
        if (notFound) return
        listener.deleteContact actor.person, instance
        listener.render status: NO_CONTENT
    }

    void updateContact(def listener) {
        def (boolean notFound, instance, Actor actor) = checkExists(listener)
        if (notFound) return
        instance.properties = listener.getObjectToBind()

        if (instance.hasErrors()) {
            listener.respond instance, status: METHOD_NOT_ALLOWED
            return
        }

        listener.updateContact actor.person, instance
        listener.render status: OK
    }

    private List checkExists(listener) {
        boolean notFound
        Map params = listener.params
        Actor actor = actorDataService.getInstance(params.actorId as Long)
        def instance = listener.queryForResource(params.id)
        if (instance == null || actor == null) {
            listener.notFound()
            notFound = true
        }
        return [notFound, instance, actor]
    }
}