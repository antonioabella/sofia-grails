package sofia

class GradeDataService extends GenericData<Grade> {
    @Override
    Class<Grade> getSource() {
        return Grade.class
    }
}
