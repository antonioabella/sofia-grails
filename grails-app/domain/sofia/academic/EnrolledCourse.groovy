package sofia.academic

import groovy.transform.ToString

@ToString
class EnrolledCourse {
    Course course
    Enrollment enrollment
}