package sofia.academic

import sofia.Period
import sofia.PeriodWeek

class Evaluation {
    Course course
    Period period
    PeriodWeek week
    String content
    String instrument
    int weight
    Map results
    Map remarks

    static constraints = {
        week nullable: true
        content nullable: true
        weight min: 0, max: 100,
                validator: { weight, evaluation ->
                    def query = Evaluation.where {
                        period.id == evaluation.period.id && course.id == evaluation.course.id
                    }
                    if (evaluation.id != null) query = query.where { id != evaluation.id }
                    def sum = query.sum('weight').get()
                    if (sum == null) return true
                    return sum + weight <= 100
                }
    }
}
