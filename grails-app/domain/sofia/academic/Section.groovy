package sofia.academic

import groovy.transform.ToString
import sofia.Grade
import sofia.SchoolYear

@ToString
class Section {

    static auditable
    static hasMany = [enrollments: Enrollment]
    String name
    Grade grade
    SchoolYear schoolYear

    boolean isActive() {
        schoolYear.active
    }

    static mapping = {
        cache true
        sort "grade"
    }

    static constraints = {
        schoolYear(nullable: false)
        grade(nullable: false)
        name(blank: false, unique: ['schoolYear', 'grade'])
    }
}
