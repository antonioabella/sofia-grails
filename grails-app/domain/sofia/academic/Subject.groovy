package sofia.academic

import sofia.Grade
import sofia.SchoolYear

class Subject {
    static hasMany = [subjects: Subject]
    Subject parentSubject
    String name
    SchoolYear schoolYear
    Grade grade
    private boolean multiSubject

    void setMultiSubject(boolean value) {
        this.multiSubject = isMultiSubject()
    }

    boolean isMultiSubject() {
        subjects?.size() > 0
    }

    static constraints = {
        parentSubject nullable: true
    }
}