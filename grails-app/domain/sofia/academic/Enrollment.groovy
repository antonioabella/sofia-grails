package sofia.academic

import groovy.transform.ToString
import sofia.*
import sofia.administrative.Commitment
import sofia.administrative.Discount
import sofia.administrative.PaidConcept
import sofia.administrative.Scholarship

@ToString
class Enrollment {

    static auditable
    static hasMany = [
            scholarships: Scholarship,
            commitments : Commitment,
            discounts   : Discount,
            paidConcepts: PaidConcept
    ]

    Student student
    Section section
    Institute institute
    Grade grade
    SchoolYear schoolYear
    Month enrollmentMonth
    Month withdrawalMonth
    Date enrollmentDate
    Date withdrawalDate
    String withdrawalReason
    String destinationInstitute
    String originInstitute
    Boolean sofia = false

    static constraints = {
        section nullable: true
        withdrawalMonth nullable: true
        withdrawalReason nullable: true
        withdrawalDate nullable: true
        destinationInstitute nullable: true
        originInstitute nullable: true
    }
}
