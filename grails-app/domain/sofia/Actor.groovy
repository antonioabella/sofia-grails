package sofia

import groovy.transform.ToString
import sofia.security.User

@ToString
class Actor {
    Person person
    User user
    boolean active = true

    void setActive(boolean value) {
        active = value
        if (user) {
            user.enabled = value
        }
    }

    void setUser(User value) {
        user = value
        if (value) {
            value.actor = this
        }
    }

    static constraints = {
        user(nullable: true)
    }
}
