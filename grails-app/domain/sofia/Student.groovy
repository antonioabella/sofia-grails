package sofia

import groovy.transform.ToString
import sofia.academic.Enrollment
import sofia.enums.Kinship

@ToString
class Student extends Actor {
    static hasMany = [custodians: Custody, enrollments: Enrollment]

    transient Guardian getMainGuardian() {
        return custodians.find { it.main }?.guardian
    }

    transient boolean isFatherOrMotherDuplicated(Kinship kinship) {
        custodians.find {
            it.kinship == kinship && (it.kinship == Kinship.FATHER || it.kinship == Kinship.MOTHER)
        }
    }
}