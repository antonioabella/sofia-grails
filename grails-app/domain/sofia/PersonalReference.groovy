package sofia

import groovy.transform.ToString
import sofia.enums.Kinship

@ToString
class PersonalReference {

    static belongsTo = [person: Person]
    String name
    String phoneNumber
    Kinship kinship

    static constraints = {
        name(blank: false, nullable: false)
        phoneNumber(blank: false, nullable: false)
        kinship(blank: false, nullable: false)
    }
}