package sofia

import groovy.transform.ToString

@ToString
class Email {
    String address
    Boolean main = false

    static constraints = {
        address(email: true, blank: false)
    }
}
