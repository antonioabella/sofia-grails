package sofia

import groovy.transform.ToString
import sofia.enums.PhoneType

@ToString
class PhoneNumber {

    PhoneType phoneType
    String number
    Boolean main = false

    static constraints = {
        number(blank: false, nullable: false)
        phoneType(nullable: false)
    }
}
