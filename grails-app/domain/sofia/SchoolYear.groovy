package sofia

import groovy.transform.Sortable
import groovy.transform.ToString

@Sortable(includes = ['start'])
@ToString
class SchoolYear {

    Date start
    Date end
    SchoolYear next
    SchoolYear previous
    boolean active
    boolean enrollment

    static mapping = {
        cache true
        active index: true
        enrollment index: true
    }

    static constraints = {
        start nullable: false
        end nullable: false
        next nullable: true
        previous nullable: true
        enrollment(
                validator: { val, obj ->
                    if (!val) {
                        return true
                    }
                    def enrollmentYear = SchoolYear.findByEnrollment(true)
                    return enrollmentYear?.id == obj?.id
                }
        )
        active(
                validator: { val, obj ->
                    if (!val) {
                        return true
                    }
                    SchoolYear activeYear = SchoolYear.findByActive(true)
                    return activeYear?.id == obj?.id
                }
        )
    }
}