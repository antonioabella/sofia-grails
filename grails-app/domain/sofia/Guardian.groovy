package sofia

import groovy.transform.ToString

@ToString
class Guardian extends Actor {

    static hasMany = [custodianships: Custody]

    String occupation
    String workPlace
    String position

    static constraints = {
        occupation nullable: true
        workPlace nullable: true
        position nullable: true
    }
}
