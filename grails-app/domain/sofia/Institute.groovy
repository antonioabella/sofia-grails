package sofia

import groovy.transform.ToString
import sofia.enums.BillingType

@ToString
class Institute {

    static hasMany = [
            phoneNumbers: PhoneNumber,
            emails: Email,
            principals: Principal,
    ]

    static fetchMode = [phoneNumbers: "eager", emails: "eager"]

    String name
    String taxId
    // TODO: This not belongs to Institute. Use InvoiceConfig instead
    BillingType billingType
    String address
    String code

    static mapping = {
        cache true
    }

    static constraints = {
        name(blank: false)
        taxId(blank: false)
        address(blank: false)
        billingType(nullable: false)
        code(nullable: true)
    }
}
