package sofia

import groovy.transform.ToString
import sofia.enums.Gender
import sofia.enums.PhoneType

@ToString
class Person {

    static auditable
    static hasMany = [phoneNumbers: PhoneNumber, emails: Email, personalReferences: PersonalReference]

    String identityCard
    String firstName
    String lastName
    Date birthDate
    String birthCountry
    String address
    Gender gender
    String notes
    String picture
    String facebook
    String twitter
    Boolean outsider = false

    String getPhoneNumber() {
        phoneNumbers.find { it.main }?.number
    }

    void setPhoneNumber(String value) {
        turnOffOldPrincipal phoneNumbers
        if (numberExists(value)) return
        addToPhoneNumbers(number: value, phoneType: PhoneType.MOBILE, main: true)
    }

    String getEmailAddress() {
        emails.find { it.main }?.address
    }

    void setEmailAddress(String value) {
        turnOffOldPrincipal emails
        if (emailExists(value)) return
        addToEmails(address: value, main: true)
    }

    private void changeAsMain(foundElement) {
        foundElement.main = true
        foundElement.save()
    }

    private void turnOffOldPrincipal(elements) {
        def oldPrincipal = elements.find { it.main }
        oldPrincipal?.main = false
        oldPrincipal?.save()
    }

    private PhoneNumber numberExists(String value) {
        PhoneNumber foundNumber = phoneNumbers.find { it.number == value }
        if (foundNumber) this.changeAsMain(foundNumber)
        foundNumber
    }

    private Email emailExists(String value) {
        Email emailFounded = emails.find { it.address == value }
        if (emailFounded) changeAsMain emailFounded
        emailFounded
    }

    static constraints = {
        identityCard blank: false, unique: true, matches: /\d+/
        firstName blank: false, index: 'first_name'
        lastName blank: false, index: 'last_name'
        birthDate nullable: false
        birthCountry nullable: false
        address blank: false, nullable: false
        gender nullable: false
        notes nullable: true, maxSize: 1000
        picture nullable: true
        facebook nullable: true
        emailAddress email: true, nullable: true
        phoneNumber nullable: true
        twitter nullable: true
    }

    static transients = ['emailAddress', 'phoneNumber']

    static mapping = {
        sort "lastName"
        phoneNumbers cascade: "all-delete-orphan"
        emails cascade: "all-delete-orphan"
        personalReferences cascade: "all-delete-orphan"
    }
}