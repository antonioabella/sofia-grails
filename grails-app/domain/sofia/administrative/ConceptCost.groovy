package sofia.administrative

import groovy.transform.ToString
import sofia.Month
import sofia.SchoolYear
import sofia.enums.ConceptCostType

@ToString
class ConceptCost {

    static belongsTo = [concept: Concept]
    ConceptCostType conceptCostType
    Month month
    SchoolYear schoolYear
    BigDecimal amount

    static mapping = { cache true }

    static constraints = {
        schoolYear nullable: false
//        concept nullable: false, unique: ['conceptCostType', 'schoolYear', 'month']
        amount nullable: false
    }
}
