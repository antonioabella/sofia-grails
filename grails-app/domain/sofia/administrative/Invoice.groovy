package sofia.administrative

import groovy.transform.ToString
import sofia.academic.Enrollment
import sofia.enums.InvoiceStatus

@ToString(includes = ['invoiceNumber'])
class Invoice {

    static auditable
    static hasMany = [paidConcepts: PaidConcept]

    BillingGroup billingGroup
    Enrollment enrollment
    Payment payment
    InvoiceStatus status
    Long invoiceNumber
    Date invoiceDate
    BigDecimal subtotal = 0
    BigDecimal discount = 0
    BigDecimal total = 0
    String payer
    String payerIdentity
    String taxDomicile
    Date paymentDate

    static constraints = {
        payment nullable: true
        invoiceNumber nullable: false
        subtotal blank: false, nullable: false
        total blank: false, nullable: false
        payer blank: false, nullable: false
        payerIdentity blank: false, nullable: false
        taxDomicile blank: false, nullable: false
        invoiceDate nullable: false
        paymentDate nullable: true
        status nullable: false
    }

    static mapping = {
        cache true
        paidConcepts sort: 'month', cascade: 'all'
    }
}
