package sofia.administrative

import groovy.transform.ToString

@ToString
class PaymentMethod {
    String name

    static constraints = {
        name nullable: false
    }
}


