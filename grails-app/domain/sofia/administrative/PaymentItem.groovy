package sofia.administrative

import groovy.transform.ToString

@ToString
class PaymentItem {

    static belongsTo = [payment: Payment]

    Bank bank
    PaymentMethod paymentMethod
    BigDecimal amount = 0
    String identifier

    static constraints = {
        paymentMethod nullable: false
        bank nullable: true
        identifier nullable: true
    }
}