package sofia.administrative

import groovy.transform.ToString

@ToString(includes = ['name'])
class Concept {
    BillingGroup billingGroup
    String name
    Boolean variableCost = false
    Boolean optional = false
    Boolean printable = true
    Boolean household = false
    Boolean active = true
    Boolean general = true

    static mapping = {
        sort 'name'
        cache true
    }

    static constraints = {
        name blank: false, nullable: false
        billingGroup nullable: false
        printable nullable: false
        optional nullable: false
        variableCost display: false, nullable: false
        active nullable: false
        household nullable: false
        general nullable: false
    }
}
