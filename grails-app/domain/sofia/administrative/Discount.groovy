package sofia.administrative

import sofia.academic.Enrollment

class Discount {
    Enrollment enrollment
    Concept concept
    BigDecimal amount
    boolean isPercentage = true
    boolean active
}
