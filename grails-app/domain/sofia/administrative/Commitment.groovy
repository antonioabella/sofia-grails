package sofia.administrative

import groovy.transform.ToString
import sofia.Month
import sofia.academic.Enrollment

@ToString
class Commitment {

    static belongsTo = [enrollment: Enrollment]

    Concept concept
    Month enrollmentMonth
    Date startDate
    Month withdrawalMonth
    Date endDate
    Boolean active = true

    static constraints = {
        enrollment nullable: false
        concept nullable: false
        enrollmentMonth nullable: false
        startDate nullable: false
        withdrawalMonth nullable: true
        endDate nullable: true
    }
}
