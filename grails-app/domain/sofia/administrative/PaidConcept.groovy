package sofia.administrative

import groovy.transform.ToString
import sofia.Month
import sofia.academic.Enrollment

@ToString
class PaidConcept {

    static belongsTo = [invoice: Invoice, enrollment: Enrollment]

    Concept concept
    BigDecimal amount
    BigDecimal discount = 0
    Month month

    static mapping = {
        sort month: "desc"
    }

    static constraints = {
        amount(nullable: false)
        concept(nullable: false)
        month(nullable: false)
    }
}
