package sofia.administrative

import groovy.transform.ToString

@ToString
class Sponsor {

    static auditable

    String name
    String taxId
    String taxDomicile

    static mapping = {
        sort "name"
        cache true
    }

    static constraints = {
        name(blank: false, nullable: false)
        taxId(blank: false, nullable: false)
        taxDomicile(blank: false, nullable: false)
    }
}
