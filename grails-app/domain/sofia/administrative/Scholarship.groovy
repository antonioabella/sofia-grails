package sofia.administrative

import groovy.transform.ToString
import sofia.academic.Enrollment

@ToString
class Scholarship {

    static belongsTo = [enrollment: Enrollment, sponsor: Sponsor, concept: Concept]

    BigDecimal amount
    boolean isPercentage = true
    boolean active = true

    static constraints = {
        amount nullable: false, validator: validScholarship
        isPercentage validator: validScholarship
    }

    static validScholarship = { value, scholarship ->
        def amount
        def percentage
        if (value instanceof Boolean) {
            percentage = value
            amount = scholarship.amount
        } else {
            percentage = scholarship.isPercentage
            amount = value
        }
        if (!amount) return true
        percentage ? amount > 0G && amount <= 100G : amount > 0G
    }
}
