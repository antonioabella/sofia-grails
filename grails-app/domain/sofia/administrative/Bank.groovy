package sofia.administrative

import groovy.transform.ToString

@ToString
class Bank {
    String name

    static constraints = {
        name nullable: false, blank: false
    }
}
