package sofia.administrative

import groovy.transform.ToString

@ToString(includes = ['date'])
class Payment {

    static hasMany = [paymentItems: PaymentItem, invoices: Invoice]

    Date paymentDate

    static constraints = {
        paymentDate nullable: false
    }
}