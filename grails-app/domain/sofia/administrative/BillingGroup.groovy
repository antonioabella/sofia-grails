package sofia.administrative

import groovy.transform.ToString

@ToString(includePackage = false, includes = ['name'])
class BillingGroup {
    String name
    long currentNumber

    static constraints = {
        name(blank: false)
    }
}