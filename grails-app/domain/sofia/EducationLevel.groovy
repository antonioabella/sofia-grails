package sofia

import groovy.transform.ToString

@ToString
class EducationLevel {
    static hasMany = [grades: Grade]
    String name

    static mapping = {
        cache true
        grades sort: "id"
    }
}
