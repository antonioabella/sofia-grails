package sofia

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@ToString
@EqualsAndHashCode
class Month {
    public static final long ENROLLMENT = 0
    public static final long START = 1
    public static final long END = 12
    String name
    long position
    int systemId

    static mapping = { cache true }

    static constraints = {
        position(nullable: false, unique: true)
        systemId(nullable: false, unique: true)
        name(blank: false)
    }
}