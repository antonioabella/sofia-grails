package sofia

import groovy.transform.ToString

@ToString
class Principal {
    Teacher teacher
    Date startDate
    Date leavingDate
    Boolean active
}
