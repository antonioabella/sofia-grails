package sofia

class PeriodWeek {
    Period period
    Date start
    Date end

    String toString() {
        String df = 'dd MMM'
        "${start.format(df)}/${end.format(df)}"
    }
}