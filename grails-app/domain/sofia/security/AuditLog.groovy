package sofia.security

import groovy.transform.ToString

@ToString
class AuditLog {
    Date dateCreated
    String entityName
    Long entityId
    String eventType
    User user

    static constraints = {
        user nullable: true
        eventType inList: ['INSERT', 'UPDATE', 'DELETE']
    }
}
