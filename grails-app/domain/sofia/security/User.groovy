package sofia.security

import groovy.transform.ToString
import sofia.Actor

@ToString(includes = ['username'])
class User {
    Actor actor
    static auditable
    String username
    String password
    String email
    boolean enabled
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    transient String getFullName() {
        return actor?.toString() ?: username
    }

    static constraints = {
        username blank: false, unique: true
        password blank: false
        email email: true, blank: false
        actor nullable: true
    }

    static mapping = {
        actor nullable: true
        password column: '`password`'
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }
}
