package sofia.security

class Role {

	String authority

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}
	
	String toString(){
		return authority[5..-1] // Returns role name without ROLE_ prefix
	}
}
