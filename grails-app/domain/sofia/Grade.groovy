package sofia

import groovy.transform.ToString

@ToString
class Grade {
    EducationLevel level
    String name
    Grade previous
    Grade next
    int position = 0

    static constraints = {
        name nullable: false
        previous nullable: true
        next nullable: true
    }
}
