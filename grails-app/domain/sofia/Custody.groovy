package sofia

import groovy.transform.ToString
import sofia.enums.Kinship

@ToString
class Custody {

    Student student
    Guardian guardian
    Kinship kinship
    boolean main = false

    boolean validKinship() {
        if (student.isFatherOrMotherDuplicated(kinship)) {
            this.getErrors().rejectValue("kinship", "guardian.parents.duplicates")
            return false
        }
        true
    }

    void setAsMain() {
        Custody custody = student.custodians.find { it.main }
        if (custody != this) {
            withTransaction {
                if (custody) {
                    custody.main = false
                    custody.save()
                }
                this.main = true
                this.save()
            }
        }
    }
}
