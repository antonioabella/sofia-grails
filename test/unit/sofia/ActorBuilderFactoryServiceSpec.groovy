package sofia

import sofia.test.StudentBuilder
import spock.lang.Specification

class ActorBuilderFactoryServiceSpec extends Specification {
    void 'return correct implementation of ActorBuilder'() {
        ActorBuilderFactoryService service = new ActorBuilderFactoryService()
        service.studentBuilderService = new StudentBuilderService()
        service.guardianBuilderService = new GuardianBuilderService()

        given:
        ActorBuilder actorBuilder

        when:
        actorBuilder = service.create('student')

        then:
        actorBuilder instanceof StudentBuilderService

        when:
        actorBuilder = service.create('guardian')

        then:
        actorBuilder instanceof GuardianBuilderService
    }
}
