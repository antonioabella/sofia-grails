package sofia

import spock.lang.Specification

class ActorFinderFactoryServiceSpec extends Specification {
    void 'return correct implementation of PeopleFinder'() {
        ActorFinderFactoryService service = new ActorFinderFactoryService()
        service.studentDataService = new StudentDataService()
        service.custodyDataService = new CustodyDataService()
        service.mainCustodyDataService = new MainCustodyDataService()

        given:
        ActorFinder peopleFinder

        when:
        peopleFinder = service.create("billing")

        then:
        peopleFinder instanceof MainCustodyDataService

        when:
        peopleFinder = service.create("student")

        then:
        peopleFinder instanceof StudentDataService

        when:
        peopleFinder = service.create("guardian")

        then:
        peopleFinder instanceof CustodyDataService
    }
}
