package sofia

import sofia.enums.Kinship
import spock.lang.Specification

class CustodySpec extends Specification {
    Student student = Mock()
    Custody custody
    Kinship kinship

    void setup() {
        custody = new Custody(student: student, kinship: kinship)
    }

    void "testParentescoValido devuelve false y carga errores si Padre o Madre están repetidos"() {

        when:
        _ * student.isFatherOrMotherDuplicated(kinship) >> kinshipRepetido
        custody.validKinship() != kinshipRepetido

        then:
        (custody.errors.errorCount > 0) == kinshipRepetido

        where:
        kinshipRepetido << [true, false]
    }


    void "setAsPrincipal set guardian as principal" (){
        Custody guardian1 = new Custody(main: true, student: student)
        Custody guardian2 = new Custody(main: false, student: student)
        ArrayList<Custody> custodies = [guardian1, guardian2]

        when:
        guardian2.setAsMain()

        then:
        1 * student.custodians >> custodies
        guardian2.main
    }
}
