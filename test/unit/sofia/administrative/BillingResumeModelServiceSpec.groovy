package sofia.administrative

import spock.lang.Specification

class BillingResumeModelServiceSpec extends Specification {
    BillingResumeModelService service = new BillingResumeModelService()
    PaidConceptDataService paidConceptDataService = Mock()
    InvoiceDataService invoiceDataService = Mock()
    PaymentItemDataService paymentItemDataService = Mock()
    PaymentDataService paymentDataService = Mock()

    void 'getResume return invoiceResume and paidConceptResume'() {
        given:
        service.invoiceDataService = invoiceDataService
        service.paidConceptDataService = paidConceptDataService
        service.paymentItemDataService = paymentItemDataService
        service.paymentDataService = paymentDataService
        Date from = Stub()
        List<Long> invoicesIds = [1L, 2L, 3L]
        List<Long> paymentsIds = [1L, 2L, 3L]
        Date to = Stub()
        Long schoolYearId = 6L
        Long billingGroupId = 6L
        List paidConceptStub = Stub()
        List invoiceStub = Stub()
        List paymentItemsStub = Stub()

        when:
        Map resume = service.getResume(billingGroupId, schoolYearId, from, to)

        then:
        1 * invoiceDataService.getResume(billingGroupId, schoolYearId, from, to) >> invoiceStub
        1 * invoiceDataService.getPaidInvoicesIds(billingGroupId, schoolYearId, from, to) >> invoicesIds
        1 * paidConceptDataService.getResume(billingGroupId, schoolYearId, from, to) >> paidConceptStub
        1 * paymentDataService.getPaymentsIds(invoicesIds) >> paymentsIds
        1 * paymentItemDataService.getResume(paymentsIds) >> paymentItemsStub

        resume.invoiceResume == invoiceStub
        resume.paidConceptResume == paidConceptStub
        resume.paymentResume == paymentItemsStub
    }
}
