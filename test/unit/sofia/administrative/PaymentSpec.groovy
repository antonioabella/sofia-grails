package sofia.administrative

import spock.lang.Specification

class PaymentSpec extends Specification {

    void "validation constraints"() {
        Payment payment = new Payment()

        when:
        payment.validate()

        then:
        payment.errors.errorCount == 1

        when:
        payment.paymentDate = new Date()
        payment.validate()

        then:
        payment.errors.errorCount == 0
    }
}
