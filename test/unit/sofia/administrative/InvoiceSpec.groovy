package sofia.administrative

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import sofia.Email
import sofia.Month
import sofia.Person
import sofia.PhoneNumber
import sofia.SchoolYear
import sofia.Student
import sofia.academic.Enrollment
import sofia.enums.InvoiceStatus
import sofia.test.MonthsSamplesFactory
import spock.lang.Specification
import spock.lang.Subject

@Mock([Student, Concept, PaidConcept, Invoice, Enrollment, Month, SchoolYear, Person, Email, PhoneNumber])
@Build([Student, Concept, Enrollment, SchoolYear, Person])
@TestFor(Invoice)
class InvoiceSpec extends Specification {

    void 'validate constraints'() {
        MonthsSamplesFactory msf = new MonthsSamplesFactory()
        Invoice invoice = new Invoice()

        when:
        invoice.validate()

        then:
        invoice.errors.errorCount == 8

        when:
        invoice.with {
            invoiceDate = new Date()
            invoiceNumber = 54
            payer = "Antonio"
            payerIdentity = "J45"
            taxDomicile = "Caracas"
            status = InvoiceStatus.PAID
            enrollment = createEnrollment("1", msf.ene)
            billingGroup = new BillingGroup(name: 'Name', currentNumber: 0)
        }
        invoice.validate()

        then:
        invoice.errors.errorCount == 0
    }

    def createEnrollment(identityCard, month) {
        def student = Student.build(person: Person.build(identityCard: identityCard,
                emailAddress: 'irrelevant@email.com'))
        def schoolYear = SchoolYear.build(start: new Date())
        Enrollment.build(student: student, schoolYear: schoolYear, enrollmentMonth: month)
    }
}
