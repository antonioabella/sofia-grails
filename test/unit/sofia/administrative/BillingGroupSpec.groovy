package sofia.administrative

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(BillingGroup)
class BillingGroupSpec extends Specification {

    void 'Name cannot be blank'() {
        mockForConstraintsTests(BillingGroup)

        when:
        def billingGroup = new BillingGroup()
        billingGroup.validate()

        then:
        billingGroup.errors.hasFieldErrors('name')
    }

    void "toString returns name"() {
        String name = "Name"

        expect:
        new BillingGroup(name: name).toString() == "BillingGroup($name)"
    }
}