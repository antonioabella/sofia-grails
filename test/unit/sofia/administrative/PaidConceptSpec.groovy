package sofia.administrative

import grails.test.mixin.TestFor
import sofia.Month
import sofia.academic.Enrollment
import spock.lang.Specification

@TestFor(PaidConcept)
class PaidConceptSpec extends Specification {

    void 'validate constraints'() {
        PaidConcept paidConcept = new PaidConcept()

        when:
        paidConcept.validate()

        then:
        paidConcept.errors.errorCount == 5

        when:
        paidConcept.invoice = new Invoice()
        paidConcept.amount = 5G
        paidConcept.enrollment = new Enrollment()
        paidConcept.month = new Month()
        paidConcept.concept = new Concept()
        paidConcept.validate()

        then:
        paidConcept.errors.errorCount == 0
    }
}