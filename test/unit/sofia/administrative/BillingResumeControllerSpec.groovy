package sofia.administrative

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(BillingResumeController)
@Mock(Invoice)
class BillingResumeControllerSpec extends Specification {
    BillingResumeModelService billingResumeModelService = Mock()

    void 'index returns invoice and paid concepts resume'() {
        given:
        controller.billingResumeModelService = billingResumeModelService

        Date from = Date.parse("yyyy-MM-dd", "2014-01-01")
        Date until = Date.parse("yyyy-MM-dd", "2014-01-07")
        Date modifiedUntil = Date.parse("yyyy-MM-dd", "2014-01-08")
        Long schoolYearId = 6L
        Long billingGroupId = 6L
        BillingResumeCommand brc = new BillingResumeCommand(billingGroupId: billingGroupId, schoolYearId: schoolYearId, from: from, until: until)

        when:
        controller.index(brc)

        then:
        1 * billingResumeModelService.getResume(billingGroupId, schoolYearId, from, modifiedUntil)
    }
}
