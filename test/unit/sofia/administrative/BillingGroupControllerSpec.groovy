package sofia.administrative

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(BillingGroupController)
@Mock(BillingGroup)
class BillingGroupControllerSpec extends Specification {
    void 'index returns billing groups'() {
        given:
        BillingGroupDataService billingGroupDataService = Mock()
        controller.billingGroupDataService = billingGroupDataService

        when:
        controller.index()

        then:
        1 * billingGroupDataService.list()
    }
}
