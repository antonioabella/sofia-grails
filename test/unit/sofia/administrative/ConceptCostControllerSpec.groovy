package sofia.administrative

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(ConceptCostController)
@Mock(Concept)
class ConceptCostControllerSpec extends Specification {

    ConceptCostDataService conceptCostDataService = Mock()

    void setup() {
        controller.conceptCostDataService = conceptCostDataService
    }

    void 'index should call ConceptCostDataService.findAllBySchoolYear'() {
        given:
        Long schoolYearId = 1L

        when:
        controller.index(schoolYearId)

        then:
        1 * conceptCostDataService.findAllBySchoolYearId(schoolYearId)
    }
}
