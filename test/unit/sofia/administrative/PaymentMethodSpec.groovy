package sofia.administrative

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(PaymentMethod)
class PaymentMethodSpec extends Specification {

    void 'name cannot be blank'() {
        def paymentMethod = new PaymentMethod()

        when:
        paymentMethod.validate()

        then:
        paymentMethod.errors.hasFieldErrors('name')

        when:
        paymentMethod.name = 'name'
        paymentMethod.validate()

        then:
        paymentMethod.errors.errorCount == 0
    }
}
