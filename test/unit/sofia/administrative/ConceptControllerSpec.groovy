package sofia.administrative

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(ConceptController)
@Mock(Concept)
class ConceptControllerSpec extends Specification {

    ConceptDataService conceptDataService = Mock()

    void setup() {
        controller.conceptDataService = conceptDataService
    }

    void 'index should call ConceptDataService.list'() {
        when:
        controller.index()

        then:
        1 * conceptDataService.list()
    }
}
