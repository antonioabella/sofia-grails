package sofia.administrative

import grails.test.mixin.TestFor
import sofia.enums.ConceptCostType
import spock.lang.Specification

@TestFor(Concept)
class ConceptSpec extends Specification {

    void 'validation constraints'() {
        Concept concept = new Concept()

        when:
        concept.validate()

        then:
        concept.errors.hasFieldErrors('name')
        concept.errors.hasFieldErrors('billingGroup')
        concept.errors.errorCount == 2

        when:
        concept.name = 'Concept'
        concept.billingGroup = new BillingGroup()
        concept.validate()

        then:
        concept.errors.errorCount == 0
    }
}