package sofia.administrative

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(Bank)
class BankSpec extends Specification {

    void 'validate constraints'() {
        Bank bank = new Bank()

        when:
        bank.validate()

        then:
        bank.errors.errorCount == 1

        when:
        bank.name = 'bank'
        bank.validate()

        then:
        bank.errors.errorCount == 0
    }
}
