package sofia.administrative

import spock.lang.Specification

class BillingResumeCommandSpec extends Specification {

    void 'setFrom clean time value from date'() {
        given:
        Date from = Date.parse("yyyy-MM-dd HH:mm:ss.S", "2014-08-14 10:52:45.23")
        BillingResumeCommand brc = new BillingResumeCommand()
        BillingResumeCommand brc2 = new BillingResumeCommand(from: from)
        Date correctDate = Date.parse("yyyy-MM-dd", "2014-08-14")

        when:
        brc.from = from

        then:
        brc.from == correctDate
        brc2.from == correctDate
    }

    void 'setUntil clean time value and add a day to date'() {
        given:
        Date until = Date.parse("yyyy-MM-dd HH:mm:ss.S", "2014-08-14 10:52:45.23")
        BillingResumeCommand brc = new BillingResumeCommand()
        BillingResumeCommand brc2 = new BillingResumeCommand(until: until)
        Date correctDate = Date.parse("yyyy-MM-dd", "2014-08-15")

        when:
        brc.until = until

        then:
        brc.until == correctDate
        brc2.until == correctDate
    }
}
