package sofia.administrative

import grails.test.mixin.TestFor
import sofia.Month
import sofia.academic.Enrollment
import spock.lang.Specification

@TestFor(Commitment)
class CommitmentSpec extends Specification {

    void 'constraints validation'() {
        Commitment commitment = new Commitment()

        when:
        commitment.validate()

        then:
        commitment.errors.errorCount == 4

        when:
        commitment.enrollment = new Enrollment()
        commitment.concept = new Concept()
        commitment.startDate = new Date()
        commitment.enrollmentMonth = new Month()
        commitment.validate()

        then:
        commitment.errors.errorCount == 0
    }
}