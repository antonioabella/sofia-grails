package sofia.administrative

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(BillingGroupDataService)
class BillingGroupDataServiceSpec extends Specification {
    void 'source return BillingGroup class'() {
        expect:
        service.getSource() instanceof Class<BillingGroup>
    }
}