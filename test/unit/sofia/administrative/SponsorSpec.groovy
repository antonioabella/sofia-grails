package sofia.administrative

import spock.lang.Specification

class SponsorSpec extends Specification {

    void 'validate constraints'() {
        Sponsor sponsor = new Sponsor()

        when:
        sponsor.validate()

        then:
        sponsor.errors.errorCount == 3

        when:
        sponsor.name = 'Sponsor'
        sponsor.taxId = 'RJC45'
        sponsor.taxDomicile = 'Street'
        sponsor.validate()

        then:
        sponsor.errors.errorCount == 0
    }
}