package sofia.administrative

import grails.test.mixin.TestFor
import sofia.academic.Enrollment
import spock.lang.Specification

@TestFor(Scholarship)
class ScholarshipSpec extends Specification {

    void 'validate constraints'() {
        Scholarship scholarship = new Scholarship(sponsor: new Sponsor(),
                enrollment: new Enrollment(), concept: new Concept())

        when:
        scholarship.isPercentage = percentage
        scholarship.amount = amount
        scholarship.validate()

        then:
        scholarship.errors.errorCount == errorCount

        where:
        percentage | amount | errorCount
        true       | 110G   | 2
        false      | 110G   | 0
        true       | 10G    | 0
        false      | -1G    | 2
        true       | -1G    | 2
    }
}