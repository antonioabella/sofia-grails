package sofia

import spock.lang.Specification
import spock.lang.Subject

class GenericDataSpec extends Specification {
    @Subject
    GenericData genericData
    Class source = Object.class

    void setup() {
        genericData = [getSource: { return source }] as GenericData
    }

    void cleanup() {
        GroovySystem.metaClassRegistry.removeMetaClass(Object.class)
    }

    void 'get object by id'() {
        def result = new Object()
        source.metaClass.static.get = { Long id -> result }

        expect:
        genericData.getInstance(1l) == result
    }

    void 'delete object'() {
        Expando instance = new Expando()
        instance.delete = { Map map -> true }

        expect:
        genericData.delete(instance)
    }

    void 'save object'() {
        def result = new Object()
        Expando instance = new Expando()
        instance.save = { Map map -> result }

        expect:
        genericData.save(instance) == result
    }

    void 'return object list'() {
        def result = []
        source.metaClass.static.list = { it -> result }

        expect:
        genericData.list() == result
    }

    void 'return object list with params'() {
        def result = []
        source.metaClass.static.list = { Map it -> result }
        Map params = Mock()

        expect:
        genericData.list(params) == result
    }

    void "create new instance of object"() {
        Object result = Mock(Object)
        source.metaClass.static.newInstance = { params -> result }

        expect:
        genericData.create(test: "test") == result
    }

    void 'count elements in repository'() {
        int result = 15
        source.metaClass.static.count = { -> result }

        expect:
        genericData.count() == result
    }

    void 'get first element in repository'() {
        def result = new Object()
        source.metaClass.static.first = { -> result }

        expect:
        genericData.default == result
    }

    void 'search object by criteria'() {
        List result = []
        source.metaClass.static.search = { String criteria -> result }
        String criteria = "irrelevant"

        expect:
        genericData.search(criteria) == result
    }

    void 'get object by id or default object if not found'() {
        def defaultObject = new Object()
        def instance = new Object()
        source.metaClass.static.first = { -> defaultObject }
        source.metaClass.static.get = { Long id -> instance }

        when:
        def result = genericData.getInstanceOrDefault(id)

        then:
        result == id ? instance : defaultObject

        where:
        id << [null, 10l]
    }
}
