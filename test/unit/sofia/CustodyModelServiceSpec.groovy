package sofia

import grails.rest.RestfulController
import grails.test.mixin.TestFor
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import spock.lang.Specification

@TestFor(CustodyModelService)
class CustodyModelServiceSpec extends Specification {
    CustodyDataService custodyDataService = Mock()
    RestfulController listener = Mock()

    void setup() {
        service.custodyDataService = custodyDataService
        RestfulController.metaClass.respond = { def entity -> }
    }

    void cleanup() {
        GroovySystem.metaClassRegistry.removeMetaClass(RestfulController.class)
    }

    void 'setMainGuardian call sofia.GuardianDataService.getGuardians'() {
        given:
        Long studentId = 1
        Long guardianId = 1
        GrailsParameterMap params = new GrailsParameterMap([studentId: studentId, id: guardianId], listener.request)

        when:
        service.setMainCustodian(listener)

        then:
        1 * listener.getParams() >> params
        1 * custodyDataService.setMainCustodian(studentId, guardianId)
    }
}
