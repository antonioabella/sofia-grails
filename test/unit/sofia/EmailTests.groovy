package sofia

import grails.test.mixin.*
import org.junit.*

@TestFor(Email)
class EmailTests {

    void testConstraints() {
        mockForConstraintsTests Email
        def obj = new Email()
        assert !obj.validate()
        assert "nullable" == obj.errors["address"]
        obj.address = "badAddress"
        assert !obj.validate()
        assert "email" == obj.errors["address"]
        obj.address = "test@home.com"
        assert obj.validate()
    }
}
