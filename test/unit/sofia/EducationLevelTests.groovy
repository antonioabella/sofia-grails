package sofia

import grails.test.mixin.TestFor
import org.junit.Test

@TestFor(EducationLevel)
class EducationLevelTests {

    @Test
    void testConstraints() {
        def obj = new EducationLevel()
        assert !obj.validate()
        obj.name = 'name'
        assert obj.validate()
    }
}
