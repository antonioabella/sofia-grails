package sofia

import spock.lang.Specification

class PeriodWeekSpec extends Specification {
    void 'period week toString'() {
        given:
        String df = 'yyyy-MM-dd'
        PeriodWeek pw = new PeriodWeek()

        when:
        pw.start = Date.parse(df, '2050-09-01')
        pw.end = Date.parse(df, '2050-09-08')

        then:
        pw.toString() == '01 Sep/08 Sep'
    }
}
