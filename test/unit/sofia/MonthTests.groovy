package sofia

import grails.test.mixin.TestFor

@TestFor(Month)
class MonthTests {

    void testConstraints() {
        mockForConstraintsTests(Month, [new Month(systemId: 1, position: 1)])
        def obj = new Month()
        assert !obj.validate()
        assert 'nullable' == obj.errors['name']
        obj.name = 'May'
        obj.position = 2
        obj.systemId = 3
        assert obj.validate()
        obj.position = 1
        obj.systemId = 1
        assert !obj.validate()
        assert 'unique' == obj.errors['position']
        assert 'unique' == obj.errors['systemId']
    }
}
