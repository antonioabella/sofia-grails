package sofia

import spock.lang.Specification

class ActorSearchServiceSpec extends Specification {
    ActorSearchService service = new ActorSearchService()
    ActorFinderFactoryService actorFinderFactoryService = Mock()

    void setup() {
       service.actorFinderFactoryService = actorFinderFactoryService
    }

    void 'search call to peopleFinderFactoryService' () {
        ActorFinder peopleFinder = Mock()
        ActorSearchCommand actorSearchCommand = new ActorSearchCommand(entityName: 'Entity Name', criteria: 'Criteria')

        List list = [1..5]
        List result

        when:
        result = service.search(actorSearchCommand)

        then:
        1 * actorFinderFactoryService.create(actorSearchCommand.entityName) >> peopleFinder
        if(entityExist) {
            1 * peopleFinder.search(actorSearchCommand.criteria) >> list
            result == list
        }
        else {
            result == []
        }

        where:
        entityExist << [true, false]
    }
}
