package sofia

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import sofia.academic.Enrollment
import sofia.enums.Kinship
import spock.lang.Specification
import spock.lang.Subject

@TestFor(Student)
@Build([Grade, SchoolYear, Person, Student, Enrollment])
class StudentSpec extends Specification {

    @Subject
    Student student = new Student()

    void 'mainGuardian returns main guardian'() {
        student.custodians = []
        Custody custody = Mock()
        Guardian guardian = Mock()
        _ * custody.main >> main
        _ * custody.guardian >> guardian
        student.custodians << custody

        expect:
        (student.mainGuardian == null) == isNull

        where:
        main  | isNull
        true  | false
        false | true
    }

    void "isFatherOrMotherDuplicated is true when father or mother kinship is duplicated"() {
        student.custodians = []
        [Kinship.MOTHER, Kinship.FATHER, Kinship.GRANDMOTHER].each {
            Custody custody = Mock()
            _ * custody.kinship >> it
            student.custodians.add(custody)
        }

        expect:
        student.isFatherOrMotherDuplicated(kinship) == asignado

        where:
        kinship             | asignado
        Kinship.MOTHER      | true
        Kinship.FATHER      | true
        Kinship.GRANDMOTHER | false

    }

    private List<Enrollment> createEnrollments() {
        List<Enrollment> enrollments = []
        List dates = ["2010-09-15", "2010-09-16", "2010-09-17", "2010-09-18"]
        String format = "yyyy-MM-dd"
        Student a = Student.build(person: Person.build(identityCard: "1"))
        dates.each { String date ->
            enrollments << Enrollment.build(student: a, enrollmentDate: Date.parse(format, date))
        }
        return enrollments
    }
}
