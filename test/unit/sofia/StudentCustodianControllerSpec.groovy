package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(StudentCustodianController)
@Mock([Student])
class StudentCustodianControllerSpec extends Specification {
    StudentDataService studentDataService = Mock()
    CustodyModelService custodyModelService = Mock()

    void setup() {
        controller.studentDataService = studentDataService
        controller.custodyModelService = custodyModelService
    }

    void 'index returns list of guardians'() {
        Long studentId = 20
        params.studentId = studentId

        when:
        controller.index()

        then:
        1 * studentDataService.getCustodians(studentId)
    }

    void 'update set main guardian'() {
        given:
        request.requestMethod = 'PUT'

        when:
        controller.update()

        then:
        1 * custodyModelService.setMainCustodian(controller)
    }
}
