package sofia

import grails.test.mixin.TestFor
import org.springframework.validation.Errors
import spock.lang.Specification

import static org.springframework.http.HttpStatus.NO_CONTENT

@TestFor(GenericController)
class GenericControllerSpec extends Specification {

    GenericController controller = new GenericController()

    void "search not found redirect to action"() {
        String criteria = "criteria"
        String action = "show"
        controller.defaultAction = action

        when:
        controller.searchNotFound(criteria)

        then:
        controller.flash.message == "elemento.not.found.message"
        response.redirectUrl == "/generic/$action"
    }

    void "saving error render view 'create' with instance"() {
        def instance = new Object()

        when:
        controller.savingError(instance)

        then:
        model.nullInstance == instance
        view == "/generic/create"
    }

    void "saving ok redirect to show"() {
        Long id = 10
        Expando instance = new Expando(id: id)

        when:
        controller.savingOk(instance)

        then:
        flash.message == "default.created.message"
        response.redirectUrl == "/generic/show/$id"
    }

    void "not found redirect to action"() {
        Long id = 10
        String action = "show"
        controller.defaultAction = action

        when:
        controller.notFound(id)

        then:
        controller.flash.message == "default.not.found.message"
        response.redirectUrl == "/generic/$action"
    }

    void "updating ok redirect to show"() {
        Long id = 10l
        Expando instance = new Expando(id: id)

        when:
        controller.updatingOk(instance)

        then:
        flash.message == "default.updated.message"
        response.redirectUrl == "/generic/show/$id"
    }

    void "updating error render view edit with instance"() {
        def instance = new Object()

        when:
        controller.updatingError(instance)

        then:
        model.nullInstance == instance
        view == "/generic/edit"
    }

    void "updating version error render edit with instance"() {
        def instance = new Expando()
        Errors errors = Mock()
        instance.errors = errors

        when:
        controller.updatingVersionError(instance)

        then:
        view == "/generic/edit"
        1 * errors.rejectValue("version", "default.optimistic.locking.failure", ['null'],
                "Another user has updated this null while you were editing")
    }

    void "deleting ok redirect to action"() {
        Long id = 10l
        Expando instance = new Expando(id: id)
        String action = "show"
        controller.defaultAction = action

        when:
        request.contentType = 'application/x-www-form-urlencoded'
        controller.deletingOk(instance)

        then:
        controller.flash.message == "default.deleted.message"
        response.redirectUrl == "/generic/$action"

    }

    void "deleting ok without contentType returns status NO_CONTENT"() {
        Long id = 10l
        Expando instance = new Expando(id: id)
        String action = "show"
        controller.defaultAction = action

        when:
        controller.deletingOk(instance)

        then:
        response.status == NO_CONTENT.value()

    }

    void "deleting error redirect to show"() {
        Long id = 10l
        Expando instance = new Expando(id: id)

        when:
        controller.deletingError(instance)

        then:
        controller.flash.message == "default.not.deleted.message"
        response.redirectUrl == "/generic/show/$id"
    }

    void "inactive error redirect to show"() {
        Long id = 10l
        Expando instance = new Expando(id: id)

        when:
        controller.inactiveError(instance)

        then:
        controller.flash.message == "default.inactive.message"
        response.redirectUrl == "/generic/show/$id"
    }
}
