package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(GuardianStudentController)
@Mock([Guardian])
class GuardianStudentControllerSpec extends Specification {
    GuardianDataService guardianDataService = Mock()

    void setup() {
        controller.guardianDataService = guardianDataService
    }

    void 'index returns list of guardians'() {
        Long id = 20L
        params.guardianId = id

        when:
        controller.index()

        then:
        1 * guardianDataService.getStudents(id)
    }
}