package sofia

import grails.test.mixin.TestFor
import sofia.enums.Kinship

@TestFor(PersonalReference)
class PersonalReferenceTests {

    void testConstraints() {
        mockForConstraintsTests PersonalReference
        def obj = new PersonalReference()
        assert !obj.validate()
        assert "nullable" == obj.errors["name"]
        assert "nullable" == obj.errors["phoneNumber"]
        assert "nullable" == obj.errors["person"]
        obj.name = "John"
        obj.phoneNumber = "56132435"
        obj.kinship = Kinship.BROTHER
        obj.person = new Person()
        assert obj.validate()
    }
}
