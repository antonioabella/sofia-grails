package sofia

import grails.test.mixin.*
import org.junit.*

import sofia.enums.PhoneType;

@TestFor(PhoneNumber)
class PhoneNumberTests {

    void testConstraints() {
        mockForConstraintsTests PhoneNumber
        def obj = new PhoneNumber()
        assert !obj.validate()
        obj.number = '5612435'
        obj.phoneType = PhoneType.HOME
        assert obj.validate()
    }
}
