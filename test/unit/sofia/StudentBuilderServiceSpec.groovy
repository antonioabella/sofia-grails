package sofia

import grails.buildtestdata.mixin.Build
import spock.lang.Specification

@Build([Person])
class StudentBuilderServiceSpec extends Specification {
    StudentBuilderService service = new StudentBuilderService()
    StudentDataService studentDataService = Mock()
    PersonDataService personDataService = Mock()

    void setup() {
        service.studentDataService = studentDataService
        service.personDataService = personDataService
    }

    void 'save call StudentDataService and PersonDataService'() {
        given:
        Student student = Stub()
        Person person = Stub()
        ActorCommand actorCommand = new ActorCommand(person: person)

        when:
        service.save(actorCommand)

        then:
        1 * studentDataService.create() >> student
        1 * personDataService.save(person) >> person
        1 * studentDataService.save(student) >> student
    }
}
