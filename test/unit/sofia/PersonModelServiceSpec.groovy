package sofia

import spock.lang.Specification

class PersonModelServiceSpec extends Specification {
    void 'search call to personaSearchService.search'() {
        ActorSearchService actorSearchService = Mock()
        PersonModelService personModelService = new PersonModelService(actorSearchService: actorSearchService)
        ActorSearchCommand actorSearchCommand = Mock()

        when:
        personModelService.search(actorSearchCommand)

        then:
        1 * actorSearchService.search(actorSearchCommand)
    }
}
