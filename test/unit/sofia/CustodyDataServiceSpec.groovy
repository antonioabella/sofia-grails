package sofia

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import sofia.test.PersonBuilder
import spock.lang.Specification
import sofia.test.StudentBuilder

@TestFor(CustodyDataService)
@Build([Student, Person, Guardian, Custody])
@Mock([Student, PhoneNumber, Email, Person, Guardian, Custody])
class CustodyDataServiceSpec extends Specification {

    void 'setMainGuardian set a guardian as unique main'() {
        given:
        Student student = new StudentBuilder().buildList(1)[0]
        Custody originalGuardianMain = createGuardianLink(student, true)
        Custody newGuardianMain = createGuardianLink(student, false)

        when:
        service.setMainCustodian(student.id, newGuardianMain.id)

        then:
        !originalGuardianMain.main
        newGuardianMain.main
    }

    Custody createGuardianLink(Student student, boolean isMain) {
        Custody guardianLink = Custody.build(
                main: isMain,
                student: student, guardian: Guardian.build(
                    person: new PersonBuilder().createList(1)[0]))
        student.addToCustodians(guardianLink)
        guardianLink
    }
}
