package sofia

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.IgnoreRest
import spock.lang.Specification
import static org.springframework.http.HttpStatus.*

@TestFor(ActorController)
@Build([Student, Person, Email, PhoneNumber, Actor])
@Mock(Actor)
class ActorControllerSpec extends Specification {
    ActorSearchService actorSearchService = Mock()
    ActorDataService actorDataService = Mock()
    ActorBuilderService actorBuilderService = Mock()

    void setup() {
        controller.actorSearchService = actorSearchService
        controller.actorDataService = actorDataService
        controller.actorBuilderService = actorBuilderService
    }

    void 'index do search and returns list with people'() {
        ActorSearchCommand asc = new ActorSearchCommand(entityName: 'student', criteria: 'names')

        when:
        controller.index(asc)

        then:
        1 * actorSearchService.search(asc)
    }

    void 'show return a map with actor and person properties'() {
        Long id = 5

        when:
        controller.show(id)

        then:
        1 * actorDataService.get(id)
    }

    void 'save call ActorBuilderService.save()'() {
        ActorCommand actorCommand = new ActorCommand()
        Actor actor = new Actor()
        request.method = 'POST'

        when:
        actorCommand.validate()
        controller.save(actorCommand)

        then:
        response.status == UNPROCESSABLE_ENTITY.value()

        when:
        response.reset()
        actorCommand.person = new Person()
        actorCommand.entityName = 'student'
        actorCommand.validate()
        controller.save(actorCommand)

        then:
        response.status == UNPROCESSABLE_ENTITY.value()

        when:
        response.reset()
        actorCommand.person = buildPerson()
        actorCommand.validate()
        controller.save(actorCommand)

        then:
        1 * actorBuilderService.save(actorCommand) >> actor
        response.status == CREATED.value()
    }

    private Person buildPerson() {
        return Person.build(identityCard: '1').save(flush: true)
    }

    @IgnoreRest
    void 'update call actorDataService.save'() {
        Actor actor = new Actor()
        request.method = 'PUT'

        when:
        controller.update(actor)

        then:
        response.status == NOT_FOUND.value()

        when:
        response.reset()
        actor.person = new Person()
        params.id = actor.id
        controller.update(actor)

        then:
        response.status == UNPROCESSABLE_ENTITY.value()

        when:
        response.reset()
        actor.person = buildPerson()
        actor.save(flush: true)
        params.id = actor.id
        controller.update(actor)

        then:
        1 * actorDataService.save(actor) >> actor
        response.status == OK.value()
    }

    void 'conditional phoneNumber and emailAddress validation'() {
        when:
        ActorCommand ac = new ActorCommand(person: new Person(), entityName: entityName)
        ac.validate()

        then:
        if(entityName != 'student') {
            assert ac.errors.errorCount > 0
        }

        when:
        ac.person.phoneNumber = 561
        ac.person.emailAddress = 'irrelevant@irrelevant.com'
        ac.validate()

        then:
        ac.errors.errorCount == 0

        where:
        entityName << ['student', 'guardian', 'teacher']
    }
}