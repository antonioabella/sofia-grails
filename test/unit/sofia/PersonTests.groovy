package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import sofia.enums.Gender

@TestFor(Person)
@Mock([PhoneNumber, Email])
class PersonTests {

    void testConstraints() {
        mockForConstraintsTests(Person, [new Person(identityCard: '12059207')])
        Person obj = new Person()
        assert !obj.validate()
        obj.birthCountry = 'VE'
        obj.address = 'Caracas'
        obj.firstName = 'Antonio'
        obj.lastName = 'Abella'
        obj.identityCard = '5612435'
        obj.birthDate = new Date()
        obj.notes = 'C' * 500
        obj.gender = Gender.MALE
        assert obj.validate()
        obj.notes = 'C' * 2000
        obj.identityCard = '12059207'
        assert !obj.validate()
        assert 2 == obj.errors.allErrors.size()
        assert "unique" == obj.errors["identityCard"]
        assert "maxSize" == obj.errors["notes"]
    }
}
