package sofia.academic

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(SubjectModelService)

class SubjectModelServiceSpec extends Specification {

    SubjectDataService subjectDataService = Mock()

    void setup() {
        service.subjectDataService = subjectDataService
    }

    void 'call getBySchoolYearAndGrade from subjectDataService'() {
        Long schoolYearId = 1L
        Long gradeId = 1L

        when:
        service.getBySchoolYearAndGrade(schoolYearId, gradeId)

        then:
        1 * subjectDataService.findAllBySchoolYearAndGrade(schoolYearId, gradeId)
    }
}
