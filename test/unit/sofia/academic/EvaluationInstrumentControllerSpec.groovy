package sofia.academic

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(EvaluationInstrumentController)
@Build(EvaluationInstrument)
class EvaluationInstrumentControllerSpec extends Specification {

    void 'list all EvaluationInstrument'() {
        given:
        3.times {
            EvaluationInstrument.build().save(flush: true)
        }

        when:
        controller.index()

        then:
        response.json*.name == [
            'name',
            'name',
            'name',
        ]
    }
}
