package sofia.academic

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import sofia.*
import spock.lang.IgnoreRest
import spock.lang.Specification
import spock.lang.Unroll

@TestFor(Evaluation)
@Build([Evaluation, Course, Teacher, Subject, Section, Grade, SchoolYear, Person, Period])
class EvaluationSpec extends Specification {
    Evaluation evaluation

    void setup() {
        evaluation = new Evaluation(course: Stub(Course), period: Stub(Period))
    }

    @Unroll
    void 'weight must be a valid positive percent (0-100)'() {
        given:
        evaluation.weight = weight

        expect:
        evaluation.validate() == valid

        where:
        weight | valid
        -1     | false
        0      | true
        50     | true
        100    | true
        101    | false
    }

    @Unroll
    @IgnoreRest
    void 'weigh sum <= 100 in evaluations with same course and period '() {
        given: 'Five evaluation weighting 10 each'
        def person = Person.build(identityCard: '1').save(flush: true)
        def period = Period.build().save(flush: true)
        def course = Course.build(teacher: Teacher.build(person: person)).save(flush: true)
        createEvaluations period, course, 5
        evaluation.period = period
        evaluation.course = course

        when: 'this evaluation weight plus others > 100'
        evaluation.weight = 51

        then: 'validation error'
        evaluation.validate() == false
        evaluation.errors['pepe'] != null

        when:
        evaluation.weight = 50

        then: 'validation error'
        evaluation.validate() == true
    }

    void createEvaluations(Period period, Course course, int times) {
        times.times {
            Evaluation.build(period: period, course: course, weight: 10).save(flush: true, failOnError: true)
        }
        println Evaluation.count()
    }
}
