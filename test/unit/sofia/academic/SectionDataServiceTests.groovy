package sofia.academic

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.junit.Test
import sofia.Grade
import sofia.SchoolYear

@TestFor(SectionDataService)
@Build([Section, Grade, SchoolYear])
class SectionDataServiceTests {

    @Test
    void listTest() {
        SchoolYear pe = SchoolYear.build()
        20.times { createSection(pe) }
        assert service.list(pe.id).size() == 20
    }

    @Test
    void findAllByGradesAndSchoolYearTest() {
        SchoolYear schoolYear = SchoolYear.build()
        SchoolYear schoolYear1 = SchoolYear.build()
        Grade grade = Grade.build()
        Grade grade1 = Grade.build()
        assert schoolYear.id != schoolYear1.id
        9.times { createSection(schoolYear, grade) }
        1.times { createSection(schoolYear, grade1) }
        5.times { createSection(schoolYear1, grade) }
        7.times { createSection(schoolYear1, grade1) }

        assert service.findAllByGradesAndSchoolYear([], schoolYear1.id).size() == 0
        assert service.findAllByGradesAndSchoolYear([grade.id], schoolYear.id).size() == 9
        assert service.findAllByGradesAndSchoolYear([grade.id], schoolYear1.id).size() == 5
        assert service.findAllByGradesAndSchoolYear([grade.id, grade1.id], schoolYear1.id).size() == 12
        assert service.findAllByGradesAndSchoolYear([grade1.id], schoolYear1.id).size() == 7
        assert service.findAllByGradesAndSchoolYear([grade.id, grade1.id], schoolYear.id).size() == 10
        assert service.findAllByGradesAndSchoolYear([grade1.id], schoolYear.id).size() == 1
    }

    @Test
    void findAllBySections() {
        SchoolYear schoolYear = SchoolYear.build()
        10.times {
            createSection(schoolYear)
        }

        assert service.findAllBySections([]).size() == 0
        assert service.findAllBySections((1..5) as List<Long>).size() == 5
    }


    private void createSection(SchoolYear schoolYear, Grade grade = null) {
        schoolYear.save()
        grade = grade ?: Grade.build()
        Random random = new Random()
        Section.build(name: random.nextDouble(), schoolYear: schoolYear, grade: grade)
    }
}
