package sofia.academic

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(CourseController)
@Mock(Course)
class CourseControllerSpec extends Specification {

    CourseDataService courseDataService = Mock()

    void setup() {
        controller.courseDataService = courseDataService
    }

    void 'index call findAllBySchoolYearAndTeacher'() {
        given:
        Long teacherId = 10L
        Long schoolYearId = 10L
        params.putAll([schoolYearId: schoolYearId, teacherId: teacherId])

        when:
        controller.index()

        then:
        1 * courseDataService.findAllBySchoolYearAndTeacher(schoolYearId, teacherId)
    }
}