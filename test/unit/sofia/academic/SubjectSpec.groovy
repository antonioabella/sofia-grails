package sofia.academic

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import sofia.Grade
import sofia.SchoolYear
import spock.lang.Specification

@TestFor(Subject)
@Build([Subject, Grade, SchoolYear])
class SubjectSpec extends Specification {
    void 'setting and getting multiSubject property'() {
        given:
        Subject subject = new Subject()

        expect:
        !subject.multiSubject

        when:
        subject.addToSubjects(Subject.build())

        then:
        subject.multiSubject
    }
}
