package sofia.academic

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(SubjectController)
@Mock(Subject)
class SubjectControllerSpec extends Specification {
    SubjectModelService subjectModelService = Mock()

    void setup() {
        controller.subjectModelService = subjectModelService
    }

    void 'index call getBySchoolYearAndGrade'() {
        given:
        Long schoolYearId = 10L
        Long gradeId = 10L
        params.putAll([schoolYearId: schoolYearId, gradeId: gradeId])

        when:
        controller.index()

        then:
        1 * subjectModelService.getBySchoolYearAndGrade(schoolYearId, gradeId)
    }
}
