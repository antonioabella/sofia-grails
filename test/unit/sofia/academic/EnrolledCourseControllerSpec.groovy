package sofia.academic

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(EnrolledCourseController)
@Mock(EnrolledCourse)
class EnrolledCourseControllerSpec extends Specification {
    
    EnrolledCourseDataService enrolledCourseDataService = Mock()
    
    void setup() {
        controller.enrolledCourseDataService = enrolledCourseDataService
    }
    
    void 'index call getByCourse'() {
        given:
        Long courseId = 10L
        params.courseId = courseId
        
        when:
        controller.index()
        
        then:
        1 * enrolledCourseDataService.findAllByCourse(courseId)
    }
}
