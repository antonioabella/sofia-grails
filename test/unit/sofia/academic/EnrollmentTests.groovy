package sofia.academic

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import sofia.Grade
import sofia.Institute
import sofia.Month
import sofia.SchoolYear
import sofia.Student

@TestFor(Enrollment)
@Mock([Institute, SchoolYear, Student])
class EnrollmentTests {

    String dateFormat = "yyyy-MM-dd"

    void testConstraints() {
        mockForConstraintsTests Enrollment
        mockForConstraintsTests Institute
        mockForConstraintsTests SchoolYear
        mockForConstraintsTests Month
        mockForConstraintsTests Student
        mockForConstraintsTests Grade
        Enrollment obj = new Enrollment()
        assert !obj.validate()
        obj.institute = new Institute()
        obj.grade = new Grade(name: 'Grade')
        obj.schoolYear = new SchoolYear(start: Date.parse(dateFormat, "2010-09-15"),
                end: Date.parse(dateFormat, "2011-09-15"))
        obj.enrollmentMonth = new Month()
        obj.sofia = false
        obj.student = new Student()
        obj.enrollmentDate = new Date()
        assert obj.validate()
    }
}
