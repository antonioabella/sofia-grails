package sofia.academic

import grails.rest.RestfulController
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import sofia.Period
import sofia.PeriodWeek
import spock.lang.Specification

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY

@TestFor(EvaluationModelService)
@Mock([Evaluation, RestfulController])
class EvaluationModelServiceSpec extends Specification {
    EvaluationDataService evaluationDataService = Mock()
    RestfulController listener = GroovyMock()

    void setup() {
        service.evaluationDataService = evaluationDataService
    }

    void 'list all evaluations by period and course'() {
        given:
        Long periodId = 1L
        Long courseId = 1L

        when:
        service.findAllByPeriodAndCourse(periodId, courseId)

        then:
        1 * evaluationDataService.findAllByPeriodAndCourse(periodId, courseId)
    }

    void 'update an evaluation'() {
        given:
        Period period = Mock()
        Course course = Mock()
        PeriodWeek week = Mock()
        Evaluation evaluation = new Evaluation(period: period,
                course: course, week: week)

        when:
        service.save(listener)

        then:
        1 * listener.createResource() >> evaluation
        1 * evaluation.period.discard()
        1 * evaluation.course.discard()
        1 * evaluation.week.discard()
        1 * listener.respond(status: UNPROCESSABLE_ENTITY.value(), evaluation)
//        1 * evaluation.validate()
//        1 * evaluation.hasErrors() >> false
//        1 * evaluationDataService.save(evaluation)
        0 * _
    }
}
