package sofia.academic

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(EvaluationController)
@Mock(Evaluation)
class EvaluationControllerSpec extends Specification {
    EvaluationModelService evaluationModelService = Mock()

    void setup() {
        controller.evaluationModelService = evaluationModelService
    }

    void 'list all evaluation for period and course'() {
        given:
        Long periodId = 5L
        Long courseId = 5L
        params.putAll([periodId: periodId, courseId: courseId])

        when:
        controller.index()

        then:
        1 * evaluationModelService.findAllByPeriodAndCourse(periodId, courseId)
    }

    void 'create new Evaluation'() {
        when:
        controller.save()

        then:
        1 * evaluationModelService.save(controller)
    }

    void 'update evaluation'() {
        when:
        controller.update()

        then:
        1 * evaluationModelService.update(controller)
    }
}
