package sofia.academic

import grails.test.mixin.TestFor
import sofia.Grade
import sofia.SchoolYear

import java.text.SimpleDateFormat

@TestFor(Section)
class SectionTests {

    void testConstraints() {
        mockForConstraintsTests SchoolYear
        mockForConstraintsTests Grade
        def obj = new Section()
        assert !obj.validate()
        obj.grade = new Grade(name: 'Grade')
        obj.schoolYear = new SchoolYear(start: new SimpleDateFormat('yyyy-MM-dd').parse('2010-09-15'),
                end: new SimpleDateFormat('yyyy-MM-dd').parse('2011-09-15'))
        obj.name = "A"
        assert obj.validate()
    }

    void testActive() {
        SchoolYear schoolYearInactive = new SchoolYear(active: false)
        SchoolYear schoolYearActive = new SchoolYear(active: true)
        Section obj = new Section(schoolYear: schoolYearInactive)
        assert !obj.active
        obj = new Section(schoolYear: schoolYearActive)
        assert obj.active
    }
}
