package sofia.academic

import grails.rest.RestfulController
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import sofia.*
import spock.lang.Specification

import static org.springframework.http.HttpStatus.OK
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY

@TestFor(EnrollmentModelService)
@Mock([Grade, Section, Month, Enrollment])
class EnrollmentModelServiceSpec extends Specification {
    EnrollmentDataService enrollmentDataService = Mock()
    StudentDataService studentDataService = Mock()
    InstituteDataService instituteDataService = Mock()
    SchoolYearDataService schoolYearDataService = Mock()
    RestfulController listener = GroovyMock()

    void setup() {
        service.enrollmentDataService = enrollmentDataService
        service.studentDataService = studentDataService
        service.instituteDataService = instituteDataService
        service.schoolYearDataService = schoolYearDataService
    }

    void 'getCurrentEnrollmentByStudent calls StudentDataService.getLastEnrollmentId and EnrollmentDataService.getLastEnrollmentByStudent'() {
        given:
        Long enrollmentId = 5
        Long studentId = 4

        when:
        service.getCurrentEnrollmentByStudent(studentId)

        then:
        1 * studentDataService.getCurrentEnrollmentId(studentId) >> enrollmentId
        1 * enrollmentDataService.getInstance(enrollmentId)
    }

    void 'getEnrollmentsByStudent'() {
        given:
        Long studentId = 4

        when:
        service.getEnrollmentsByStudent(studentId)

        then:
        1 * studentDataService.getEnrollments(studentId)
    }

    void 'set institute to enrollment'() {
        given:
        Enrollment enrollment = Mock()
        Institute institute = Stub()

        when:
        service.setInstitute(enrollment)

        then:
        1 * instituteDataService.getDefault() >> institute
        1 * enrollment.setInstitute(institute)
    }

    void 'set schoolYear to enrollment'() {
        given:
        Enrollment enrollment = Mock()
        SchoolYear schoolYear = Stub()

        when:
        service.setSchoolYear(enrollment)

        then:
        1 * schoolYearDataService.getEnrollmentYear() >> schoolYear
        1 * enrollment.setSchoolYear(schoolYear)
    }

    void 'save an Enrollment and get an error'() {
        given:
        Institute institute = Stub()
        SchoolYear schoolYear = Stub()
        Enrollment enrollment = new Enrollment(grade: Mock(Grade),
                section: Mock(Section), enrollmentMonth: Mock(Month),
                withdrawalMonth: Mock(Month))

        when: 'Errors in validation'
        service.save(listener)

        then:
        1 * listener.createResource() >> enrollment
        1 * enrollment.grade.discard()
        1 * enrollment.section.discard()
        1 * enrollment.enrollmentMonth.discard()
        1 * enrollment.withdrawalMonth.discard()
        1 * instituteDataService.getDefault() >> institute
        1 * schoolYearDataService.getEnrollmentYear() >> schoolYear
        1 * listener.respond(status: UNPROCESSABLE_ENTITY.value(), enrollment)

        when: 'Error saving'
        enrollment.student = Stub(Student)
        service.save(listener)

        then:
        1 * listener.createResource() >> enrollment
        1 * listener.respond(status: UNPROCESSABLE_ENTITY.value(), null)

        when: 'Everything is Ok'
        enrollment.student = Stub(Student)
        service.save(listener)

        then:
        1 * listener.createResource() >> enrollment
        1 * enrollmentDataService.save(enrollment) >> enrollment
        1 * listener.respond(status: OK.value(), enrollment)
    }
}
