package sofia.academic

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(SectionController)
@Mock([Section])
class SectionControllerSpec extends Specification {
    SectionDataService sectionDataService = Mock()

    void setup() {
        controller.sectionDataService = sectionDataService
    }

    void 'index call sectionDataService.list'() {
        params.schoolYearId = 50L

        when:
        controller.index()

        then:
        1 * sectionDataService.list(params.schoolYearId)
    }
}
