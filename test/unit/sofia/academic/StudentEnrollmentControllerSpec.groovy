package sofia.academic

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import sofia.Grade
import sofia.Month
import sofia.Person
import sofia.Student
import spock.lang.Specification
import sofia.test.StudentBuilder

@TestFor(StudentEnrollmentController)
@Mock([Enrollment, Grade, Section, Month])
@Build([Enrollment, Student, Person])
class StudentEnrollmentControllerSpec extends Specification {

    EnrollmentModelService enrollmentModelService = Mock()
    StudentBuilder studentBuilder = new StudentBuilder()
    Student student

    void setup() {
        student = studentBuilder.build()
        controller.enrollmentModelService = enrollmentModelService
    }

    void 'index calls enrollmentModelService.getEnrollmentsByStudent'() {
        given:
        Long studentId = 4
        params.studentId = studentId

        when:
        controller.index()

        then:
        1 * enrollmentModelService.getEnrollmentsByStudent(studentId)
    }

    void 'show calls enrollmentModelService.getCurrentEnrollmentByStudent'() {
        given:
        Long studentId = 4
        params.studentId = studentId

        when:
        controller.show()

        then:
        1 * enrollmentModelService.getCurrentEnrollmentByStudent(studentId)
    }

    void 'create new Enrollment'() {
        when:
        controller.save()

        then:
        1 * enrollmentModelService.save(controller)
    }

    void 'update enrollment'() {
        given:
        Enrollment enrollment = Enrollment.build(student: student)
        params.studentId = student.id

        when:
        controller.update()

        then:
        1 * enrollmentModelService.updateWithParam(controller, 'studentId')
    }
}
