package sofia

import spock.lang.Specification

class PersonDataServiceSpec extends Specification {
    PersonDataService service = new PersonDataService()

    void 'getSource returns class Person'() {
        expect:
        service.getSource() instanceof Class<Person>
    }
}
