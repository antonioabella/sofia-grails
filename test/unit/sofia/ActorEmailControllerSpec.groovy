package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(ActorEmailController)
@Mock(Email)
class ActorEmailControllerSpec extends Specification {
    ActorModelService actorModelService = Mock()

    void setup() {
        controller.actorModelService = actorModelService
    }

    void 'index call actorModelService.getEmails'() {
        given:
        Long actorId = 10L
        params.actorId = actorId

        when:
        controller.index()

        then:
        1 * actorModelService.getEmails(actorId)
    }

    void 'delete call actorModelService.deleteContact'() {
        when:
        controller.delete()

        then:
        1 * actorModelService.deleteContact(controller)
    }

    void 'deleteContact call actorModelService.removeEmail'() {
        given:
        Person person = Stub()
        Email email = Stub()

        when:
        controller.deleteContact(person, email)

        then:
        actorModelService.removeEmail(person, email)
    }
}
