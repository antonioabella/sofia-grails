package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(GradeController)
@Mock([Grade])
class GradeControllerSpec extends Specification {
    GradeDataService gradeDataService = Mock()

    void setup() {
        controller.gradeDataService = gradeDataService
    }

    void 'index call gradeDataService.list'() {
        when:
        controller.index()

        then:
        1 * gradeDataService.list()
    }
}