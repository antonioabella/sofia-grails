package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(ActorModelService)
@Mock([Person, PhoneNumber, Email])
class ActorModelServiceSpec extends Specification {
    ActorDataService actorDataService = Mock()
    PhoneNumberDataService phoneNumberDataService = Mock()
    EmailDataService emailDataService = Mock()

    void setup() {
        service.actorDataService = actorDataService
        service.phoneNumberDataService = phoneNumberDataService
        service.emailDataService = emailDataService
    }

    void 'return main phoneNumber from actor'() {
        given:
        Long actorId = 0L
        Long phoneNumberId = 0L

        when:
        service.getMainPhoneNumber(actorId)

        then:
        1 * actorDataService.getMainPhoneNumberId(actorId) >> phoneNumberId
        1 * phoneNumberDataService.getInstance(phoneNumberId)
    }

    void 'return main email from actor'() {
        given:
        Long actorId = 0L
        Long emailId = 0L

        when:
        service.getMainEmail(actorId)

        then:
        1 * actorDataService.getMainEmailId(actorId) >> emailId
        1 * emailDataService.getInstance(emailId)
    }

    void 'return phoneNumbers from actor'() {
        given:
        Long actorId = 0L

        when:
        service.getPhoneNumbers(actorId)

        then:
        1 * actorDataService.getPhoneNumbers(actorId)
    }

    void 'return emails from actor'() {
        given:
        Long actorId = 0L

        when:
        service.getEmails(actorId)

        then:
        1 * actorDataService.getEmails(actorId)
    }

    void 'remove phone'() {
        given:
        Person person = Mock()
        PhoneNumber phoneNumber = Stub()

        when:
        service.removePhone(person, phoneNumber)

        then:
        1 * person.getProperty('phoneNumbers')
        1 * person.markDirty('phoneNumbers')
        0 * _
    }

    void 'remove email'() {
        given:
        Person person = Mock()
        Email email = Stub()

        when:
        service.removeEmail(person, email)

        then:
        1 * person.getProperty('emails')
        1 * person.markDirty('emails')
        0 * _
    }

    void 'update phone'() {
        given:
        Person person = Mock()
        PhoneNumber phoneNumber = Stub()

        when:
        service.updatePhone(person, phoneNumber)

        then:
        1 * person.getPhoneNumbers()
        1 * phoneNumberDataService.save(phoneNumber)
        0 * _
    }

    void 'update email'() {
        given:
        Person person = Mock()
        Email email = Stub()

        when:
        service.updateEmail(person, email)

        then:
        1 * person.getEmails()
        1 * emailDataService.save(email)
        0 * _
    }
}
