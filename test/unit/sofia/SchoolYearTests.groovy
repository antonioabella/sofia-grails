package sofia

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.junit.Test

import java.text.SimpleDateFormat

@TestFor(SchoolYear)
@Build(SchoolYear)
class SchoolYearTests {

    @Test
    void testConstraints() {
        SchoolYear schoolYear = new SchoolYear()
        assert !schoolYear.validate()

        schoolYear.start = new SimpleDateFormat("yyyy-MM-dd").parse("2010-09-15")
        schoolYear.end = new SimpleDateFormat("yyyy-MM-dd").parse("2011-09-15")
        schoolYear.active = false
        schoolYear.enrollment = false
        assert schoolYear.validate()
        schoolYear.active = true
        schoolYear.enrollment = true
        assert schoolYear.validate()

        SchoolYear schoolYear1 = new SchoolYear(start: new Date(),
                end: new Date(), active: true, enrollment: true).save()
        assert !schoolYear.validate()
        schoolYear.active = false
        assert !schoolYear.validate()
        schoolYear.enrollment = false
        assert schoolYear.validate()
        schoolYear.enrollment = true
        assert !schoolYear.validate()

        schoolYear1.active = true
        schoolYear1.enrollment = true
        assert schoolYear1.validate()
        schoolYear1.active = false
        schoolYear1.enrollment = false
        assert schoolYear1.validate()
        schoolYear1.active = true
        schoolYear1.enrollment = true
        assert schoolYear1.validate()
    }

    @Test
    void testCompare() {
        SchoolYear schoolYear = new SchoolYear(start: new Date())
        SchoolYear schoolYear1 = new SchoolYear(start: new Date() + 50)
        assert schoolYear < schoolYear1
    }
}
