package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED

@TestFor(CustodyController)
@Mock([Custody])
class CustodyControllerSpec extends Specification {
    void 'index return METHOD_NOT_ALLOWED'() {
        when:
        controller.index()

        then:
        response.status == METHOD_NOT_ALLOWED.value()
    }
}