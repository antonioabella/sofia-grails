package sofia

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.IgnoreRest
import spock.lang.Specification

@TestFor(Person)
@Build([Email, Person, PhoneNumber])
@Mock([Email, PhoneNumber])
class PersonSpec extends Specification {
    void 'creating a new Person with phoneNumber add it to phoneNumbers and set it as principal'() {
        when:
        String phoneNumber = 'irrelevant'
        Person person = Person.build(phoneNumber: phoneNumber, identityCard: '1')

        then:
        person.phoneNumbers.size() == 1
        person.phoneNumbers[0].number == phoneNumber
        person.phoneNumbers[0].main

        when:
        String newPhoneNumber = 'not irrelevant'
        person.phoneNumber = newPhoneNumber

        then:
        person.phoneNumbers.size() == 2
        person.phoneNumbers[1].number == newPhoneNumber
        person.phoneNumbers[1].main

        when:
        person.phoneNumber = phoneNumber

        then:
        person.phoneNumbers.size() == 2
        person.phoneNumbers[0].main
        !person.phoneNumbers[1].main
    }

    void 'creating a new Person with email add it to emails and set it as principal'() {
        when:
        String emailAddress = 'irrelevant@email.com'
        Person person = Person.build(emailAddress: emailAddress, identityCard: '1')

        then:
        person.emails.size() == 1
        person.emails[0].address == emailAddress
        person.emails[0].main

        when:
        String newEmailAddress = 'notirrelevant@email.com'
        person.emailAddress = newEmailAddress

        then:
        person.emails.size() == 2
        person.emails[1].address == newEmailAddress
        person.emails[1].main

        when:
        person.emailAddress = emailAddress

        then:
        person.emails.size() == 2
        person.emails[0].main
        !person.emails[1].main
    }

    void 'getEmailAddress returns address from main email'() {
        when:
        String emailAddress = 'irrelevant@email.com'
        Person person = Person.build(emailAddress: emailAddress, identityCard: '1')

        then:
        person.emailAddress == emailAddress
    }

    void 'getPhoneNumber returns phone number from main phone'() {
        when:
        String phoneNumber = 'irrelevant'
        Person person = Person.build(phoneNumber: phoneNumber, identityCard: '1')

        then:
        person.phoneNumber == phoneNumber
    }
}
