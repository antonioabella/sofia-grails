package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(PeriodController)
@Mock(Period)
class PeriodControllerSpec extends Specification {

    PeriodDataService periodDataService = Mock()

    void setup() {
        controller.periodDataService = periodDataService
    }

    void 'index call findAllBySchoolYear'() {
        given:
        Long schoolYearId = 10L

        when:
        controller.index()

        then:
        1 * periodDataService.findAllBySchoolYear(null)

        when:
        params.putAll([schoolYearId: schoolYearId])
        controller.index()

        then:
        1 * periodDataService.findAllBySchoolYear(schoolYearId)
    }
}
