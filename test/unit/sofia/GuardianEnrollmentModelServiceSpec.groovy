package sofia

import sofia.academic.EnrollmentDataService
import sofia.administrative.CommitmentDataService
import sofia.administrative.DiscountDataService
import sofia.administrative.PaidConcept
import sofia.administrative.PaidConceptDataService
import spock.lang.Specification

class GuardianEnrollmentModelServiceSpec extends Specification {
    GuardianEnrollmentModelService service = new GuardianEnrollmentModelService()
    EnrollmentDataService enrollmentDataService = Mock()
    CommitmentDataService commitmentDataService = Mock()
    PaidConceptDataService paidConceptDataService = Mock()
    DiscountDataService discountDataService = Mock()

    void setup() {
        service.enrollmentDataService = enrollmentDataService
        service.commitmentDataService = commitmentDataService
        service.paidConceptDataService = paidConceptDataService
        service.discountDataService = discountDataService
    }

    void 'return a map with enrollments, commitments and paidConcepts'() {
        given:
        List enrollments = [[id: 1L], [id: 2L], [id: 3L]]
        List enrollmentsIds = enrollments*.id
        List commitments = Stub()
        List paidConcepts = Stub()
        List discounts = Stub()
        Long guardianId = 1L
        Long schoolYearId = 1L

        when:
        Map result = service.findAllByGuardianIdAndSchoolYearId(guardianId, schoolYearId)

        then:
        1 * enrollmentDataService.findAllByGuardianIdAndSchoolYearId(guardianId, schoolYearId) >> enrollments
        1 * commitmentDataService.findAllByEnrollmentsIds(enrollmentsIds) >> commitments
        1 * paidConceptDataService.findAllByEnrollmentsIds(enrollmentsIds) >> paidConcepts
        1 * discountDataService.findAllByEnrollmentsIds(enrollmentsIds) >> discounts

        result.enrollments == enrollments
        result.commitments == commitments
        result.paidConcepts == paidConcepts
        result.discounts == discounts
    }
}
