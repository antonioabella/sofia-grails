package sofia

import grails.rest.RestfulController
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import spock.lang.Specification

import static org.springframework.http.HttpStatus.OK

class GenericRestModelSpec extends Specification {
    GenericRestModel genericRestModel
    GenericData source = Mock()
    RestfulController listener = Mock()

    void setup() {
        genericRestModel = [getSource: { return source }] as GenericRestModel
        RestfulController.metaClass.respond = { def entity, def value -> }
    }

    void cleanup() {
        GroovySystem.metaClassRegistry.removeMetaClass(RestfulController.class)
    }

    void 'save process entity and send message with listener'() {
        given:
        Expando entity = new Expando(validate: { -> }, hasErrors: { -> return thisHasErrors })

        when:
        genericRestModel.save(listener)

        then:
        1 * listener.createResource() >> entity
        if(!thisHasErrors) 1 * source.save(entity) >> entity
        0 * _

        where:
        thisHasErrors << [true, false]
    }

    void 'update process entity and send message with listener'() {
        given:
        GrailsParameterMap params = new GrailsParameterMap([id: 70L], listener.request)
        Expando entity = new Expando(validate: { -> }, hasErrors: { -> return thisHasErrors })

        when:
        genericRestModel.update(listener)

        then:
        1 * listener.getParams() >> params
        1 * listener.queryForResource(params.id) >> entity
        1 * listener.getObjectToBind()
        if(!thisHasErrors) 1 * source.save(entity) >> entity
        0 * _

        where:
        thisHasErrors << [true, false]
    }
}
