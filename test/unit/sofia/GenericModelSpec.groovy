package sofia

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.dao.DataIntegrityViolationException
import spock.lang.Specification
import spock.lang.Subject

import javax.servlet.http.HttpServletRequest

class GenericModelSpec extends Specification {

    @Subject
    GenericModel genericModel
    GenericData source
    GenericController listener = Mock()

    void setup() {
        source = Mock()
        listener = Mock()
        genericModel = [getSource: { return source }] as GenericModel
    }

    void "busca elementos en el repositorio según criterio. Si encuentra los devuelve sino error de buscar"() {
        when:
        if (criterio) {
            1 * source.search(criterio) >> lista
        }
        genericModel.search(criterio, listener)

        then:
        if (lista.size() == 0 && criterio) {
            1 * listener.searchNotFound(criterio)
        }

        where:
        [criterio, lista] << [[null, "pepe"], [[], [1, 2, 3]]].combinations()
    }

    void "emite una lista de los elementos, con un máximo de 100 y un mínimo de 10"() {
        GrailsParameterMap params = new GrailsParameterMap([:], null)

        when:
        genericModel.list(max, params)

        then:
        1 * source.list(params)
        1 * source.count()

        where:
        max  | valor
        null | 10
        5    | 5
        20   | 20
        120  | 100
    }

    void "emite una lista de todos los elementos"() {
        when:
        genericModel.list()

        then:
        1 * source.list()
    }

    void "crea una nueva instancia"() {
        GrailsParameterMap params = new GrailsParameterMap([:], null)

        when:
        genericModel.create(params)

        then:
        1 * source.create(params)
    }

    void "withDomain busca un elemento por id, ejecuta un closure si lo encuentra y redirige sino"() {
        Long id = 10

        when:
        1 * source.getInstance(id) >> instance
        // Cannot create mock for class groovy.lang.Closure.
        genericModel.withDomain id, listener, { inst -> inst.toString() }

        then:
        if (!instance) { // No sé cómo testear la ejecución del closure
            listener.notFound(instance)
        }

        where:
        instance << [null, Mock(Object)]
    }

    private def createInactiveExpando() {
        return new Expando(active: false)
    }

    void "edit devuelve un elemento para edit, si el elemento está inactivo redirige a listener"() {
        when:
        genericModel.edit(instance, listener)

        then:
        if (inactive) {
            listener.inactiveError(instance)
        }

        where:
        inactive | instance
        false    | new Object()
        true     | createInactiveExpando()
    }

    void "update actualiza un elemento en el repositorio"() {
        GrailsParameterMap params = new GrailsParameterMap([:], null)
        Long version = 0l
        Expando instance = new Expando()
        instance.version = internalVersion

        when:
        genericModel.update(instance, version, listener, params)

        then:
        if (internalVersion > version) {
            1 * listener.updatingVersionError(instance)
        } else {
            1 * source.save(instance) >> exito
            if (exito) {
                listener.updatingOk(instance)
            } else {
                listener.updatingError(instance, _)
            }
        }

        where:
        [internalVersion, exito] << [[1l, 0], [true, false]].combinations()
    }

    void "delete elimina un elemento del repositorio"() {
        when:
        genericModel.delete(instance, listener)

        then:
        vecesDelete * source.delete(instance)
        vecesDelete * listener.deletingOk(instance)
        vecesError * listener.inactiveError(instance)

        where:
        instance                | vecesDelete | vecesError
        new Object()            | 1           | 0
        createInactiveExpando() | 0           | 1
    }

    void "save instance"() {
        Expando instance = new Expando()
        instance.save = { Map map -> true }

        when:
        genericModel.save(null, listener)

        then:
        1 * listener.notFound(null)

        when:
        instance.hasErrors = { return true }
        genericModel.save(instance, listener)

        then:
        1 * listener.savingError(instance)

        when:
        instance.hasErrors = { return false }
        genericModel.save(instance, listener)

        then:
        1 * listener.savingOk(instance)
    }

    void "delete genera error de integridad y redirige a deletingError"() {
        Expando instance = new Expando()

        when:
        1 * source.delete(instance) >> {
            throw new DataIntegrityViolationException("error")
        }
        genericModel.delete(instance, listener)

        then:
        1 * listener.deletingError(instance)
    }

    void "paramsToListLong returns list of longs for GrailsParams"() {
        HttpServletRequest httpServletRequest
        ArrayList<String> listLong = ['1', '2', '3']
        GrailsParameterMap params = new GrailsParameterMap(param: [listLong], httpServletRequest)

        expect:
        genericModel.paramToList(params, "param").size() == 3
    }

    void "activar/desactivar change status from object with active property"() {
        Expando instance = new Expando(active: false)

        when:
        genericModel.activate(instance)

        then:
        1 * source.save(instance)
        instance.active

        when:
        genericModel.deactivate(instance)

        then:
        1 * source.save(instance)
        !instance.active
    }
}
