package sofia.test

import sofia.Person
import sofia.Student

// Remember: the @Build tag in calling class should to include Student and Person
class StudentBuilder {
    PersonBuilder personBuilder = new PersonBuilder()

    Student build() {
        Student.build(person: personBuilder.build())
    }

    List<Student> buildList(int howMany) {
        return loadList { List list ->
            howMany.times {
                list.add build()
            }
        }
    }

    List<Student> buildList(List<Person> people) {
        return loadList { List list ->
            people.each {
                list.add Student.build(person: it)
            }
        }
    }

    private List<Student> loadList(Closure closure) {
        List<Student> list = []
        closure.call list
        list
    }
}
