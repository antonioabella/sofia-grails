package sofia.test

import sofia.Person

// Remember: the @Build tag in calling class should to include Person
class PersonBuilder {
    Random rdn = new Random()

    Person build(String firstName = 'firstName') {
        Person.build(firstName: firstName,
                identityCard: rdn.nextInt(10000000))
    }

    List<Person> createList(int howMany) {
        return loadList { List list ->
            howMany.times {
                list.add this.build()
            }
        }
    }

    List<Person> createList(String[] names) {
        return loadList { List list ->
            names.each {
                list.add this.build(firstName: it)
            }
        }
    }

    private List<Person> loadList(Closure closure) {
        List<Person> list = []
        closure.call list
        list
    }
}
