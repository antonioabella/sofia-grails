package sofia.test

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import sofia.Month

@TestMixin(GrailsUnitTestMixin)
class MonthsSamplesFactory {

    Map<String, Month> meses = [:]

    MonthsSamplesFactory() {
        def nombreMeses = [
                "ene", "feb", "mar", "abr",
                "may", "jun", "jul", "ago",
                "sep", "oct", "nov", "dic", "mat"]
        def posicion = [5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 0]
        13.times {
            def nombre = nombreMeses[it]
            meses[nombre] = new Month(position: posicion[it], name: nombre, systemId: it)
        }
    }

    Month getEne() { return meses["ene"] }

    Month getFeb() { return meses["feb"] }

    Month getMar() { return meses["mar"] }

    Month getAbr() { return meses["abr"] }

    Month getMay() { return meses["may"] }

    Month getJun() { return meses["jun"] }

    Month getJul() { return meses["jul"] }

    Month getAgo() { return meses["ago"] }

    Month getSep() { return meses["sep"] }

    Month getOct() { return meses["oct"] }

    Month getNov() { return meses["nov"] }

    Month getDic() { return meses["dic"] }

    Month getMat() { return meses["mat"] }

    def cargarBD() {
        if (!Month.first()) {
            meses.values().each { it.save(flush: true) }
        }
    }

    def getActual(now) {
        def fecha = now ?: new Date()
        todos.find { it.systemId == fecha.month }
    }

    def getTodos() {
        meses.collect { it.value }.sort()
    }

    def getHasta(mes) {
        todos.findAll { it.position <= mes.position }
    }

    def getEntre(inicio, fin) {
        todos.findAll { it.position >= inicio.position && it.position <= fin.position }
    }

    def getInicial() {
        def mes = todos.find { it.position == Month.START }
        assert mes == getSep()
        return mes
    }

    def getFinal() {
        def mes = todos.find { it.position == Month.END }
        assert mes == getAgo()
        return mes
    }

    def getMatricula() {
        todos.find { it.position == Month.ENROLLMENT }
    }

    def getInicioMorosidad(now) {
        getActual(now)
    }

    def getEntreMat(inicio, fin = null) {
        fin = fin ?: inicio
        return [mat] + getEntre(inicio, fin)
    }

    def getEntreMatEnd(inicio, fin = null) {
        return getEntreMat(inicio, fin) << ago
    }

    def getEntreEnd(inicio, fin = null) {
        fin = fin ?: inicio
        return getEntre(inicio, fin) << ago
    }
}
