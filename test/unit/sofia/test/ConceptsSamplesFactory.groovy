package sofia.test

import sofia.administrative.Concept
import sofia.administrative.PaidConcept

class ConceptsSamplesFactory {

    def getConceptos() {
        def conceptos = []
        ["Colegio", "Seguro Escolar", "Fundación"].each {
            conceptos << new Concept(name: it)
        }
        conceptos
    }

    def createConceptosCancelados(inscripcion, factura, concepto, meses) {
        def conceptosCancelados = []
        meses.each { mes ->
            conceptosCancelados << new PaidConcept(enrollment: inscripcion,
                    invoice: factura, concept: concepto, month: mes)
        }
        return conceptosCancelados
    }

    def createConceptosCancelados(inscripcion, factura, meses) {
        def conceptosCancelados = []
        inscripcion.conceptos.each { concepto ->
            conceptosCancelados += createConceptosCancelados(inscripcion, factura, concepto, meses)
        }
        return conceptosCancelados
    }
}
