package sofia

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import org.junit.Test
import sofia.enums.Kinship

@TestMixin(Custody)
@Build([Person, Guardian, Student, Custody])
@Mock([Email, PhoneNumber])
class CustodyTests {

    @Test
    void testConstraints() {
        def obj = new Custody()
        assert !obj.validate()
        Person persona = Person.build(identityCard: "1", firstName: "Antonio", lastName: "Abella")
        obj.guardian = new Guardian(person: persona)
        obj.kinship = Kinship.FATHER
        obj.main = true
        obj.student = new Student()
        assert obj.validate()
    }
}
