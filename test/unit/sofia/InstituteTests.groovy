package sofia

import grails.test.mixin.TestFor
import sofia.enums.BillingType

@TestFor(Institute)
class InstituteTests {

    void testConstraints() {
        mockForConstraintsTests Institute
        def obj = new Institute()
        assert !obj.validate()
        obj.address = 'Caracas'
        obj.taxId = 'J295525583'
        obj.name = 'Advanced College'
        obj.billingType = BillingType.ADVANCE
        assert obj.validate()
    }
}
