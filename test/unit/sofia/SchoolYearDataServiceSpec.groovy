package sofia

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(SchoolYearDataService)
@Build([SchoolYear])
class SchoolYearDataServiceSpec extends Specification {

    void "getDefault returns current SchoolYear"() {
        3.times {
            SchoolYear.build(active: (it % 2 != 0))
        }

        expect:
        service.default.id == 2l
    }

    void "getEnrollment returns enrollment SchoolYear"() {
        3.times {
            SchoolYear.build(enrollment: (it % 2 != 0))
        }

        expect:
        service.enrollmentYear.id == 2l
    }
}
