package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(GuardianController)
@Mock([Guardian])
class GuardianControllerSpec extends Specification {
    GuardianDataService guardianDataService = Mock()

    void setup() {
        controller.guardianDataService = guardianDataService
    }

    void 'show call to GuardianService.getInstance'() {
        given:
        Long id = 5

        when:
        controller.show(id)

        then:
        1 * guardianDataService.getInstance(id)
    }
}
