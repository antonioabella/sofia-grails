package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(SchoolYearController)
@Mock([SchoolYear])
class SchoolYearControllerSpec extends Specification {
    SchoolYearDataService schoolYearDataService = Mock()

    void setup() {
        controller.schoolYearDataService = schoolYearDataService
    }

    void 'index call schoolYearDataService.list'() {
        when:
        controller.index()

        then:
        1 * schoolYearDataService.list()
    }
}
