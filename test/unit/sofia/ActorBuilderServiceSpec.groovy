package sofia

import spock.lang.Specification

class ActorBuilderServiceSpec extends Specification {
    ActorBuilderService service = new ActorBuilderService()
    ActorBuilderFactoryService actorBuilderFactoryService = Mock()

    void setup() {
       service.actorBuilderFactoryService = actorBuilderFactoryService
    }

    void 'save call to actorBuilderFactoryService' () {
        ActorBuilder actorBuilder = Mock()
        ActorCommand actorCommand = new ActorCommand(entityName: 'Entity Name')
        Actor actor = Stub()
        Actor result

        when:
        result = service.save(actorCommand)

        then:
        1 * actorBuilderFactoryService.create(actorCommand.entityName) >> actorBuilder
        if(entityExist) {
            1 * actorBuilder.save(actorCommand) >> actor
            result == actor
        }
        else {
            result == null
        }

        where:
        entityExist << [true, false]
    }
}
