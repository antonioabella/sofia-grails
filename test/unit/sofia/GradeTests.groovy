package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(Grade)
@Mock([Grade, EducationLevel])
class GradeTests {

    void testConstraints() {
        def obj = new Grade()
        assert !obj.validate()
        obj.name = "Caracas"
        obj.level = new EducationLevel()
        assert obj.validate()
    }
}
