package sofia

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(PhoneNumberDataService)
class PhoneNumberDataServiceSpec extends Specification {
    void 'getSource return PhoneNumber class'() {
        expect:
        service.getSource() instanceof Class<PhoneNumber>
    }
}
