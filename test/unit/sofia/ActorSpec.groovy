package sofia

import sofia.security.User
import spock.lang.Specification

class ActorSpec extends Specification {

    Actor actor = new Actor()

    void 'cuando se cambia "active" del actor, si el actor tiene un user este también cambia "active"'() {
        given:
        User user = Mock()
        actor.user = user

        when:
        actor.active = valor

        then:
        1 * user.setProperty('enabled', valor)

        where:
        valor << [true, false]
    }

    void 'cuando se asigna un user al actor, el actor se asigna al  user'() {
        given:
        User user = Mock()

        when:
        actor.user = user

        then:
        1 * user.setProperty('actor', actor)
    }
}
