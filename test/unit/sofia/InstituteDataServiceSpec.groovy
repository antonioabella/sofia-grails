package sofia

import spock.lang.Specification

class InstituteDataServiceSpec extends Specification {
    InstituteDataService service = new InstituteDataService()

    void "getSource returns class Institute"() {
        expect:
        service.getSource() instanceof Class<Institute>
    }
}
