package sofia.security

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.apache.commons.lang.builder.HashCodeBuilder

@TestFor(UserRole)
@Mock(UserRole)
class UserRoleTests {

    void testConstraints() {
        mockForConstraintsTests User
        mockForConstraintsTests UserRole
        mockForConstraintsTests Role
        def obj = new UserRole()
        assert !obj.validate()
        obj.user = new User()
        obj.role = new Role()
        assert obj.validate()
    }

    void testEquals() {
        def obj = new UserRole()
        mockDomain User
        mockDomain Role

        obj.user = new User()
        obj.role = new Role()
        assert !obj.equals(new User())
        def other = new UserRole()
        other.user = obj.user
        other.role = obj.role
        assert obj.equals(other)
    }

    private def createUser(username, email, password) {
        new User(username: username, email: email, password: password).save()
    }

    void testHashCode() {
        mockDomain User
        mockDomain Role
        def obj = new UserRole()
        def builder = new HashCodeBuilder()
        obj.user = new User()
        obj.role = new Role()
        assert obj.hashCode() == builder.append(obj.user.id).append(obj.role.id).toHashCode()
    }

    void testCreate() {
        mockDomain User
        mockDomain Role
        UserRole.create(createUser("pedro", "pedro@gmail.com", "paraco"), new Role(authority: "admin").save())
        UserRole.create(createUser("juand", "juand@gmail.com", "juand"), new Role(authority: "pal").save())
        UserRole.create(createUser("juadd", "juasd@gmail.com", "juad"), new Role(authority: "pol").save())
        UserRole.create(createUser("juad", "juad@gmail.com", "jand"), new Role(authority: "pul").save())
        assert UserRole.count() == 4
    }

    void testRemove() {
        mockDomain User
        mockDomain Role
        def user = createUser("pauji", "apupo@colon.com", "pepere")
        def role = new Role(authority: "admin").save()
        UserRole.create(user, role)
        UserRole.create(createUser("paujiaa", "apaaupo@colon.com", "pepeaare"), new Role(authority: "pal").save())
        assert UserRole.count() == 2
        UserRole.remove(user, role)
    }
}
