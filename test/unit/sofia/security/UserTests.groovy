package sofia.security

import grails.test.mixin.TestFor

@TestFor(User)
class UserTests {

    void testConstraints() {
        String valorUnico = "especial"
        mockForConstraintsTests(User, [new User(username: valorUnico)])
        def obj = new User()
        assert !obj.validate()
        obj.password = "secret"
        obj.email = "paraguana@paranormal.com"
        obj.username = "pepe"
        assert obj.validate()
        obj.username = valorUnico
        assert !obj.validate()
        assert "unique" == obj.errors["username"]
    }

    void testGetAuthorities() {
        mockDomain User
        mockDomain Role
        mockDomain UserRole
        def roleUser = new Role(authority: "user").save(validate: false)
        def role = new Role(authority: "admin").save(validate: false)
        def user = new User(username: "pepe").save(validate: false)
        UserRole.create(user, role)
        assert user.authorities.size() == 1
        UserRole.create(user, roleUser)
        assert user.authorities.size() == 2
    }
}
