package sofia.security

import sofia.Actor
import spock.lang.Specification
import spock.lang.Subject

class UserSpec extends Specification {
    @Subject
    User user = new User()

    void "si no hay un actor, el usuario llama a los miembros internos"() {
        String email = "pepe@email.com"
        String username = "Juan"
        user.email = email
        user.username = username

        expect:
        user.getFullName() == username
    }
}
