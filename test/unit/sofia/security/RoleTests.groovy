package sofia.security

import grails.test.mixin.TestFor

@TestFor(Role)
class RoleTests {
    void testConstraints() {
        mockForConstraintsTests(Role, [new Role(authority: "ROLE_unico")])
        def obj = new Role()
        assert !obj.validate()
        obj.authority = "ROLE_parangua"
        assert obj.validate()
        assert "parangua" == obj.toString()
        obj.authority = "ROLE_unico"
        assert !obj.validate()
        assert "unique" == obj.errors["authority"]
    }
}

