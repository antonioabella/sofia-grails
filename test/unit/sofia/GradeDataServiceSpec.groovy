package sofia

import spock.lang.Specification

class GradeDataServiceSpec extends Specification {
    GradeDataService service = new GradeDataService()

    void "getSource returns class Grade"() {
        expect:
        service.getSource() instanceof Class<Grade>
    }
}
