package sofia

import spock.lang.Specification

class MonthDataServiceSpec extends Specification {
    MonthDataService service = new MonthDataService()

    void "getSource returns class Month"() {
        expect:
        service.getSource() instanceof Class<Month>
    }
}
