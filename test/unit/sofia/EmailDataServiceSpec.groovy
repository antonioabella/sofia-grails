package sofia

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(EmailDataService)
class EmailDataServiceSpec extends Specification {
    void 'getSource return Email Class'() {
        expect:
        service.getSource() instanceof Class<Email>
    }
}
