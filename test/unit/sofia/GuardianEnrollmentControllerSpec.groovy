package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(GuardianEnrollmentController)
@Mock(Guardian)
class GuardianEnrollmentControllerSpec extends Specification {

    GuardianEnrollmentModelService guardianEnrollmentModelService = Mock()

    void setup() {
        controller.guardianEnrollmentModelService = guardianEnrollmentModelService
    }

    void 'index call enrollmentModelService.findAllByGuardianIdAndSchoolYearId'() {
        given:
        Long guardianId = 5l
        Long schoolYearId = 1l
        GuardianSchoolYearCommand cmd = new GuardianSchoolYearCommand(guardianId: guardianId, schoolYearId: schoolYearId)

        when:
        controller.index(cmd)

        then:
        1 * guardianEnrollmentModelService.findAllByGuardianIdAndSchoolYearId(guardianId, schoolYearId)
    }
}
