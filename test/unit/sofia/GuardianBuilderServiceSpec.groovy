package sofia

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(GuardianBuilderService)
class GuardianBuilderServiceSpec extends Specification {
    GuardianDataService guardianDataService = Mock()
    PersonDataService personDataService = Mock()

    void setup(){
        service.personDataService = personDataService
        service.guardianDataService = guardianDataService
    }

    void 'save call personDataService and guardianDataService methods'() {
        given:
        Person person = Stub()
        Guardian guardian = Stub()
        ActorCommand actorCommand = new ActorCommand(person: person)

        when:
        service.save(actorCommand)

        then:
        1 * personDataService.save(person) >> person
        1 * guardianDataService.create() >> guardian
        1 * guardianDataService.save(guardian) >> guardian
        0 * _
    }
}
