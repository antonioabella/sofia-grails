package sofia

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(MonthController)
@Mock([Month])
class MonthControllerSpec extends Specification {
    MonthDataService monthDataService = Mock()

    void setup() {
        controller.monthDataService = monthDataService
    }

    void 'index call monthDataService.list'() {
        when:
        controller.index()

        then:
        1 * monthDataService.list()
    }
}
