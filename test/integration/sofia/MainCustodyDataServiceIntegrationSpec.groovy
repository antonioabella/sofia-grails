package sofia

class MainCustodyDataServiceIntegrationSpec extends PeopleFinderContractIntegrationSpec {
    MainCustodyDataService service = new MainCustodyDataService()

    void setup() {
        fixtureLoader.load 'people/custodians*'
    }

    void 'search returns a list with guardians'() {
        expect:
        service.search('Antonio').size() == 1
        service.search('Cruise').size() == 1
        service.search('100').size() == 1
        service.search('Petra Maria').size() == 0
        service.search('Mendez').size() == 0
        service.searchInactive('199').size() == 0
    }

    @Override
    List getList() {
        service.search('Torres')
    }
}
