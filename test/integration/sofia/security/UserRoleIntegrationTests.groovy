package sofia.security

import org.junit.Test

class UserRoleIntegrationTests {

    @Test
    void testGet() {
        UserRole obj = new UserRole()
        def user = new User(username: "pepe", email: "pedro@gmail.com", password: "pepito", apellidos: "lastName", nombres: "firstName").save(validate: false)
        def role = new Role(authority: "ADMIN").save(validate: false)
        assert User.findByUsername("pepe").username == "pepe"
        assert Role.findByAuthority("ADMIN").authority == "ADMIN"
        obj.user = user
        obj.role = role
        UserRole.create(user, role)
        def model = UserRole.get(user.id, role.id)
        assert obj == model
        model = UserRole.get(0, 0)
        assert obj != model
    }

    @Test
    void testRemoveAll() {
        def obj = new UserRole()
        def user = new User(username: "pepe", email: "pedro@gmail.com", password: "pepito", apellidos: "lastName", nombres: "firstName").save(validate: false)
        def role = new Role(authority: "ADMIN").save(validate: false)
        def roleUser = new Role(authority: "USER").save(validate: false)
        obj.user = user
        obj.role = role
        def ur1 = UserRole.create(user, role)
        assert UserRole.count() == 1
        def ur2 = UserRole.create(user, roleUser)
        assert ur1 != ur2
        assert new Object() != ur2
        def ur3 = new UserRole(user: user, role: role)
        assert ur1 == ur3
        assert UserRole.count() == 2
        UserRole.removeAll(user)
        assert UserRole.count() == 0
        user = new User(username: "pajarraco", email: "pedro@gmail.com", password: "pajarito", apellidos: "lastName", nombres: "firstName").save(validate: false)
        role = new Role(authority: "PASCUA").save(validate: false)
        roleUser = new Role(authority: "PERCA").save(validate: false)
        UserRole.create(user, role)
        UserRole.create(user, roleUser)
        UserRole.removeAll(role)
        assert UserRole.count() == 1
        assert !UserRole.remove(user, role)
        assert new UserRole().hashCode() != new UserRole(user: user).hashCode()
        assert new UserRole().hashCode() != new UserRole(role: role).hashCode()
    }
}
