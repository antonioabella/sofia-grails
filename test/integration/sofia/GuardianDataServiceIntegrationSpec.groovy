package sofia

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec

class GuardianDataServiceIntegrationSpec extends IntegrationSpec {
    GuardianDataService service = new GuardianDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'people/custodians*'
    }

    void 'getStudents returns student list'() {
        Guardian guardian = getGuardian()
        Student student = getStudent()
        when:
        List result = service.getStudents(guardian.id)

        then:
        result.size() == guardian.custodianships.size()
        Map selectedStudent = result.find {it.firstName == 'Sarah'}
        selectedStudent.lastName == 'Sosa'
        selectedStudent.actorId == student.id
        selectedStudent.personId == student.person.id
        selectedStudent.picture ==  student.person.picture
    }

    private Guardian getGuardian() {
        Person person = Person.findByFirstName('Diana')
        Guardian.findByPerson(person)
    }

    private Student getStudent() {
        Person person = Person.findByFirstName('Sarah')
        Student.findByPerson(person)
    }
}
