package sofia

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec

class PeriodWeekDataServiceIntegrationSpec extends IntegrationSpec {
    PeriodWeekDataService service = new PeriodWeekDataService()

    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'periodWeeks'
    }

    void 'PeriodWeek list by period'() {
        given:
        Period period = Period.first()

        when:
        List<PeriodWeek> result = service.findAllByPeriod(period.id)

        then:
        result.size() == 3
    }
}
