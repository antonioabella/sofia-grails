package sofia

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec

abstract class PeopleFinderContractIntegrationSpec extends IntegrationSpec {
    FixtureLoader fixtureLoader

    abstract List getList()

    void 'elements in search list should to implement next properties'() {
        given:
        List result

        when:
        result = getList()

        then:
        result[0].id
        result[0].identityCard
        result[0].firstName
        result[0].lastName
        result[0].active
        result[0].personId
    }
}