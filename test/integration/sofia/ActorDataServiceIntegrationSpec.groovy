package sofia

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec

class ActorDataServiceIntegrationSpec extends IntegrationSpec {
    ActorDataService actorDataService = new ActorDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'people/students*'
    }

    void "get returns an map with actor an person attributes"() {
        given:
        Actor actor = Actor.first()
        Person person = actor.person

        when:
        Map result = actorDataService.get(actor.id)

        then:
        result.id == actor.id
        result.active == actor.active
        result.identityCard == person.identityCard
        result.gender == person.gender
        result.firstName == person.firstName
        result.lastName == person.lastName
        result.address == person.address
        result.birthDate == person.birthDate
        result.birthCountry == person.birthCountry
        result.notes == person.notes
        result.picture == person.picture
        result.phoneNumber == person.phoneNumber
        result.emailAddress == person.emailAddress
        result['class']
    }

    void "get main phone number id"() {
        given:
        Actor actor = Actor.first()
        Person person = actor.person

        when:
        Long id = actorDataService.getMainPhoneNumberId(actor.id)

        then:
        id == person.phoneNumbers.find { it.main }.id
    }

    void "get main email id"() {
        given:
        Actor actor = Actor.first()
        Person person = actor.person

        when:
        Long id = actorDataService.getMainEmailId(actor.id)

        then:
        id == person.emails.find { it.main }.id
    }

    void "get phoneNumber List from actorId"() {
        given:
        Actor actor = Actor.first()

        when:
        List phoneNumbers = actorDataService.getPhoneNumbers(actor.id)

        then:
        phoneNumbers.size() == actor.person.phoneNumbers.size()
    }

    void "get email List from actorId"() {
        given:
        Actor actor = Actor.first()

        when:
        List emails = actorDataService.getEmails(actor.id)

        then:
        emails.size() == actor.person.emails.size()
    }
}
