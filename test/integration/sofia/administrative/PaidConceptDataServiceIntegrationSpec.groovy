package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.SchoolYear
import sofia.academic.Enrollment

class PaidConceptDataServiceIntegrationSpec extends IntegrationSpec {
    PaidConceptDataService service = new PaidConceptDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'administrative/paidConcepts*'
    }

    void 'findAllByEnrollmentsIds returns a map with paidConcepts'() {
        given:
        List enrollmentsIds = Enrollment.list()*.id

        expect:
        service.findAllByEnrollmentsIds(enrollmentsIds).size() == 3
    }

    void 'getResume returns paid by concept between two dates for school year'() {
        given:
        Date from = Date.parse("yyyy-MM-dd", "2014-01-01")
        Date until = Date.parse("yyyy-MM-dd", "2014-01-07")
        Long schoolYearId = SchoolYear.findByActive(true).id
        Long billingGroupId = BillingGroup.first().id
        List resume = service.getResume(billingGroupId, schoolYearId, from, until)

        expect:
        resume.size() == 3
    }
}
