package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.SchoolYear
import sofia.enums.InvoiceStatus

class InvoiceDataServiceIntegrationSpec extends IntegrationSpec {
    InvoiceDataService service = new InvoiceDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load "administrative/invoices*"
    }

    void 'getResume return resume of invoices for schoolYear and billing group between two dates'() {
        given:
        Date from = Date.parse("yyyy-MM-dd", "2014-01-01")
        Date until = Date.parse("yyyy-MM-dd", "2014-01-07")
        Long schoolYearId = SchoolYear.findByActive(true).id
        Long billingGroupId = BillingGroup.first().id
        List resume = service.getResume(billingGroupId, schoolYearId, from, until).sort {
            it.status
        }

        expect:
        resume[2].status == InvoiceStatus.OPEN
        resume[2].count == 1
        resume[2].discount == 5
        resume[2].amount== 10
        resume[0].status == InvoiceStatus.PAID
        resume[0].count == 6
        resume[0].discount == 30
        resume[0].amount == 60
        resume[1].status == InvoiceStatus.REVERSED
        resume[1].count == 1
        resume[1].discount == 5
        resume[1].amount == 10
        resume[1].fromNumber == 0
        resume[1].toNumber == 0
    }

    void 'getPaidInvoiceIds return ids from invoices for schoolYear and billing group between two dates'() {
        given:
        Date from = Date.parse("yyyy-MM-dd", "2014-01-01")
        Date until = Date.parse("yyyy-MM-dd", "2014-01-07")
        Long schoolYearId = SchoolYear.findByActive(true).id
        Long billingGroupId = BillingGroup.first().id
        List invoiceIds = service.getPaidInvoicesIds(billingGroupId, schoolYearId, from, until)

        expect:
        invoiceIds.size() == 6
    }
}
