package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec

class PaymentItemDataServiceIntegrationSpec extends IntegrationSpec {
    PaymentItemDataService service = new PaymentItemDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load "administrative/paidConcepts*"
    }

    void 'getResume return resume of payments for invoices ids'() {
        given:
        List<Long> ids = Payment.list()*.id
        println ids
        List resume = service.getResume(ids)

        expect:
        resume.size() == 3
        println resume
        resume.find { it.paymentMethod == 'PaymentMethod 01' }.amount == 20
    }
}