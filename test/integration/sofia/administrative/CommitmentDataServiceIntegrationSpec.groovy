package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.academic.Enrollment

class CommitmentDataServiceIntegrationSpec extends IntegrationSpec {
    CommitmentDataService service = new CommitmentDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'administrative/commitments*'
    }

    void 'findAllByEnrollmentsIds returns a map with commitments'() {
        given:
        List enrollmentsIds = Enrollment.list()*.id

        expect:
        service.findAllByEnrollmentsIds(enrollmentsIds).size() == 10

    }
}
