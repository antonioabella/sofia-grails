package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.academic.Enrollment

class DiscountDataServiceIntegrationTest extends IntegrationSpec {
    DiscountDataService service = new DiscountDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'administrative/invoices*'
    }

    void 'findAllByEnrollmentsIds returns a map with discounts'() {
        given:
        List enrollmentsIds = Enrollment.list()*.id

        expect:
        service.findAllByEnrollmentsIds(enrollmentsIds).size() == 3
    }
}
