package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.SchoolYear

import java.text.SimpleDateFormat

class ConceptCostDataServiceIntegrationSpec extends IntegrationSpec {
    ConceptCostDataService service = new ConceptCostDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'administrative/conceptCosts*'
    }

    void 'list conceptCost by schoolYear'() {
        given:
        SchoolYear schoolYear = SchoolYear.findByActive(true)

        expect:
        service.findAllBySchoolYearId(schoolYear.id).size() == 5
    }
}
