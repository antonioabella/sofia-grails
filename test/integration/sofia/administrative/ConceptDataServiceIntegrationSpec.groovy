package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.SchoolYear

import java.text.SimpleDateFormat

class ConceptDataServiceIntegrationSpec extends IntegrationSpec {
    ConceptDataService service = new ConceptDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'administrative/concepts*'
    }

    void 'list returns a list of concepts'() {
        expect:
        service.list().size() == 5
    }
}
