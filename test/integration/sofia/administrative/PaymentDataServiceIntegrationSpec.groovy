package sofia.administrative

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec

class PaymentDataServiceIntegrationSpec extends IntegrationSpec {
    FixtureLoader fixtureLoader
    PaymentDataService service = new PaymentDataService()

    void setup() {
        fixtureLoader.load 'administrative/paidConcepts*'
    }

    void 'list of payments ids for invoices'() {
        given:
        List<Long> invoicesIds = Invoice.list()*.id
        List<Long> originalPaymentsIds = Payment.list()*.id
        List<Long> paymentsIds = service.getPaymentsIds(invoicesIds)

        expect:
        originalPaymentsIds.sort() == paymentsIds.sort()
    }
}
