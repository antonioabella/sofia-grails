package sofia.academic

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.Grade
import sofia.SchoolYear

class SubjectDataServiceIntegrationSpec extends IntegrationSpec {
    FixtureLoader fixtureLoader
    SubjectDataService service = new SubjectDataService()

    void setup() {
        fixtureLoader.load('academic/subjects*')
    }

    void 'list subjects by schoolYearId and gradeId'() {
        given:
        Grade grade = Grade.findByName('3er Año')
        SchoolYear schoolYear = SchoolYear.findByActive(true)

        when:
        List<Subject> result = service.findAllBySchoolYearAndGrade(null, null)

        then:
        result == []

        when:
        result = service.findAllBySchoolYearAndGrade(schoolYear.id, grade.id)

        then:
        result.size() == 3
        result.findAll { it.grade == grade }.size() == 3
        result.findAll { it.schoolYear == schoolYear }.size() == 3
    }
}
