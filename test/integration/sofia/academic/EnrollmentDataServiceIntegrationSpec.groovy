package sofia.academic

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.Guardian
import sofia.Person
import sofia.SchoolYear

import java.text.Format
import java.text.SimpleDateFormat

class EnrollmentDataServiceIntegrationSpec extends IntegrationSpec {
    FixtureLoader fixtureLoader
    EnrollmentDataService service = new EnrollmentDataService()

    void setup() {
        fixtureLoader.load 'academic/enrollments*'
    }


    void 'get all enrollments where guardian is main in a given school year'() {
        given:
        Long schoolYearId = SchoolYear.findByActive(true).id
        Long guardianId = Guardian.findByPerson(Person.findByIdentityCard('100')).id

        when:
        List result = service.findAllByGuardianIdAndSchoolYearId(guardianId, schoolYearId)

        then:
        result.size() == 2
    }
}