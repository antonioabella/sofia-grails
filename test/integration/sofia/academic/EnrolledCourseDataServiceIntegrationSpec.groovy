package sofia.academic

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.Grade

class EnrolledCourseDataServiceIntegrationSpec extends IntegrationSpec {
    FixtureLoader fixtureLoader
    EnrolledCourseDataService service = new EnrolledCourseDataService()

    void setup() {
        fixtureLoader.load('academic/enrolledCourses*')
    }

    void 'list enrolled subjects by course'() {
        given:
        Subject subject = Subject.findByName('English')
        Grade grade = Grade.findByName('3er Año')
        Section section = Section.findByGradeAndName(grade, 'A')
        Course course = Course.findBySubjectAndSection(subject, section)

        when:
        List<Subject> result = service.findAllByCourse(null)

        then:
        result == []

        when:
        result = service.findAllByCourse(course.id)

        then:
        result.size() == 16
        result[0].firstName == 'Carlos'
        result[0].lastName == 'Ayala'
        result[0].enrollmentId
        result[1].lastName == 'Bolívar'
        result[2].lastName == 'Bump'
        result[3].lastName == 'Cars'
        result[4].lastName == 'Castillo'
        result[5].lastName == 'Dundee'
        result[6].lastName == 'Molina'
        result[7].lastName == 'Renoir'
        result[8].lastName == 'Ruiz'
        result[9].lastName == 'Smith'
        result[10].lastName == 'Sosa'
        result[11].lastName == 'Spain'
        result[12].lastName == 'Strauss'
        result[13].lastName == 'Bell'
        result[14].lastName == 'Vivaldi'
        result[15].lastName == 'Le Branch'
    }
}
