package sofia.academic

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.SchoolYear
import sofia.Teacher

class CourseDataServiceIntegrationSpec extends IntegrationSpec {
    FixtureLoader fixtureLoader
    CourseDataService service = new CourseDataService()

    void setup() {
        fixtureLoader.load('academic/courses*')
    }

    void 'get course list for teacher in a schoolYear'() {
        given:
        SchoolYear schoolYear = SchoolYear.findByActive(true)
        Teacher teacher = Teacher.first()

        when:
        List<Course> result = service.findAllBySchoolYearAndTeacher(null, null)

        then:
        result == []

        when:
        result = service.findAllBySchoolYearAndTeacher(schoolYear.id, teacher.id)

        then:
        result.size() == 6
    }
}