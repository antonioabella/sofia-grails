package sofia.academic

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec
import sofia.Grade
import sofia.Period

class EvaluationDataServiceIntegrationSpec extends IntegrationSpec {
    EvaluationDataService service = new EvaluationDataService()
    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'academic/evaluations*'
    }

    void 'list evaluations by period and course'() {
        given:
        Subject subject = Subject.findByName('English')
        Grade grade = Grade.findByName('3er Año')
        Section section = Section.findByGradeAndName(grade, 'A')
        Course course = Course.findBySubjectAndSection(subject, section)
        Period period = Period.findByName('Primer Lapso')

        when:
        List<Evaluation> evaluations = service.findAllByPeriodAndCourse(null, null)

        then:
        evaluations == []

        when:
        evaluations = service.findAllByPeriodAndCourse(period.id, course.id)

        then:
        evaluations.size() == 5
        Evaluation evaluation = evaluations.find {it.results.size() > 0}
        evaluation.results.size() == 16
    }
}
