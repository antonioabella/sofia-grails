package sofia

import sofia.enums.Kinship

class CustodyDataServiceIntegrationSpec extends PeopleFinderContractIntegrationSpec {
    CustodyDataService service = new CustodyDataService()

    void setup() {
        fixtureLoader.load 'people/custodians*'
    }

    void 'search returns a list with guardians'() {
        expect:
        service.search('Antonio').size() == 1
        service.search('Torres').size() == 1
        service.search('100').size() == 1
        service.search('Petra Maria').size() == 0
        service.search('n').size() == 3
        service.searchInactive('199').size() == 0
    }

    void 'create new custodian'() {
        Student student = Student.first()
        Guardian guardian = Guardian.last()
        Custody custody = service.save(student, guardian, Kinship.BROTHER)

        expect:
        custody.student == student
        custody.guardian == guardian
    }

    @Override
    List getList() {
        service.search('100')
    }
}
