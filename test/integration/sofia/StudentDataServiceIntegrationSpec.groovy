package sofia

import sofia.enums.Kinship

class StudentDataServiceIntegrationSpec extends PeopleFinderContractIntegrationSpec {
    StudentDataService service = new StudentDataService()
    Student student

    void setup() {
        fixtureLoader.load 'academic/enrollments*'
        student = createStudent()
    }

    void 'search returns a list with students'() {
        expect:
        service.search('Leonardo').size() == 1
        service.search('Cars').size() == 1
        service.search('Ma').size() == 3
        service.search('1').size() == 1
        service.searchInactive('10').size() == 0
    }

    void 'list size limit by PeopleFinder.PEOPLE_LIST_SIZE_LIMIT'() {
        given:
        List result

        when:
        result = service.search('a')

        then:
        result.size() == ActorFinder.PEOPLE_LIST_SIZE_LIMIT
        !result.find { it.firstName == 'Za' }
        result.find { it.firstName == 'Manuela' }
    }

    void 'getGuardians returns guardian list'() {
        when:
        List result = service.getCustodians(student.id)

        then:
        result.size() == student.custodians.size()
        result[0].firstName == 'Antonio'
        result[0].lastName == 'Torres'
        result[0].kinship == Kinship.FATHER
        result[0].id
        result[0].actorId
        result[0].studentId
        result[0].personId
        result[0].picture == 'v5.jpg'
        result[0].main == true
    }

    void 'getEnrollments returns enrollments list'() {
        when:
        List result = service.getEnrollments(student.id)

        then:
        result.size() == student.enrollments.size()
        result[0].with {
            id
            start
            end
            grade == 'Preescolar 1'
            section == 'A'
        }
    }

    void 'get enrollment id for enrollment school year'() {
        when:
        Long result = service.getCurrentEnrollmentId(student.id)

        then:
        student.enrollments[0].id == result
    }

    private Student createStudent() {
        Person person = Person.findByFirstName('Leonardo')
        Student.findByPerson(person)
    }

    @Override
    List getList() {
        service.search('Leonardo')
    }
}