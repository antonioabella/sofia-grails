package sofia

import grails.plugin.fixtures.FixtureLoader
import grails.test.spock.IntegrationSpec

class PeriodDataServiceIntegrationSpec extends IntegrationSpec {
    PeriodDataService service = new PeriodDataService()

    FixtureLoader fixtureLoader

    void setup() {
        fixtureLoader.load 'periods'
    }

    void 'Period list by schoolYear'() {
        given:
        SchoolYear schoolYear = SchoolYear.findByActive(true)

        when:
        List<Period> result = service.findAllBySchoolYear(schoolYear.id)

        then:
        result.size() == 3
    }
}
