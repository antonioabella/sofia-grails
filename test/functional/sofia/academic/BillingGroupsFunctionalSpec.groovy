package sofia.academic

import grails.plugin.remotecontrol.RemoteControl
import spock.lang.Specification
import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes as GA

import wslite.http.auth.HTTPBasicAuthorization
import wslite.rest.*


class BillingGroupsFunctionalSpec extends Specification {
    RESTClient restClient = new RESTClient('http://localhost:8080/')
    private static final String PATH = '/billingGroups/'

    void setup() {
        restClient.authorization = new HTTPBasicAuthorization('pedro', 'perez')
        restClient.httpClient.sslTrustAllCerts = true
    }

    void 'GET a list of billingGroups as JSON'() {
        when:
        def response = restClient.get(path: PATH, accept: ContentType.JSON)

        then:
        1 == 3
        response.json*.name.sort() == ['Group A', 'Group C']
    }
}