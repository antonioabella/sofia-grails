package sofia

import spock.lang.Specification
import wslite.http.auth.HTTPBasicAuthorization
import wslite.rest.*

class ActorsFunctionalSpec extends Specification {
    RESTClient restClient = new RESTClient('http://localhost:8080/')
    private static final String PATH = '/actors/'

    void setup() {
        restClient.authorization = new HTTPBasicAuthorization('pedro', 'perez')
        restClient.httpClient.sslTrustAllCerts = true
    }

    void 'POST a new actor with student entityType' () {
        when:
        def response = restClient.post(path: PATH, accept: ContentType.JSON) {
            type ContentType.JSON
            json firstName: 'Irrelevant', lastName: 'Irrelevant', identityCard: 'Irrelevant',
                  birthDate: new Date(), birthCountry: 'NA', address: 'Irrelevant', gender: 'FEMALE'
        }

        then:
        response.json*.name.sort() == ['Group A', 'Group C']
    }
}