package sofia.enums

enum InvoiceStatus {
    PAID,
    REVERSED,
    OPEN
}