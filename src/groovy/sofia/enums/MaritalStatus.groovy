package sofia.enums

enum MaritalStatus {
    SINGLE,
    MARRIED,
    WIDOWED,
    DIVORCED,
    OTHER
}
