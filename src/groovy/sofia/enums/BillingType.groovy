package sofia.enums

enum BillingType {
    ADVANCE,
    OVERDUE
}
