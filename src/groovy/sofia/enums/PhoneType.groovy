package sofia.enums

enum PhoneType {
    MOBILE,
    HOME,
    WORK,
    OTHER
}
