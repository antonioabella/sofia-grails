package sofia.enums

enum Kinship {
    FATHER,
    MOTHER,
    BROTHER,
    SISTER,
    UNCLE,
    AUNT,
    GRANDMOTHER,
    GRANDFATHER,
    OTHER
}