package sofia

public interface ActorFinder {
    static final int PEOPLE_LIST_SIZE_LIMIT = 20
    List search(String criteria)
    List searchInactive(String criteria)
}