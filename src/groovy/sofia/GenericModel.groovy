package sofia

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional

abstract class GenericModel<T> {

    abstract GenericData getSource()

    @Transactional
    void save(T instance, listener) {
        if (instanceIsNull(instance, listener) || instanceHasErrors(instance, listener)) return

        source.save(instance)
        listener.savingOk(instance)
    }

    protected boolean instanceIsNull(T instance, listener) {
        if (instance == null) {
            listener.notFound(null)
        }
        return (instance == null)
    }

    protected boolean instanceHasErrors(T instance, listener) {
        if (instance.hasErrors()) {
            listener.savingError(instance)
        }
        return instance.hasErrors()
    }

    List search(String criteria, listener) {
        def list
        if (criteria) {
            list = source.search(criteria)
        }
        if (list?.size() == 0) {
            listener.searchNotFound(criteria)
        } else {
            list
        }
    }

    Map list(Integer max, Map params) {
        params.max = Math.min(max ?: 10, 100)
        [instanceList: source.list(params), instanceTotal: source.count()]
    }

    List list() {
        return source.list()
    }

    protected def withDomain(Long id, listener, Closure closure) {
        def instance = source.getInstance(id)
        if (instance) {
            closure.call instance
        } else {
            listener.notFound(id)
        }
    }

    boolean versionError(T instance, listener, version) {
        boolean error = (version != null && instance?.version > version)
        if (error) {
            listener.updatingVersionError(instance)
        }
        error
    }

    boolean isInactivo(T instance, listener) {
        boolean inactivo = hasActiveProperty(instance) && !instance.isActive
        if (inactivo) {
            listener.inactiveError(instance)
        }
        inactivo
    }

    private boolean hasActiveProperty(instance) {
        instance.properties.find { it.key == "active" }
    }

    T edit(T instance, listener) {
        if (instanceIsNull(instance, listener) || isInactivo(instance, listener)) return
        instance
    }

    def create(Map params) {
        return source.create(params)
    }

    @Transactional
    void update(T instance, Long version, listener, Map params) {
        if (instanceIsNull(instance, listener) || isInactivo(instance, listener)) return
        if (versionError(instance, listener, version)) return
        instance.properties = params
        if (source.save(instance)) {
            listener.updatingOk(instance)
        } else {
            listener.updatingError(instance)
        }
    }

    void delete(T instance, listener) {
        if (instanceIsNull(instance, listener) || isInactivo(instance, listener)) return
        try {
            source.delete(instance)
            listener.deletingOk(instance)
        }
        catch (DataIntegrityViolationException ignored) {
            listener.deletingError(instance)
        }
    }

    List<Long> paramToList(GrailsParameterMap params, String paramName) {
        List<Long> list = []
        if (params[paramName]) {
            list = params.list(paramName)[0].collect { String it -> Long.parseLong(it) }
        }
        list
    }

    void activate(T instance) {
        changeActiveStatus(instance, true)
    }

    void deactivate(T instance) {
        changeActiveStatus(instance, false)
    }

    private void changeActiveStatus(T instance, boolean status) {
        if (hasActiveProperty(instance)) {
            instance.active = status
            source.save(instance)
        }
    }

}
