package sofia

import grails.converters.JSON
import sofia.academic.Section

import javax.annotation.PostConstruct

class MarshallerRegistrar {
    @PostConstruct
    void registerMarshaller() {
        println 'Remember: update to grails 2.4.4 to put ObjectMarshaller here:'
        println '\t/src/groovy/sofia/MarshallerRegistrar.groovy'
    }
}
