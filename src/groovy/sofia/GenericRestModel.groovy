package sofia

import grails.rest.RestfulController
import org.springframework.http.HttpStatus

import static org.springframework.http.HttpStatus.*

abstract class GenericRestModel<T> {
    abstract protected GenericData getSource()

    void save(RestfulController listener, T entity) {
        processMethod(listener, entity, CREATED)
    }

    void save(RestfulController listener) {
        save listener, listener.createResource()
    }

    void update(RestfulController listener) {
        updateWithParam(listener, "id")
    }

    void updateWithParam(RestfulController listener, String paramName) {
        T entity = listener.queryForResource(listener.params[paramName])
        entity.properties = listener.getObjectToBind()
        update(listener, entity)
    }

    void update(RestfulController listener, T entity) {
        processMethod(listener, entity, OK)
    }

    protected void discardChildChanges(T entity) {}

    protected void addDefaultValues(T entity) {}

    private boolean hasErrors(RestfulController listener, T entity) {
        entity.validate()
        if (entity.hasErrors()) listener.respond entity, status: UNPROCESSABLE_ENTITY.value()
        entity.hasErrors()
    }

    protected void processMethod(RestfulController listener, T entity, HttpStatus status) {
        discardChildChanges(entity)
        addDefaultValues(entity)
        if (hasErrors(listener, entity)) return
        entity = source.save(entity)
        listener.respond entity, status: entity ? OK.value() : UNPROCESSABLE_ENTITY.value()
    }
}