package sofia

abstract class GenericData<T> {
    abstract Class<T> getSource()

    List<T> search(String criteria) {
        source.search(criteria)
    }

    protected T getInstance(final Long id) {
        source.get(id)
    }

    protected T save(T instance) {
        instance.save(flush: true)
    }

    void delete(T instance) {
        instance.delete(flush: true)
    }

    List<T> list(Map params) {
        source.list(params)
    }

    List<T> list() {
        source.list()
    }

    int count() {
        source.count()
    }

    protected T create(Map params) {
        source.newInstance(params)
    }

    protected T create() {
        source.newInstance()
    }

    protected T getDefault() {
        source.first()
    }

    protected T getInstanceOrDefault(Long id) {
        return id ? getInstance(id) : getDefault()
    }
}
