package sofia

public interface ActorBuilder {
    Actor save(ActorCommand actorCommand)
}