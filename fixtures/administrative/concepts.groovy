import sofia.administrative.Concept

include 'administrative/billingGroups*'

def concepts = build {
    concept1(Concept, name: 'Concept 01', billingGroup: groupA)
    concept2(Concept, name: 'Concept 02', billingGroup: groupA)
    concept3(Concept, name: 'Concept 03', billingGroup: groupA, household: true)
    concept4(Concept, name: 'Concept 04', billingGroup: groupB)
    concept5(Concept, name: 'Concept 05', billingGroup: groupB)
}
