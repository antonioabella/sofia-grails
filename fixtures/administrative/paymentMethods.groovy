import sofia.administrative.PaymentMethod

def paymentMethods = build {
    paymentMethod1(PaymentMethod, name: 'PaymentMethod 01')
    paymentMethod2(PaymentMethod, name: 'PaymentMethod 02')
    paymentMethod3(PaymentMethod, name: 'PaymentMethod 03')
    paymentMethod4(PaymentMethod, name: 'PaymentMethod 04')
    paymentMethod5(PaymentMethod, name: 'PaymentMethod 05')
}
