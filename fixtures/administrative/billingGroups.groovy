import sofia.administrative.BillingGroup

def billingGroups = build {
    groupA(BillingGroup, name: 'Group A')
    groupB(BillingGroup, name: 'Group B')
}
