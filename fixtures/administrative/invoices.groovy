import sofia.administrative.Discount
import sofia.administrative.Invoice
import sofia.administrative.Payment
import sofia.administrative.PaymentItem
import sofia.enums.InvoiceStatus

def sdf = new java.text.SimpleDateFormat('yyyy-MM-dd')

include 'academic/enrollments*', 'administrative/billingGroups*','administrative/paymentMethods*', 'administrative/concepts*'

def payments = build {
    payment1(Payment)
    payment2(Payment)
}

def paymentItems = build {
    paymentItem1(PaymentItem, paymentMethod: paymentMethod1, amount: 10, identifier: 'AAAA', payment: payment1)
    paymentItem2(PaymentItem, paymentMethod: paymentMethod1, amount: 10, identifier: 'BBBB', payment: payment2)
    paymentItem3(PaymentItem, paymentMethod: paymentMethod2, amount: 10, identifier: 'EEEE', payment: payment1)
    paymentItem4(PaymentItem, paymentMethod: paymentMethod3, amount: 10, identifier: 'FFFF', payment: payment2)
}

def invoices = build {
    invoice1(Invoice, billingGroup: groupA, enrollment: enrollment01, invoiceDate: sdf.parse("2014-01-01"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.PAID, payment: payment1)
    invoice2(Invoice, billingGroup: groupA, enrollment: enrollment02, invoiceDate: sdf.parse("2014-01-02"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.PAID, payment: payment1)
    invoice3(Invoice, billingGroup: groupA, enrollment: enrollment03, invoiceDate: sdf.parse("2014-01-03"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.PAID, payment: payment2)
    invoice4(Invoice, billingGroup: groupA, enrollment: enrollment04, invoiceDate: sdf.parse("2014-01-04"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.PAID, payment: payment2)
    invoice5(Invoice, billingGroup: groupA, enrollment: enrollment05, invoiceDate: sdf.parse("2014-01-06"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.REVERSED)
    invoice6(Invoice, billingGroup: groupA, enrollment: enrollment06, invoiceDate: sdf.parse("2014-01-06"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.PAID)
    invoice7(Invoice, billingGroup: groupA, enrollment: enrollment07, invoiceDate: sdf.parse("2014-01-07"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.OPEN)
    invoice8(Invoice, billingGroup: groupA, enrollment: enrollment08, invoiceDate: sdf.parse("2014-01-07"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.PAID)
    invoice9(Invoice, billingGroup: groupA, enrollment: enrollment08, invoiceDate: sdf.parse("2014-01-08"), total: 10, subtotal: 5, discount: 5, status: InvoiceStatus.PAID)
}

def discounts = build {
    discount1(Discount, enrollment: enrollment01, concept: concept1, active: true, amount: 50)
    discount2(Discount, enrollment: enrollment02, concept: concept2, active: true, amount: 50)
    discount3(Discount, enrollment: enrollment03, concept: concept3, active: true, amount: 50)
}
