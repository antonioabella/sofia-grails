import sofia.administrative.PaidConcept

include 'administrative/invoices*'

def paidConcepts = build {
    paidConcept1(PaidConcept, invoice: invoice1, enrollment: enrollment01, concept: concept1, month: septiembre)
    paidConcept2(PaidConcept, invoice: invoice1, enrollment: enrollment01, concept: concept2, month: septiembre)
    paidConcept3(PaidConcept, invoice: invoice1, enrollment: enrollment01, concept: concept3, month: septiembre)
}