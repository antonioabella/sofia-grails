import sofia.administrative.Commitment

include 'academic/enrollments*', 'administrative/concepts*'

def commitments = build {
    commitment1(Commitment, enrollment: enrollment01, enrollmentMonth: noviembre, concept: concept1)
    commitment2(Commitment, enrollment: enrollment01, enrollmentMonth: septiembre, concept: concept2)
    commitment3(Commitment, enrollment: enrollment01, enrollmentMonth: septiembre, concept: concept3)
    commitment4(Commitment, enrollment: enrollment02, enrollmentMonth: noviembre, concept: concept1)
    commitment5(Commitment, enrollment: enrollment02, enrollmentMonth: septiembre, concept: concept2)
    commitment6(Commitment, enrollment: enrollment02, enrollmentMonth: septiembre, concept: concept3)
    commitment7(Commitment, enrollment: enrollment03, enrollmentMonth: noviembre, concept: concept1)
    commitment8(Commitment, enrollment: enrollment03, enrollmentMonth: septiembre, concept: concept2)
    commitment9(Commitment, enrollment: enrollment03, enrollmentMonth: septiembre, concept: concept3)
    commitment0(Commitment, enrollment: enrollment04, enrollmentMonth: septiembre, concept: concept3)
}
