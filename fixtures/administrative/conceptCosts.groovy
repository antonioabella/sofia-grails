import sofia.administrative.ConceptCost
import sofia.enums.ConceptCostType

include 'administrative/concepts*', 'schoolYears', 'months'

def conceptCosts = build {
    conceptCost01(ConceptCost, concept: concept1, conceptCostType: ConceptCostType.MONTHLY, schoolYear: peActive, month: septiembre, amount: 100)
    conceptCost02(ConceptCost, concept: concept2, conceptCostType: ConceptCostType.MONTHLY, schoolYear: peActive, month: octubre, amount: 200)
    conceptCost03(ConceptCost, concept: concept3, conceptCostType: ConceptCostType.MONTHLY, schoolYear: peActive, month: noviembre, amount: 300)
    conceptCost04(ConceptCost, concept: concept4, conceptCostType: ConceptCostType.MONTHLY, schoolYear: peActive, month: diciembre, amount: 400)
    conceptCost05(ConceptCost, concept: concept5, conceptCostType: ConceptCostType.MONTHLY, schoolYear: peActive, month: enero, amount: 500)
}
