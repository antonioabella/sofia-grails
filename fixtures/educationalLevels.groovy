import sofia.EducationLevel

build {
    n1(EducationLevel, name: "Preschool")
    n2(EducationLevel, name: "Basic Education")
    n3(EducationLevel, name: "High School")
}
