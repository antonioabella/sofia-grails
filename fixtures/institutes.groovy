import sofia.Institute
import sofia.enums.BillingType

build {
    institute(Institute) {
        address = 'Caracas'
        taxId = 'J295525583'
        name = 'New Big College'
        code = '5612435'
        billingType = BillingType.ADVANCE
    }
}
