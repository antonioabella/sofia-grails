import sofia.Grade

include "educationalLevels"

def grades = build {
    gp1(Grade, level: ref("n1"), name: "Preescolar 1",  position: 1)
    gp2(Grade, level: ref("n1"), name: "Preescolar 2",  position: 2)
    gp3(Grade, level: ref("n1"), name: "Preescolar 3",  position: 3)
    g1(Grade,  level: ref("n2"), name: "1er Grado",  position: 4)
    g2(Grade,  level: ref("n2"), name: "2do Grado",  position: 5)
    g3(Grade,  level: ref("n2"), name: "3er Grado",  position: 6)
    g4(Grade,  level: ref("n2"), name: "4to Grado",  position: 7)
    g5(Grade,  level: ref("n2"), name: "5to Grado",  position: 8)
    g6(Grade,  level: ref("n2"), name: "6to Grado",  position: 9)
    gb1(Grade, level: ref("n3"), name: "1er Año",  position: 10)
    gb2(Grade, level: ref("n3"), name: "2do Año",  position: 11)
    gb3(Grade, level: ref("n3"), name: "3er Año",  position: 12)
    gb4(Grade, level: ref("n3"), name: "4to Año",  position: 13)
    gb5(Grade, level: ref("n3"), name: "5to Año",  position: 14)
}

post {
    gp1.next = gp2
    gp2.next = gp3
    gp3.next = g1
    g1.next = g2
    g2.next = g3
    g3.next = g4
    g4.next = g5
    g5.next = g6
    g6.next = gb1
    gb1.next = gb2
    gb2.next = gb3
    gb3.next = gb4
    gb4.next = gb5
    gp2.previous = gp1
    gp3.previous = gp2
    g1.previous = gp3
    g2.previous = g1
    g3.previous = g2
    g4.previous = g3
    g5.previous = g4
    g6.previous = g5
    gb1.previous = g6
    gb2.previous = gb1
    gb3.previous = gb2
    gb4.previous = gb3
    gb5.previous = gb4
    gp1.save()
    gp2.save()
    gp3.save()
    g1.save()
    g2.save()
    g3.save()
    g4.save()
    g5.save()
    g6.save()
    gb1.save()
    gb2.save()
    gb3.save()
    gb4.save()
    gb5.save()
}
