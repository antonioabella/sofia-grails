import sofia.PeriodWeek

import java.text.SimpleDateFormat

def sdf = new SimpleDateFormat('yyyy-MM-dd')
include 'periods'

build {
    pw1(PeriodWeek, start: sdf.parse('2014-09-15'), end: sdf.parse('2014-12-15'), period: p1)
    pw2(PeriodWeek, start: sdf.parse('2015-01-07'), end: sdf.parse('2015-03-14'), period: p1)
    pw3(PeriodWeek, start: sdf.parse('2015-03-15'), end: sdf.parse('2015-06-15'), period: p1)
}

