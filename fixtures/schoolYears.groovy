import sofia.SchoolYear

import java.text.SimpleDateFormat

def sdf = new SimpleDateFormat('yyyy-MM-dd')

build {
    pe2009(SchoolYear, start: sdf.parse('2009-09-15'), end: sdf.parse('2010-09-14'), active: false, enrollment: false)
    pe2010(SchoolYear, start: sdf.parse('2010-09-15'), end: sdf.parse('2011-09-14'), active: false, enrollment: false)
    pe2011(SchoolYear, start: sdf.parse('2011-09-15'), end: sdf.parse('2012-09-14'), active: false, enrollment: false)
    peActive(SchoolYear, start: sdf.parse('2012-09-15'), end: sdf.parse('2013-09-14'), active: false, enrollment: false)
    pe2013(SchoolYear, start: sdf.parse('2013-09-15'), end: sdf.parse('2014-09-14'), active: false, enrollment: false)
    peActive(SchoolYear, start: sdf.parse('2014-09-15'), end: sdf.parse('2015-09-14'), active: true, enrollment: true)
    pe2015(SchoolYear, start: sdf.parse('2015-09-15'), end: sdf.parse('2016-09-14'), active: false, enrollment: false)
    pe2016(SchoolYear, start: sdf.parse('2016-09-15'), end: sdf.parse('2017-09-14'), active: false, enrollment: false)
}
