import sofia.Student

include 'people/people*'

build {
    s01(Student, person: p01, active: true)
    s02(Student, person: p02, active: true)
    s03(Student, person: p03, active: true)
    s04(Student, person: p04, active: true)
    s05(Student, person: p05, active: true)
    s06(Student, person: p06, active: true)
    s07(Student, person: p07, active: true)
    s08(Student, person: p08, active: true)
    s09(Student, person: p09, active: true)
    s10(Student, person: p10, active: true)
    s11(Student, person: p11, active: true)
    s12(Student, person: p12, active: true)
    s13(Student, person: p13, active: true)
    s14(Student, person: p14, active: true)
    s15(Student, person: p15, active: true)
    s16(Student, person: p16, active: true)
    s17(Student, person: p17, active: true)
    s18(Student, person: p18, active: true)
    s19(Student, person: p19, active: true)
    s20(Student, person: p20, active: true)
    s21(Student, person: p21, active: true)
    s22(Student, person: p22, active: true)
    s23(Student, person: p23, active: true)
    s24(Student, person: p24, active: true)
    s25(Student, person: p25, active: true)
    s26(Student, person: p26, active: true)
}
