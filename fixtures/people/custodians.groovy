import sofia.Custody

include 'people/guardians*'

build {
    c01(Custody, student: s01, guardian: gu1, main: true)
    c001(Custody, student: s01, guardian: gu2, main: false)
    c02(Custody, student: s02, guardian: gu2, main: true)
    c03(Custody, student: s03, guardian: gu3, main: true)
    c04(Custody, student: s04, guardian: gu4, main: false)
    c05(Custody, student: s05, guardian: gu1, main: false)
    c06(Custody, student: s06, guardian: gu2, main: false)
    c07(Custody, student: s07, guardian: gu3, main: true)
    c08(Custody, student: s08, guardian: gu4, main: true)
    c09(Custody, student: s09, guardian: gu1, main: false)
    c10(Custody, student: s10, guardian: gu2, main: false)
    c11(Custody, student: s11, guardian: gu3, main: true)
    c12(Custody, student: s12, guardian: gu4, main: false)
    c13(Custody, student: s13, guardian: gu1, main: false)
    c14(Custody, student: s14, guardian: gu2, main: true)
    c15(Custody, student: s15, guardian: gu3, main: true)
    c16(Custody, student: s16, guardian: gu4, main: false)
    c17(Custody, student: s17, guardian: gu3, main: true)
    c18(Custody, student: s18, guardian: gu4, main: true)
    c19(Custody, student: s19, guardian: gu1, main: false)
    c20(Custody, student: s20, guardian: gu2, main: false)
    c21(Custody, student: s21, guardian: gu3, main: true)
    c22(Custody, student: s22, guardian: gu4, main: false)
    c23(Custody, student: s23, guardian: gu1, main: true)
    c24(Custody, student: s24, guardian: gu2, main: true)
    c25(Custody, student: s25, guardian: gu3, main: true)
    c26(Custody, student: s26, guardian: gu4, main: false)
}

