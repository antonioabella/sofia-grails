import sofia.Person
import sofia.PhoneNumber
import sofia.Email
import sofia.enums.PhoneType

final String EMAIL_ADDRESS = 'irrelevant@example.com'
final String PHONE_NUMBER = '555-5555555'
build {
    p01(Person, emailAddress: 'example@gmail.com', phoneNumber: PHONE_NUMBER, identityCard: "1", firstName: "Sarah", lastName: "Sosa",
            birthDate: Date.parse('yyyy-MM-dd', '2011-09-27'), address: 'An irrelevant address', gender: 'FEMALE',
            notes: 'Nothing', birthCountry: 'VE', picture: 'h1.jpg', facebook: 'sarah', twitter: 'sarah')
    p02(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "2", firstName: "Leonardo", lastName: "Ruiz", picture: 'v1.jpg')
    p03(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "3", firstName: "Julio", lastName: "Molina", picture: 'v2.jpg')
    p04(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "4", firstName: "Manuela", lastName: "Bolívar", picture: 'h2.jpg')
    p05(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "5", firstName: "Patricia", lastName: "Cars", picture: 'h2.jpg')
    p06(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "6", firstName: "Luisa", lastName: "Bump", picture: 'h2.jpg')
    p07(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "7", firstName: "Ana", lastName: "Castillo", picture: 'h2.jpg')
    p08(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "8", firstName: "Pedro", lastName: "Spain", picture: 'v3.jpg')
    p09(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "9", firstName: "Miguel", lastName: "Renoir", picture: 'v4.jpg')
    p10(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "10", firstName: "Angelo", lastName: "Dundee", picture: 'v5.jpg')
    p11(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "11", firstName: "Martha", lastName: "Smith", picture: 'h2.jpg')
    p12(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "12", firstName: "Johan Andrew", lastName: "Le Branch", picture: 'v6.png')
    p13(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "13", firstName: "Alexander", lastName: "Bell", picture: 'v2.jpg')
    p14(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "14", firstName: "Fernando", lastName: "Vivaldi", picture: 'v3.jpg')
    p15(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "15", firstName: "Carlos", lastName: "Ayala", picture: 'v4.jpg')
    p16(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "16", firstName: "Johan", lastName: "Strauss", picture: 'v5.jpg')
    p17(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "17", firstName: "Pedro", lastName: "Wang", picture: 'v1.jpg')
    p18(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "18", firstName: "Pablo", lastName: "Lee", picture: 'v5.jpg')
    p19(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "19", firstName: "Julio", lastName: "Carmelita", picture: 'v4.jpg')
    p20(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "20", firstName: "Elisa", lastName: "Navarro", picture: 'h1.jpg')
    p21(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: "21", firstName: "Ana", lastName: "Castro", picture: 'h5.jpg')
    p22(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '22', firstName: "Maria", lastName: "Jacinta", picture: 'h3.jpg')
    p23(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '23', firstName: "Petra", lastName: "Call", picture: 'h2.jpg')
    p24(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '24', firstName: "Joseph", lastName: "Mendez", picture: 'v2.jpg')
    p25(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '25', firstName: "Ernesto", lastName: "Zamora", picture: 'v1.jpg')
    p26(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '26', firstName: "Acacia", lastName: "Phelps", picture: 'h1.jpg')
    rp1(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '100', firstName: "Diana", lastName: "Cruise", picture: 'h5.jpg')
    rp2(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '101', firstName: "Antonio", lastName: "Torres", picture: 'v5.jpg')
    rp3(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '102', firstName: "Luis", lastName: "Perez", picture: 'v3.jpg')
    rp4(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '103', firstName: "Isabel", lastName: "Sanchez", picture: 'h4.jpg')
    t01(Person, emailAddress: EMAIL_ADDRESS, phoneNumber: PHONE_NUMBER, identityCard: '104', firstName: "Elena", lastName: "Estrada", picture: 'h6.jpg')
}

build {
    pn1(PhoneNumber, number: 'irrelevant', phoneType: PhoneType.HOME)
    pn2(PhoneNumber, number: 'irrelevant', phoneType: PhoneType.HOME)
}

build {
    e1(Email, address: 'total_irrelevance@example.com')
    e2(Email, address: 'total_irrelevance@example.com')
}

post {
    p01.addToPhoneNumbers(pn1)
    p01.addToEmails(e1)
    p02.addToPhoneNumbers(pn2)
    p02.addToEmails(e2)
}
