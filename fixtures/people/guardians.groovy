import sofia.Guardian

include 'people/students*'

build {
    gu1(Guardian, person: rp1, active: true)
    gu2(Guardian, person: rp2, active: true)
    gu3(Guardian, person: rp3, active: true)
    gu4(Guardian, person: rp4, active: true)
}
