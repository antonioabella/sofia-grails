import sofia.Month

build {
    enero(Month, name: "enero", position: 5, systemId: 0)
    febrero(Month, name: "febrero", position: 6, systemId: 1)
    marzo(Month, name: "marzo", position: 7, systemId: 2)
    abril(Month, name: "abril", position: 8, systemId: 3)
    mayo(Month, name: "mayo", position: 9, systemId: 4)
    junio(Month, name: "junio", position: 10, systemId: 5)
    julio(Month, name: "julio", position: 11, systemId: 6)
    agosto(Month, name: "agosto", position: 12, systemId: 7)
    septiembre(Month, name: "septiembre", position: 1, systemId: 8)
    octubre(Month, name: "octubre", position: 2, systemId: 9)
    noviembre(Month, name: "noviembre", position: 3, systemId: 10)
    diciembre(Month, name: "diciembre", position: 4, systemId: 11)
    matricula(Month, name: "matrícula", position: 0, systemId: 12)
}