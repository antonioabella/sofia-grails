import sofia.academic.EnrolledCourse
import sofia.academic.Enrollment
import sofia.academic.Evaluation
import sofia.administrative.*

include 'academic/enrollments*', 'academic/courses*'

def enrolledCourses = build {
    ec01(EnrolledCourse, enrollment: enrollment01, course: course03)
    ec02(EnrolledCourse, enrollment: enrollment02, course: course03)
    ec03(EnrolledCourse, enrollment: enrollment03, course: course03)
    ec04(EnrolledCourse, enrollment: enrollment04, course: course03)
    ec05(EnrolledCourse, enrollment: enrollment05, course: course03)
    ec06(EnrolledCourse, enrollment: enrollment06, course: course03)
    ec07(EnrolledCourse, enrollment: enrollment07, course: course03)
    ec08(EnrolledCourse, enrollment: enrollment08, course: course03)
    ec09(EnrolledCourse, enrollment: enrollment09, course: course03)
    ec10(EnrolledCourse, enrollment: enrollment10, course: course03)
    ec11(EnrolledCourse, enrollment: enrollment11, course: course03)
    ec12(EnrolledCourse, enrollment: enrollment12, course: course03)
    ec13(EnrolledCourse, enrollment: enrollment13, course: course03)
    ec14(EnrolledCourse, enrollment: enrollment14, course: course03)
    ec15(EnrolledCourse, enrollment: enrollment15, course: course03)
    ec16(EnrolledCourse, enrollment: enrollment16, course: course03)
    ec21(EnrolledCourse, enrollment: enrollment01, course: course02)
    ec22(EnrolledCourse, enrollment: enrollment02, course: course02)
    ec23(EnrolledCourse, enrollment: enrollment03, course: course02)
    ec24(EnrolledCourse, enrollment: enrollment04, course: course02)
    ec25(EnrolledCourse, enrollment: enrollment05, course: course02)
    ec26(EnrolledCourse, enrollment: enrollment06, course: course02)
    ec27(EnrolledCourse, enrollment: enrollment07, course: course02)
    ec28(EnrolledCourse, enrollment: enrollment08, course: course02)
    ec29(EnrolledCourse, enrollment: enrollment09, course: course02)
    ec30(EnrolledCourse, enrollment: enrollment10, course: course02)
    ec31(EnrolledCourse, enrollment: enrollment11, course: course02)
    ec32(EnrolledCourse, enrollment: enrollment12, course: course02)
    ec33(EnrolledCourse, enrollment: enrollment13, course: course02)
    ec34(EnrolledCourse, enrollment: enrollment14, course: course02)
    ec35(EnrolledCourse, enrollment: enrollment15, course: course02)
    ec36(EnrolledCourse, enrollment: enrollment16, course: course02)
}
