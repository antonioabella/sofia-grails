import sofia.academic.EvaluationInstrument

/*
Análisis
Anteproyecto
Comprobación de lectura
Cuestionario
Debate
Ejercicios
English Special Activity
Ensayo
Examen
Composición
Guía de Problemas
Informe
Laboratorio
Listening
Monografía
Practica
Quiz
Rasgos
Reading
Reading-Writing
Resumen
Taller
Trabajo
Trabajo en Clase
Video
Workbook
Writing
 */

def instruments = build {
    instrument01(EvaluationInstrument, name: 'Interrogatorio')
    instrument02(EvaluationInstrument, name: 'Cartelera')
    instrument03(EvaluationInstrument, name: 'Prueba Escrita')
}

