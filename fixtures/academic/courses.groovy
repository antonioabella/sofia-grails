import sofia.academic.Course

include 'people/teachers*', 'academic/sections*', 'academic/subjects*'

def courses = build {
    course01(Course, teacher: te1, subject: subject1, section: sectionb3A)
    course02(Course, teacher: te1, subject: subject2, section: sectionb3A)
    course03(Course, teacher: te1, subject: subject3, section: sectionb3A)
    course04(Course, teacher: te1, subject: subject1, section: sectionb3B)
    course05(Course, teacher: te1, subject: subject2, section: sectionb3B)
    course06(Course, teacher: te1, subject: subject3, section: sectionb3B)
}