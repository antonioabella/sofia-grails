import sofia.academic.EnrolledCourse
import sofia.academic.Evaluation

include 'academic/enrolledCourses*', 'periodWeeks'

String format = 'yyy-MM-dd'
String date = '2014-10-15'

def evaluations = build {
    ev01(Evaluation, course: course03, period: p1, week: pw1, instrument: 'Prueba Corta', content: 'Objetivos 01 al 07', weight: 20)
    ev02(Evaluation, course: course03, period: p1, week: pw1, instrument: 'Prueba Corta', content: 'Objetivos 08 al 10', weight: 30)
    ev03(Evaluation, course: course03, period: p1, week: pw2, instrument: 'Prueba Corta', content: 'Objetivos 11 al 20', weight: 15)
    ev04(Evaluation, course: course03, period: p1, week: pw2, instrument: 'Prueba Corta', content: 'Objetivos 21 al 30', weight: 25)
    ev05(Evaluation, course: course03, period: p1, week: pw3, instrument: 'Prueba Corta', content: 'Objetivos 31 al 35', weight: 10)

//    ev06(Evaluation, course: course02, period: p1, evaluationDate: Date.parse(format, date), instrument: instrument01, name: 'Test 01', description: 'Objetivos 01 al 07', position: 1, weight: 20)
//    ev07(Evaluation, course: course02, period: p1, evaluationDate: Date.parse(format, date), instrument: instrument01, name: 'Test 02', description: 'Objetivos 08 al 10', position: 2, weight: 30)
//    ev08(Evaluation, course: course02, period: p1, evaluationDate: Date.parse(format, date), instrument: instrument01, name: 'Test 03', description: 'Objetivos 11 al 20', position: 3, weight: 15)
//    ev09(Evaluation, course: course02, period: p1, evaluationDate: Date.parse(format, date), instrument: instrument01, name: 'Test 04', description: 'Objetivos 21 al 30', position: 4, weight: 20)
}

Random random = new Random()
post {
    notes1 = [5, 8, 20, 14, 11, 17, 18, 12, 20, 0, 13, 9, 7, 5, 20, 19]
    notes2 = [12, 13, 14, 15, 16, 17, 18, 19, 20, 19, 18, 17, 16, 15, 14, 13]
    notes3 = [16, 17, 18, 19, 20, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10]
    notes4 = [5, 8, 20, 14, 11, 17, 18, 12, 20, 0, 13, 9, 7, 5, 20, 19]
    notes5 = [16, 17, 18, 19, 20, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10]
    annotation = ['Truant', 'Cry like baby', 'Should to repeat this', 'Good Work!']
    List<Long> keys = EnrolledCourse.findAllByCourse(course03)*.enrollmentId
//    List evals = [ev01, ev02, ev03, ev06, ev07]
    List evals = [ev01, ev02, ev03]
    List notes = [notes1, notes2, notes3, notes4, notes5]
    3.times { idx ->
        Map results = [:]
        Map remarks = [:]
        keys.eachWithIndex { Long key, int i ->
            results.put(key.toString(), notes[idx][i].toString())
            int luck = random.nextInt(10)
            if (luck < 3) remarks.put(key.toString(), annotation[random.nextInt(3)])
        }

        evals[idx].results = results
        evals[idx].remarks = remarks
        evals[idx].save()
    }
}

