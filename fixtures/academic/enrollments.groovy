import sofia.academic.Enrollment

def sdf = new java.text.SimpleDateFormat('yyyy-MM-dd')
include 'people/custodians*', 'institutes', 'months', 'academic/sections*'

def enrollments = build {
    enrollment01(Enrollment, student: s01, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment02(Enrollment, student: s02, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: false, section: sectionb3A)
    enrollment03(Enrollment, student: s03, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment04(Enrollment, student: s04, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment05(Enrollment, student: s05, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment06(Enrollment, student: s06, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment07(Enrollment, student: s07, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment08(Enrollment, student: s08, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment09(Enrollment, student: s09, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment10(Enrollment, student: s10, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment11(Enrollment, student: s11, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3A)
    enrollment12(Enrollment, student: s12, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: noviembre, sofia: true, section: sectionb3A)
    enrollment13(Enrollment, student: s13, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: octubre, sofia: true, section: sectionb3A)
    enrollment14(Enrollment, student: s14, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: octubre, sofia: true, section: sectionb3B)
    enrollment15(Enrollment, student: s15, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment16(Enrollment, student: s16, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment17(Enrollment, student: s17, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment18(Enrollment, student: s18, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment19(Enrollment, student: s19, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment20(Enrollment, student: s20, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment21(Enrollment, student: s21, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment22(Enrollment, student: s22, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment23(Enrollment, student: s23, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment24(Enrollment, student: s24, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment25(Enrollment, student: s25, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
    enrollment26(Enrollment, student: s26, institute: institute, schoolYear: peActive, grade: gb3, enrollmentDate: sdf.parse('2012-09-15'), enrollmentMonth: septiembre, sofia: true, section: sectionb3B)
}
