import sofia.academic.Subject

include 'grades', 'schoolYears'

def subjects = build {
    subject1(Subject, name: 'History', schoolYear: peActive, grade: gb3)
    subject2(Subject, name: 'Chemistry', schoolYear: peActive, grade: gb3)
    subject3(Subject, name: 'English', schoolYear: peActive, grade: gb3)
}