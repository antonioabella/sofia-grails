import sofia.academic.Section

include "grades", "schoolYears"

def sections = build {
    sectionp1A(Section, name: "A", schoolYear: peActive, grade: gp1)
    sectionp1B(Section, name: "B", schoolYear: peActive, grade: gp1)
    sectionp2A(Section, name: "A", schoolYear: peActive, grade: gp2)
    sectionp2B(Section, name: "B", schoolYear: peActive, grade: gp2)
    sectionp3A(Section, name: "A", schoolYear: peActive, grade: gp3)
    sectionp3B(Section, name: "B", schoolYear: peActive, grade: gp3)
    section01A(Section, name: "A", schoolYear: peActive, grade: g1)
    section01B(Section, name: "B", schoolYear: peActive, grade: g1)
    section02A(Section, name: "A", schoolYear: peActive, grade: g2)
    section02B(Section, name: "B", schoolYear: peActive, grade: g2)
    section03A(Section, name: "A", schoolYear: peActive, grade: g3)
    section03B(Section, name: "B", schoolYear: peActive, grade: g3)
    section04A(Section, name: "A", schoolYear: peActive, grade: g4)
    section04B(Section, name: "B", schoolYear: peActive, grade: g4)
    section05A(Section, name: "A", schoolYear: peActive, grade: g5)
    section05B(Section, name: "B", schoolYear: peActive, grade: g5)
    section06A(Section, name: "A", schoolYear: peActive, grade: g6)
    section06B(Section, name: "B", schoolYear: peActive, grade: g6)
    sectionb1A(Section, name: "A", schoolYear: peActive, grade: gb1)
    sectionb1B(Section, name: "B", schoolYear: peActive, grade: gb1)
    sectionb2A(Section, name: "A", schoolYear: peActive, grade: gb2)
    sectionb2B(Section, name: "B", schoolYear: peActive, grade: gb2)
    sectionb3A(Section, name: "A", schoolYear: peActive, grade: gb3)
    sectionb3B(Section, name: "B", schoolYear: peActive, grade: gb3)
    sectionb4A(Section, name: "A", schoolYear: peActive, grade: gb4)
    sectionb4B(Section, name: "B", schoolYear: peActive, grade: gb4)
    sectionb5A(Section, name: "A", schoolYear: peActive, grade: gb5)
    sectionb5B(Section, name: "B", schoolYear: peActive, grade: gb5)
}
