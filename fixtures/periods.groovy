import sofia.Period

import java.text.SimpleDateFormat

def sdf = new SimpleDateFormat('yyyy-MM-dd')
include 'schoolYears'

build {
    p1(Period, start: sdf.parse('2014-09-15'), end: sdf.parse('2014-12-15'), schoolYear: peActive, name: 'Primer Lapso')
    p2(Period, start: sdf.parse('2015-01-07'), end: sdf.parse('2015-03-14'), schoolYear: peActive, name: 'Segundo Lapso')
    p3(Period, start: sdf.parse('2015-03-15'), end: sdf.parse('2015-06-15'), schoolYear: peActive, name: 'Tercer Lapso')
}